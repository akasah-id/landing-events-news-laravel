<?php

namespace App\Http\Controllers\StarterKit;
use App\Http\Controllers\Controller;
use App\Models\AccessModel;
use App\Models\GroupsModel;
use App\Models\MenusModel;

use App\Libraries\Helpers;
use App\Http\Requests;
use Illuminate\Http\Request;

class Access extends Controller
{
    protected $data = array();

    public function __construct()
    {
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->menuID = 5;
        $this->urlModule = '/access';
        $this->viewFile = 'starter_kit.access';
        $this->title = 'Access';

        $this->model = new AccessModel();
        $this->group = new GroupsModel();
        $this->menu = new MenusModel();
    }

    public function data($groupID)
    {
        $rows = $this->group->getRow($groupID);

        $this->grant_check('is_read',$this->menuID);
        $data['access']=new Access;
        $data['menuID']=$this->menuID;
        $data['title']=$rows->name.' - '.__('access.'.$this->title.' Management');
        $data['urlModule']=$this->urlModule;
        $data['delBtn'] = Helpers::del_button("$this->urlModule/batch_action/$groupID");
        
        $data['groupID']=$groupID;
        $data['column']=json_encode(array(
            array('data'=>'id'),
            array('data'=>'group'),
            array('data'=>'module'),
            array('data'=>'access'),
            array('data'=>'action')
        ));
        return view($this->viewFile, $data);
    }

    public function ajax_data(Request $req, $groupID)
    {
        $this->grant_check('is_read', $this->menuID);
        $access = new Access;
        $data = $this->model->join('tb_groups','tb_groups.group_id','=','tb_access.group_id')->join('tb_menu','tb_menu.menu_id','=','tb_access.menu_id')->where('tb_access.group_id',$groupID)->get();
        $no = 1;
        $records["draw"] = 1;
        $records["recordsTotal"] = count($data);
        $records["recordsFiltered"] = count($data);
        $records["data"]=[];
        foreach ($data as $key) {
            $action = '';
            if ($access->grant_check('is_update', $this->menuID, true)) {
                $action .= '<a href="' . url("$this->urlModule/view/$key->group_id/$key->access_id") . '" class="btn waves-effect waves-light btn-outline-warning btn-sm btn-icon-fixed"><span class="fa fa-edit"></span> '.__("dashboard.Edit").'</a> ';
            }

            $c=$key->is_create==1 ? 'Create ' : '';
            $r=$key->is_read==1 ? ' Read  ' : '';
            $u=$key->is_update==1 ? ' Update ' : '';
            $d=$key->is_delete==1 ? ' Delete' : '';
            $accesscrud=$c.$r.$u.$d;
            $records["data"][] = array(
                "DT_RowId"=>"data_".$key->access_id,
                'id'=>$key->access_id,
                'group'=>$key->name,
                'module'=>$key->menu_name,
                'access'=> $accesscrud,
                'action' => $action,
            );
            $no++;
        }
        // header('Content-Type: application/json');
        if ($req->ajax()) {
            echo json_encode($records);
        } else {
            return redirect($this->urlModule);
        }
    }

    public function view($groupID, $id=0)
    {
        $data['urlModule']=$this->urlModule;
        $data['information']=[];
        $data['id']=$id;
        $data['groupID']=$groupID;

        $data['groups'] = $this->group->getExclude('alias', ['superadmin']);
        $data['menus']=$this->menu->get();
        $this->grant_check('is_update', $this->menuID);
        $rows = $this->group->getRow($groupID);
        if ($id==0) {
            $this->grant_check('is_create', $this->menuID);
            $data['title'] = $rows->name.' - '.__('access.Add Access');
        } else {
            $data['information']=$this->model->where('access_id', $id)->first();
            $data['title'] = $rows->name.' - '.__('access.Edit Access');
        }
        return view($this->viewFile.'_view',$data);
    }

    public function save(Request $request, $groupID, $id)
    {
        $data = $request->except(['_token']);
        if ($data) {
            if ($id == 0) {
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['group_id'] = $groupID;
                $this->grant_check('is_create', $this->menuID);
                $result = $this->model->saveRow($data);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_add'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_save'),'success');
                    return redirect("$this->urlModule/$groupID");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_save'),'error');
                    return redirect("$this->urlModule/$groupID/view")->withInput();
                }
            } else {
                $data['is_create'] = is_null($request->is_create) ? '0' : '1';
                $data['is_read'] = is_null($request->is_read) ? '0' : '1';
                $data['is_update'] = is_null($request->is_update) ? '0' : '1';
                $data['is_delete'] = is_null($request->is_delete) ? '0' : '1';
                $this->grant_check('is_update', $this->menuID);
                $result = $this->model->saveRow($data, $id);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_edit'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_update'),'success');
                    return redirect("$this->urlModule/$groupID");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_update'),'error');
                    return redirect("$this->urlModule/$groupID/view")->withInput();
                }
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.no_request'),'error');
            return redirect("$this->urlModule/$groupID/view")->withInput();
        }
    }

    public function batch_action(Request $req, $groupID)
    {
        if ($req->action=='Delete') {
            $this->grant_check('is_delete', $this->menuID);
            $id=json_decode($req->id_item);
            if (count($id)>1) {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_bulk'),'success');        
            } else {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_delete'),'success');        
            }
            foreach ($id as $key) {
                $result=$this->model->where('access_id', $key->id_item)->delete();
                if ($result) {
                    Helpers::auditTrail($req, sprintf(__('dashboard.log_delete'),$key->id_item));
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_delete'),'error');        
                }
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_bulk'),'error');
        }
        return redirect("$this->urlModule/$groupID");
    }

    public static function grant_check($access, $menu_id, $in_view=false)
    {
        if (session()->get('lock')==1) {
            $url=url('/lock');
            echo "<script>window.location.href='$url'</script>";
        }
        if (session()->get('alias') != 'superadmin') {
            $query = AccessModel::where($access, 1)->where('menu_id', $menu_id)->where('group_id', session()->get('group_id'))->count();
            if ($query > 0) {
                if ($in_view) {
                    return true;
                }
            } else {
                if ($in_view) {
                    return false;
                } else {
                    // Helpers::flashMsg(__('login.Error'),__('access.You are not granted to access this Menu'),'error');
                    // return redirect("admin");
                    return abort(404);
                }

            }
        } else {
            return true;
        }

    }
}
