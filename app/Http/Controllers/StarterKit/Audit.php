<?php

namespace App\Http\Controllers\StarterKit;
use App\Http\Controllers\Controller;
use App\Libraries\Helpers;

use App\Http\Requests;
use App\Models\AuditModel;
use App\Models\UsersModel;
use Illuminate\Http\Request;


class Audit extends Controller
{
    protected $data = array();

    public function __construct()
    {
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->menuID = 7;
        $this->urlModule = '/logactivity';
        $this->viewFile = 'starter_kit.logactivity';
        $this->title = 'Log Activity';
        $this->model = new AuditModel();
        $this->user = new UsersModel();
    }

    public function data()
    {
        Access::grant_check('is_read', $this->menuID);
        $data['access'] = new Access;
        $data['menuID']=$this->menuID;
        $data['title'] = __("menu.{$this->title}");
        $data['urlModule']=$this->urlModule;
        $data['column'] = json_encode(array(
            array('data' => 'id'),
            array('data' => 'ip_address'),
            array('data' => 'name'),
            array('data' => 'module'),
            array('data' => 'task'),
            array('data' => 'note'),
            array('data' => 'log_date')
        ));
        $data['delBtn'] = Helpers::del_button("$this->urlModule/batch_action");
        return view($this->viewFile, $data);
    }

    public function ajax_data(Request $req)
    {
        Access::grant_check('is_read', $this->menuID);
        $access = new Access;
        $data = AuditModel::take(300)->orderBy('logs_id','desc')->get();
        $no = 1;
        $records["draw"] = 1;
        $records["recordsTotal"] = count($data);
        $records["recordsFiltered"] = count($data);
        $records["data"] = [];
        foreach ($data as $key) {
            $user = $this->user->getRow($key->user_id);
            $nama = '';
            if ($user) {
                $nama = $user->first_name . ' ' . $user->last_name;
            }
            $records["data"][] = array(
                "DT_RowId" => "data_" . $key->logs_id,
                'id' => $key->logs_id,
                'ip_address' => $key->ipaddress,
                'name' => $nama,
                'module' => $key->module,
                'task' => $key->task,
                'note' => $key->note,
                'log_date' => session()->get('language')=='id' ? Helpers::konversi_tanggal($key->updated_at,'l, j F Y') : date('l, j F Y',strtotime($key->updated_at)),
            );
            $no++;
        }
        // header('Content-Type: application/json');
        if ($req->ajax()) {
            echo json_encode($records);
        } else {
            return redirect($this->urlModule);
        }
    }

    public function delete($id)
    {
        Access::grant_check('is_delete', $this->menuID);
        if (AuditModel::where('logs_id', $id)->delete()) {
            \Session::flash('message', '<strong>Success</strong>Data successfully deleted');
            \Session::flash('type', 'success');
        } else {
            \Session::flash('message', '<strong>Error</strong>Delete Data Failed');
            \Session::flash('type', 'error');
        }
        return redirect($this->urlModule);
    }


    public function batch_action(Request $req)
    {
        if ($req->action == 'Delete') {
            Access::grant_check('is_delete', $this->menuID);
            $id = json_decode($req->id_item);
            foreach ($id as $key) {
                AuditModel::where('logs_id', $key->id_item)->delete();
            }
            Helpers::flashMsg(__('login.Success'),__('dashboard.success_delete'),'success');        
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_delete'),'error');        
        }
        return redirect($this->urlModule);
    }
}