<?php

namespace App\Http\Controllers\StarterKit;
use App\Http\Controllers\Controller;
use App\Models\BackEnd\BankModel;

use App\Libraries\Helpers;
use App\Http\Requests;
use Illuminate\Http\Request;

use File;
use Storage;

class Dashboard extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->menuID = 1;
        $this->urlModule = 'dashboard';
        $this->viewFile = 'starter_kit.dashboard';
        $this->title = 'Dashboard';
    }

    public function data()
    {
        Access::grant_check('is_read', $this->menuID);
        $data['menuID']=$this->menuID;
        $data['title'] = __("menu.{$this->title}");
        $data['urlModule']=$this->urlModule;
        $data['helpers']=new Helpers();
        $data['dateNow']=Helpers::konversi_tanggal(date("Y-m-d H:i:s"), 'l, j F Y');
        return view($this->viewFile,$data);
    }

    public function lasturl(Request $req)
    {
        session(array('lasturl' => $req->lasturl,'lock' => true));
        echo "true";
    }

    public function lock()
    {
        $data['title'] = __("menu.Lock Screen");
        return view('starter_kit.lock',$data);
    }

    public function unlock(Request $req)
    {
        if (md5(md5($req->password))==session()->get('password')) {
            $lasturl=session()->get('lasturl');
            session(array('lasturl' => '','lock' => false));
            Helpers::flashMsg(__('login.Success'),__('login.Login Success, Welcome back').session()->get('first_name').' '.session()->get('last_name'),'success');
            return redirect($lasturl);
        } else {
            Helpers::flashMsg(__('login.Warning'),__('login.The password you entered is incorrect, or was mistyped'),'warning');
            return redirect('/lock');
        }
    }
}
