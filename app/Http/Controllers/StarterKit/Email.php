<?php
namespace App\Http\Controllers\StarterKit;
use App\Http\Controllers\Controller;
use App\Http\Controllers\StarterKit\Access;
use App\Models\EmailModel;
use App\Models\UsersModel;
use App\Mail\MailProcessing;

use App\Libraries\Helpers;
use App\Http\Requests;
use Illuminate\Http\Request;
use File;
use Storage;
use Mail;

class Email extends Controller
{

    protected $data = array();

    public function __construct()
    {
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->menuID = 103;
        $this->urlModule = '/email';
        $this->viewFile = 'starter_kit.email';
        $this->title = 'Email';

        $this->model = new EmailModel();
    }

    public function data()
    {
        Access::grant_check('is_read',$this->menuID);
        $data['access'] = new Access;
        $data['menuID'] = $this->menuID;
        $data['title'] = __("menu.{$this->title} Log");
        $data['urlModule'] = $this->urlModule;
        $data['delBtn'] = Helpers::del_button("$this->urlModule/batch_action");
        
        $data['column'] = json_encode(array(
            array('data'=>'id'),
            array('data'=>'user'),
            array('data'=>'subject'),
            array('data'=>'type'),
	        array('data'=>'action'),
        ));
        return view($this->viewFile, $data);
    }

    public function ajax_data(Request $req)
    {
        Access::grant_check('is_read', $this->menuID);
        $data=$this->model->get();
        $no = 1;
        $records["draw"] = 1;
        $records["recordsTotal"]=$this->model->count();
        $records["recordsFiltered"]=$this->model->count();
        $records["data"] = [];
        foreach ($data as $key) {
            $action = '';
            if (Access::grant_check('is_update', $this->menuID, true)) {
                $action .= '<a href="' . url("$this->urlModule/view/$key->email_id") . '" class="btn waves-effect waves-light btn-outline-warning btn-sm btn-icon-fixed"><span class="fa fa-edit"></span> '.__("dashboard.Edit").'</a> ';
            }
            $records["data"][] = array(
                "DT_RowId"=>"data_".$key->email_id,
                'id'=>$key->email_id,
                // Specify any columns needed
                'user'=>$key->created_by,
                'subject'=>$key->title,
                'type'=>$key->type,
                'action'=>$action,
            );
            $no++;
        }
        // header('Content-Type: application/json');
        if ($req->ajax()) {
            echo json_encode($records);
        } else {
            return redirect($this->urlModule);
        }
    }

    public function view($id=0)
    {
        $data['urlModule']=$this->urlModule;
        $data['information']=[];
        $data['id']=$id;
        $data['user']=UsersModel::get();
        if ($id==0) {
            Access::grant_check('is_create',$this->menuID);
            $data['title']=__("email.Add $this->title");
        } else {
            Access::grant_check('is_update', $this->menuID);
            $data['information'] = $this->model->getRow($id);
            $name=$data['information']->title;
            $data['title']=__("email.Edit $this->title")." - $name";
        }
        return view("{$this->viewFile}_view",$data);
    }

    public function save(Request $request, $id)
    {
        $this->validate($request, [
            // Specify any fields needed
            'user_id' => 'required',
			'title' => 'required',
			'type' => 'required',
			'msg' => 'required',
        ]);
        $data = $request->except(['_token']);
        if ($data) {
            if ($id == 0) {
                Access::grant_check('is_create', $this->menuID);
                $data['created_by']=session()->get('first_name').' '.session()->get('last_name');
                $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $data['created_at'] = date('Y-m-d H:i:s');
                $result = $this->model->saveRow($data);
                if ($result) {
	                $user = UsersModel::find($request->user_id);
                	$mail['title'] = 'Akasah.id';
			    	$mail['name'] = "$user->first_name $user->last_name";
			    	$mail['type'] = $request->type;
			    	$mail['msg'] = $request->msg;
			    	Mail::to($user->email)->send(new MailProcessing($mail,$request->title));

                    Helpers::auditTrail($request, sprintf(__('dashboard.log_add'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_save'),'success');
                    return redirect("$this->urlModule");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_save'),'error');
                    return redirect("$this->urlModule/new")->withInput();
                }
            } else {
                Access::grant_check('is_update', $this->menuID);
                Helpers::flashMsg(__('login.Error'),__('dashboard.failed_update'),'error');
                return redirect("$this->urlModule/view/$id")->withInput();
	            // $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
             //    $result = $this->model->saveRow($data, $id);
             //    if ($result) {
             //        Helpers::auditTrail($request, sprintf(__('dashboard.log_edit'), $result));
             //        Helpers::flashMsg(__('login.Success'),__('dashboard.success_update'),'success');
             //        return redirect("$this->urlModule");
             //    } else {
             //        Helpers::flashMsg(__('login.Error'),__('dashboard.failed_update'),'error');
             //        return redirect("$this->urlModule/view/$id")->withInput();
             //    }
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.no_request'),'error');
            return redirect($this->urlModule)->withInput();
        }
    }

    public function delete($req, $id)
    {
        Access::grant_check('is_delete', $this->menuID);
        if ($this->model->where('email_id', $id)->delete()) {
            Helpers::auditTrail($req, sprintf(__('dashboard.log_delete'),$id));
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_delete'),'error');        
        }
        return redirect($this->urlModule);
    }

    public function batch_action(Request $req)
    {
      if ($req->action=='Delete') {
            Access::grant_check('is_delete', $this->menuID);
            $id=json_decode($req->id_item);
            if (count($id)>1) {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_bulk'),'success');        
            } else {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_delete'),'success');        
            }
            foreach ($id as $key) {
                $this->delete($req, $key->id_item);
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_bulk'),'error');
        }
        return redirect($this->urlModule);
    }

    public function sendMail($toUser,$subject,$msg,$type,$cc='')
    {
    	$user = UsersModel::find($toUser);
    	$mail_log['user_id'] = $user->user_id;
    	$mail_log['title'] = $subject ;
    	$mail_log['type'] = $type ;
    	$mail_log['msg'] = $msg ;
    	$mail_log['created_by']="$user->first_name $user->last_name";
        $mail_log['updated_by']="$user->first_name $user->last_name";
        $mail_log['created_at'] = date('Y-m-d H:i:s');
    	$result=$this->model->saveRow($mail_log);
    	if ($result) {
	    	$data['title'] = 'Akasah.id';
            $last_name = $user->last_name == '' ? " $user->last_name" : "";
	    	$data['name'] = "$user->first_name ".$last_name;
	    	$data['type'] = $type;
	    	$data['msg'] = $msg;
            Mail::to($user->email)->send(new MailProcessing($data,$subject));
            if ($cc!='') {
    	    	Mail::to($cc)->send(new MailProcessing($data,$subject));
            }
    	} else {
    		Helpers::flashMsg(__('login.Error'),__('dashboard.no_request'),'error');
    	}
    }
}