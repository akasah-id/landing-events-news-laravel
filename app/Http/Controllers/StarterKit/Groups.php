<?php

namespace App\Http\Controllers\StarterKit;
use App\Http\Controllers\Controller;
use App\Models\GroupsModel;

use App\Libraries\Helpers;
use App\Http\Requests;
use Illuminate\Http\Request;

class Groups extends Controller
{
    protected $data = array();

    public function __construct()
    {
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->menuID = 5;
        $this->urlModule = '/groups';
        $this->viewFile = 'starter_kit.group';
        $this->title = 'Group';

        $this->model = new GroupsModel();
    }

    public function data()
    {
        Access::grant_check('is_read',$this->menuID);
        $data['access']=new Access;
        $data['menuID']=$this->menuID;
        $data['title'] = __("menu.{$this->title} Management");
        $data['urlModule']=$this->urlModule;
        $data['delBtn'] = Helpers::del_button("$this->urlModule/batch_action");
        
        $data['column']=json_encode(array(
            array('data'=>'id'),
            array('data'=>'name'),
            array('data'=>'action')
        ));
        return view($this->viewFile, $data);
    }

    public function ajax_data(Request $req)
    {
        Access::grant_check('is_read', $this->menuID);
        $data = $this->model->where('deleted','=',0)->where('alias','<>','superadmin')->get();
        $no = 1;
        $records["draw"] = 1;
        $records["recordsTotal"] = count($data);
        $records["recordsFiltered"] = count($data);
        $records["data"]=[];
        foreach ($data as $key) {
            $action = '';
            if (Access::grant_check('is_update', $this->menuID, true)) {
                $action .= '<a href="' . url($this->urlModule.'/view/' . $key->group_id) . '" class="btn waves-effect waves-light btn-outline-warning btn-sm btn-icon-fixed"><span class="fa fa-edit"></span> '.__("dashboard.Edit").'</a> ';
                $action .= '<a href="' . url('/access/' . $key->group_id) . '" class="btn waves-effect waves-light btn-outline-info btn-sm btn-icon-fixed"><span class="fa fa-key"></span> '.__("dashboard.Access").'</a> ';
            }
            $records["data"][] = array(
                "DT_RowId"=>"data_".$key->group_id,
                'id'=>$key->group_id,
                'name'=>$key->name,
                'action' => $action,
            );
            $no++;
        }
        // header('Content-Type: application/json');
        if ($req->ajax()) {
            echo json_encode($records);
        } else {
            return redirect($this->urlModule);
        }
    }

    public function view($id=0)
    {
        $data['urlModule']=$this->urlModule;
        $data['information']=[];
        $data['title_edit']='';
        $data['id']=$id;
        if ($id==0) {
            Access::grant_check('is_create',$this->menuID);
            $data['title']=__("group.Add $this->title");
        } else {
            Access::grant_check('is_update',$this->menuID);
            $data['information'] = $this->model->getRow($id);
            $name=$data['information']->name;
            $data['title']=__("group.Edit $this->title")." - $name";
        }
        return view($this->viewFile.'_view',$data);
    }

    public function save(Request $request, $id)
    {
        $data = $request->except(['_token']);
        if($data){
            $data['alias'] = Helpers::slugify($data['name']);
            if($id == 0){
                $data['created_by']=session()->get('first_name').' '.session()->get('last_name');
                $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $data['created_at'] = date('Y-m-d H:i:s');
                Access::grant_check('is_create', $this->menuID);
                $result = $this->model->saveRow($data);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_add'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_save'),'success');
                    return redirect($this->urlModule);
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_save'),'error');
                    return redirect($this->urlModule.'/view')->withInput();
                }
            } else {
                Access::grant_check('is_update', $this->menuID);
                $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $result = $this->model->saveRow($data, $id);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_edit'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_update'),'success');
                    return redirect($this->urlModule);
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_update'),'error');
                    return redirect($this->urlModule.'/view')->withInput();
                }
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.no_request'),'error');
            return redirect($this->urlModule.'/view')->withInput();
        }
    }

    public function batch_action(Request $req)
    {
        if ($req->action=='Delete') {
            Access::grant_check('is_delete', $this->menuID);
            $id=json_decode($req->id_item);
            if (count($id)>1) {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_bulk'),'success');        
            } else {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_delete'),'success');        
            }
            foreach ($id as $key) {
                $result = $this->model->where('group_id', $key->id_item)->update(array('deleted' => 1));
                if ($result) {
                    Helpers::auditTrail($req, sprintf(__('dashboard.log_delete'),$key->id_item));
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_delete'),'error');        
                }
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_bulk'),'error');
        }
        return redirect($this->urlModule);
    }
}
