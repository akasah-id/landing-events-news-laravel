<?php

namespace App\Http\Controllers\StarterKit;
use App\Http\Controllers\Controller;
use App\Http\Controllers\StarterKit\Email;
use App\Libraries\Helpers;

use App\Http\Requests;
use App\Models\GroupsModel;
use App\Models\UsersModel;
use Illuminate\Http\Request;

class Login extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->email = new Email();
    }

    public function changeLang($lang)
    {
        Helpers::setLanguage($lang);
    }

    public function login()
    {
        if (session()->has('logged_in')) {
            return redirect('/dashboard');
        } else {
            return view('starter_kit.login');
        }
    }

    public function process(Request $req) {
        $dtuser = UsersModel::where('email', $req->email)->first();

        if (empty($dtuser)) {
            Helpers::flashMsg(__('login.Error'),__('login.We could not find your email address'),'warning');
            return redirect('login');
        } else {
            $dtpassword = $dtuser->password;
            $current_pass = md5(md5($req->password));

            if ($dtpassword === $current_pass) {
                if ($dtuser->active == '0') {
                    Helpers::flashMsg(__('login.Warning'),__('login.Your account is not active'),'warning');
                    return redirect('login');
                } else {
                    $dtgrp=GroupsModel::where('group_id', $dtuser->group_id)->first();
                    $dtuser = array(
                        'user_id' => $dtuser->user_id, 
                        'username' => $dtuser->username, 
                        'password' => $dtuser->password, 
                        'first_name' => $dtuser->first_name, 
                        'last_name' => $dtuser->last_name, 
                        'email' => $dtuser->email, 
                        'avatar' => $dtuser->avatar, 
                        'intro' => $dtuser->intro, 
                        'group_id' => $dtuser->group_id, 
                        'group_name' => $dtgrp->name,
                        'alias' => $dtgrp->alias,
                        'logged_in' => true,
                        'language' => $dtuser->lang,
                        'lasturl' => "",
                        'lock' => false,
                        // FileManager
                        'id' => $dtuser->user_id, 
                        );
                    session($dtuser);
                    Helpers::flashMsg(__('login.Success'),__('login.Login Success, Welcome back').session()->get('first_name').' '.session()->get('last_name'),'success');
                    return redirect('/dashboard');
                }
            } else {
                Helpers::flashMsg(__('login.Warning'),__('login.The password you entered is incorrect, or was mistyped'),'warning');
                return redirect('login');
            }
        }
    }

    public function reset(Request $req) {
        $dtuser = UsersModel::where('email', $req->email)->first();

        if (!isset($dtuser->user_id)) {
            Helpers::flashMsg(__('login.Error'),__('login.We could not find your email address'),'warning');
            return redirect('login');
        } else {
            $data = UsersModel::find($dtuser->user_id);
            if($data) {
                $password=Helpers::randomKey(6);
                $data->password=md5(md5($password));
                $data->save();
                // Mail
                $email = $data->email;
                $pass = $password;
                $msg = __('email.forgotpassmsg')."<br><br>".__('email.password').": <strong>$pass</strong>";
                $this->email->sendMail($dtuser->user_id,__('email.forgotpass'),$msg,'forgotpass');

                Helpers::flashMsg(__('login.Success'),__('login.Recovery password instruction has been sent'),'success');
                return redirect('login');
            }
        }
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            // Specify any fields needed
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        $data = $request->except(['_token']);
        $this->model = new UsersModel();
        $redir = false;
        if (!is_null($this->model->select('username')->where('username','=',$request->username)->first())) {
            Helpers::flashMsg(__('login.Error'),__('user.failed_username'),'error');
            $redir = true;
        }
        if (!is_null($this->model->select('email')->where('email','=',$request->email)->first())) {
            Helpers::flashMsg(__('login.Error'),__('user.failed_email'),'error');
            $redir = true;
        }
        if ($redir) {
            return redirect("login")->withInput();
        }
        $data['password'] = md5(md5($data['password']));
        $data['created_by']="$request->first_name $request->last_name";
        $data['updated_by']="$request->first_name $request->last_name"; 
        $data['created_at'] = date('Y-m-d H:i:s');
        $result = $this->model->saveRow($data);
        if ($result) {
            Helpers::auditTrail($request, sprintf(__('dashboard.log_add'), $result));
            Helpers::flashMsg(__('login.Success'),__('login.signup_success'),'success');
            // Mail
            // $email = $request->email;
            // $link = '';
            // $linktext = '';
            // $msg = '<strong><h2>'.__('email.welcomehead')."</h2></strong><br><br>".__('email.welcomemsg')."<br><br>".__('email.password').": <strong><a href='$link' target='_blank'>$linktext</a></strong>";
            // $this->email->sendMail($dtuser->user_id,__('email.forgotpass'),$msg,'forgotpass');
            return redirect("login");
        } else {
            Helpers::flashMsg(__('login.Error'),__('login.signup_failed'),'error');
            return redirect("login")->withInput();
        }
    }

    public function registerConfirm($id,$token)
    {
        $this->model = new UsersModel();
        if (!is_null($this->model->where('user_id','=',$id)->where('active','=',1)->first())) {
            Helpers::flashMsg(__('login.Success'),__('login.confirm_already'),'success');
            return redirect("login");
        }
        if (!is_null($this->model->where('user_id','=',$id)->where('password','=',$token)->first())) {
            $result = $this->model->where('user_id', $id)->update(array('active' => 1,'group_id' => 2));
            if ($result) {
                Helpers::flashMsg(__('login.Success'),__('login.confirm_success'),'success');
                return redirect("login");
            } else {
                Helpers::flashMsg(__('login.Error'),__('login.signup_failed'),'error');
                return redirect("login");
            }
            
        } else {
            Helpers::flashMsg(__('login.Error'),__('login.wrong_code'),'error');
            return redirect("login");
        }
    }

    public function logout(Request $req) {
        $req->session()->flush();
        return redirect('/');
    }
}
