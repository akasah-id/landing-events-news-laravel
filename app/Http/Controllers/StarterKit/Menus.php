<?php

namespace App\Http\Controllers\StarterKit;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Libraries\Helpers;
use App\Models\AccessModel;
use App\Models\MenusModel;
use Illuminate\Http\Request;

class Menus extends Controller
{
    protected $data = array();

    public function __construct()
    {
        $this->model = new MenusModel();
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->menuID = 3;
        $this->urlModule = '/menu';
        $this->viewFile = 'starter_kit.menu';
        $this->title = 'Menu';
    }

    public function data()
    {
        Access::grant_check('is_read',$this->menuID);
        $data['menu_id']=$this->menuID;
        $data['access']=new Access;
        $data['title'] = __("menu.{$this->title} Management");
        $data['urlModule']=$this->urlModule;
        $data['menus'] = $this->generatemenu();
        return view($this->viewFile, $data);
    }

    public function view($id=0)
    {
        $data['urlModule']=$this->urlModule;
        $data['information']=[];
        $data['title_edit']='';
        $data['id']=$id;
        if ($id==0) {
            Access::grant_check('is_create',$this->menuID);
            $data['title']=__("menu.Add $this->title");
        } else {
            Access::grant_check('is_update',$this->menuID);
            $data['information']=MenusModel::where('menu_id', $id)->first();
            $name=$data['information']->menu_name;
            $data['title']=__("menu.Edit $this->title")." - $name";
        }
        return view($this->viewFile.'_view',$data);
    }

    public function save(Request $request, $id)
    {
        $data = $request->except(['_token']);
        if ($data) {
            if ($id == 0) {
                $data['created_at'] = date('Y-m-d H:i:s');
                Access::grant_check('is_create', $this->menuID);
                $result = $this->model->saveRow($data);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_add'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_save'),'success');
                    return redirect($this->urlModule);
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_save'),'error');
                    return redirect($this->urlModule.'/new')->withInput();
                }
            } else {
                Access::grant_check('is_update', $this->menuID);
                $result = $this->model->saveRow($data, $id);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_edit'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_update'),'success');
                    return redirect($this->urlModule);
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_update'),'error');
                    return redirect($this->urlModule.'/view/'.$id)->withInput();
                }
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.no_request'),'error');
            return redirect($this->urlModule)->withInput();
        }
    }

    public function delete(Request $request, $id)
    {
        Access::grant_check('is_delete',$this->menuID);
        $result=MenusModel::where('menu_id', $id)->delete();
        if ($result) {
            Helpers::auditTrail($request, sprintf(__('dashboard.log_delete'),$id));
            Helpers::flashMsg(__('login.Success'),__('dashboard.success_delete'),'success');        
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_delete'),'error');        
        }
        return redirect($this->urlModule);
    }

    public function parsingdata($dec,$menuorder=0,$level=0){
        foreach ($dec as $key) {
            $menuorder++;
            MenusModel::where('menu_id', $key->id)->update(['ordering'=>$menuorder]);
            // $this->db->query('update menu set urutan='.$menuorder.' where id='.$key->id.' ');
            //echo 'reorder menu ID: '.$key['id'].' menjadi '.$menuorder.'<br/>';

            if(isset($key->children)){
                foreach ($key->children as $keychild) {
                    MenusModel::where('menu_id', $keychild->id)->update(['parent_id'=>$key->id]);
                    // $this->db->query('update menu set parent_id='.$key->id.' where id='.$keychild->id.'');
                    //echo "menu ID: ".$keychild['id']." Menjadi Child dari ID menu : ".$key['id'].'<br/>';
                }
                $this->parsingdata($key->children,0,$level+1);
            }
            if($level==0){
                MenusModel::where('menu_id', $key->id)->update(['parent_id'=>0]);
                // $this->db->query('update menu set parent_id=0 where id='.$key->id.'');
                //echo 'parent_id menu ID: '.$key['id'].' menjadi 0 <br/>';
            }
        }
    }

    public function reposition()
    {
        $decoded=json_decode($_POST['menu']);
        $this->parsingdata($decoded);
        echo json_encode(array("stat"=>true));
        Helpers::flashMsg(__('menu.Success'),__('menu.Data successfully updated'),'success');
    }

    public function generatemenu()
    {
        $nav=MenusModel::orderBy('ordering', 'asc')->get();
        if(count($nav)>0){
            $html = '<ol class="dd-list dd-nodrag">';
            foreach($nav as $page){
                        if ($page->parent_id==0) {
                            $aksi='
                           <div style="float:right"><a href="' . url($this->urlModule."/view/" . $page->menu_id) . '"><button type="button" class="btn btn-xs btn-rounded btn-warning"><span class="fa fa-edit" aria-hidden="true"></span></button></a>
                            <button onclick="item_delete(\'' . url($this->urlModule."/delete/" . $page->menu_id) . '\')" type="button" class="btn btn-xs btn-rounded btn-danger hapus"><span class="fa fa-trash" aria-hidden="true"></span></button>
                            </div>';
                            $url=($page->url!="")?" | ".$page->url:"";
                            $html .= '<li class="dd-item" data-id="' .$page->menu_id. '">';
                            $html .= '<div class="dd-handle">' .'('.$page->menu_id.') ' . __('menu.'.$page->menu_name) . $url . $aksi . '</div>';
                            $get_child=MenusModel::where('parent_id',$page->menu_id)->orderBy('ordering', 'asc')->get();
                            $html .= $this->generatechild($get_child);
                            $html .= '</li>';
                }
            }
            $html .= '</ol>';
            return $html;
        }
    }

    public function generatechild($nav)
    {
        if(count($nav)>0){
            $html = '<ol class="dd-list dd-nodrag">';
            foreach($nav as $page){
                if ($page->parent_id!=0) {
                    $aksi = '<div style="float:right"><a href="' . url($this->urlModule."/view/" . $page->menu_id) . '"><button type="button" class="btn btn-xs btn-rounded btn-warning"><span class="fa fa-edit" aria-hidden="true"></span></button></a>
                            <button onclick="item_delete(\'' . url($this->urlModule."/delete/" . $page->menu_id) . '\')" type="button" class="btn btn-xs btn-rounded btn-danger hapus"><span class="fa fa-trash" aria-hidden="true"></span></button>
                            </div>';
                    $url=($page->url!="")?" | ".$page->url:"";
                    $html .= '<li class="dd-item" data-id="' .$page->menu_id. '">';
                    $html .= '<div class="dd-handle">' .'('.$page->menu_id.') ' .  __('menu.'.$page->menu_name) . $url . $aksi . '</div>';
                    $get_child=MenusModel::where('parent_id',$page->menu_id)->orderBy('ordering', 'asc')->get();
                    $html .= $this->generatechild($get_child);
                    $html .= '</li>';
                }
            }
            $html .= '</ol>';
            return $html;
        }
    }

    public function client_generatemenu()
    {
        $nav=MenusModel::orderBy('ordering', 'asc')->get();
        if(count($nav)>0){
            $html = '';
            foreach($nav as $page){
                if (session()->get('alias') != 'superadmin') {
                    $access_check = AccessModel::where('group_id', session()->get('group_id'))->where('menu_id', $page->menu_id)->where('is_read', 1)->count();
                    if ($access_check > 0) {
                        if ($page->parent_id == 0) {
                            $query_child = MenusModel::where('parent_id', $page->menu_id)->orderBy('ordering', 'asc');
                            $get_child = $query_child->get();
                            $num_child = $query_child->count();
                            if (($page->menu_type == 'Internal')) {
                                $url = url("$page->module");
                            } else {
                                $url = url("$page->url");
                            }
                            if ($num_child > 0) {
                                $page->menu_icons == "" ? $page->menu_icons = "fa-plus-circle" : $page->menu_icons = $page->menu_icons;
                                $link = 'onclick="window.location.href=\'' . $url . '\'"';
                                if (substr($page->module, 0, 3) == 'nav') {
                                    $link = '';
                                }
                                $html .= '<li class="' . $page->module . '">
                                            <a href="javascript:void(0)" ' . $link . ' class="waves-effect waves-dark">
                                            <i class="fa ' . $page->menu_icons . '"></i><span class="hide-menu"> ' . __('menu.'.$page->menu_name) . '</span></a>
                                            ';
                            } else {
                                $page->menu_icons == "" ? $page->menu_icons = "fa-plus-circle" : $page->menu_icons = $page->menu_icons;
                                $html .= '<li class="' . $page->module . '">
                                            <a href="' . $url . '" class="waves-effect waves-dark">
                                            <i class="fa ' . $page->menu_icons . '"></i><span class="hide-menu"> ' . __('menu.'.$page->menu_name) . '</span></a>
                                            ';
                            }
                            $html .= $this->client_generatechild(2, $get_child);
                            $html .= '</li>';
                        }
                    }
                } else {
                    if ($page->parent_id == 0) {
                        $query_child = MenusModel::where('parent_id', $page->menu_id)->orderBy('ordering', 'asc');
                        $get_child = $query_child->get();
                        $num_child = $query_child->count();
                        if (($page->menu_type == 'Internal')) {
                            $url = url("$page->module");
                        } else {
                            $url = url("$page->url");
                        }
                        if ($num_child > 0) {
                            $page->menu_icons == "" ? $page->menu_icons = "fa-plus-circle" : $page->menu_icons = $page->menu_icons;
                            $link = 'onclick="window.location.href=\'' . $url . '\'"';
                            if (substr($page->module, 0, 3) == 'nav') {
                                $link = '';
                            }
                            $html .= '<li class="' . $page->module . '">
                                            <a href="javascript:void(0)" ' . $link . ' class="has-arrow waves-effect waves-dark" aria-expanded="false">
                                            <i class="fa ' . $page->menu_icons . '"></i><span class="hide-menu"> ' . __('menu.'.$page->menu_name) . '</span></a>
                                            ';
                        } else {
                            $page->menu_icons == "" ? $page->menu_icons = "fa-plus-circle" : $page->menu_icons = $page->menu_icons;
                            $html .= '<li class="' . $page->module . '">
                                            <a href="' . $url . '" class="waves-effect waves-dark">
                                            <i class="fa ' . $page->menu_icons . '"></i><span class="hide-menu"> ' . __('menu.'.$page->menu_name) . '</span></a>
                                            ';
                        }
                        $html .= $this->client_generatechild(2, $get_child);
                        $html .= '</li>';
                    }
                }
            }
            $html .= '';
            return $html;
        }
    }

    public function client_generatechild($level,$nav)
    {
        if(count($nav)>0){
            if ($level==2) {
                $html = '<ul aria-expanded="false" class="collapse">';
                foreach ($nav as $page) {
                    if (session()->get('alias') != 'superadmin') {
                        $access_check = AccessModel::where('group_id', session()->get('group_id'))->where('menu_id', $page->menu_id)->where('is_read', 1)->count();
                        if ($access_check > 0) {
                            if ($page->parent_id != 0) {
                                $query_child = MenusModel::where('parent_id', $page->menu_id)->orderBy('ordering', 'asc');
                                $get_child = $query_child->get();
                                $num_child = $query_child->count();
                                if (($page->menu_type == 'Internal')) {
                                    $url = url("$page->module");
                                } else {
                                    $url = url("$page->url");
                                }
                                if ($num_child > 0) {
                                    $page->menu_icons == "" ? $page->menu_icons = "fa-plus-circle" : $page->menu_icons = $page->menu_icons;
                                    $link = 'onclick="window.location.href=\'' . $url . '\'"';
                                    if (substr($page->module, 0, 9) == '/nav') {
                                        $link = '';
                                    }
                                    $html .= '<li class="dropdown">
                                                    <a href="javascript:void(0)" ' . $link . ' class="' . $page->module . ' has-arrow waves-effect waves-dark" aria-expanded="false">
                                                    <i class="fa ' . $page->menu_icons . '"></i><span class="hide-menu"> ' . __('menu.'.$page->menu_name) . '</span></a>
                                                    ';
                                    $html .= $this->client_generatechild(3, $get_child);
                                } else {
                                    $page->menu_icons == "" ? $page->menu_icons = "fa-plus-circle" : $page->menu_icons = $page->menu_icons;
                                    $html .= '<li>
                                                    <a href="' . $url . '" class="' . $page->module . '">
                                                    <i class="fa ' . $page->menu_icons . '"></i><span class="hide-menu"> ' . __('menu.'.$page->menu_name) . '</span></a>
                                                    ';
                                    $html .= $this->client_generatechild(2, $get_child);
                                }
                                $html .= '</li>';
                            }
                        }
                    } else {
                        if ($page->parent_id != 0) {
                            $query_child = MenusModel::where('parent_id', $page->menu_id)->orderBy('ordering', 'asc');
                            $get_child = $query_child->get();
                            $num_child = $query_child->count();
                            if (($page->menu_type == 'Internal')) {
                                $url = url("$page->module");
                            } else {
                                $url = url("$page->url");
                            }
                            if ($num_child > 0) {
                                $page->menu_icons == "" ? $page->menu_icons = "fa-plus-circle" : $page->menu_icons = $page->menu_icons;
                                $link = 'onclick="window.location.href=\'' . $url . '\'"';
                                if (substr($page->module, 0, 9) == '/nav') {
                                    $link = '';
                                }
                                $html .= '<li class="dropdown">
                                                    <a href="javascript:void(0)" ' . $link . ' class="' . $page->module . ' has-arrow waves-effect waves-dark" aria-expanded="false">
                                                    <i class="fa ' . $page->menu_icons . '"></i><span class="hide-menu"> ' . __('menu.'.$page->menu_name) . '</span></a>
                                                    ';
                                $html .= $this->client_generatechild(3, $get_child);
                            } else {
                                $page->menu_icons == "" ? $page->menu_icons = "fa-plus-circle" : $page->menu_icons = $page->menu_icons;
                                $html .= '<li>
                                                    <a href="' . $url . '" class="' . $page->module . '">
                                                    <i class="fa ' . $page->menu_icons . '"></i> ' . __('menu.'.$page->menu_name) . '</a>
                                                    ';
                                $html .= $this->client_generatechild(2, $get_child);
                            }
                            $html .= '</li>';
                        }
                    }
                }
                $html .= '</ul>';
                return $html;
            } else {
                $html = '<ul class="dropdown-menu">';
                foreach($nav as $page){
                    if (session()->get('alias') != 'superadmin') {
                        $access_check = AccessModel::where('group_id', session()->get('group_id'))->where('menu_id', $page->menu_id)->where('is_read', 1)->count();
                        if ($access_check>0) {
                            if ($page->parent_id!=0) {
                                $query_child=MenusModel::where('parent_id',$page->menu_id)->orderBy('ordering', 'asc');
                                $get_child=$query_child->get();
                                $num_child=$query_child->count();
                                if (($page->menu_type=='Internal')) {
                                    $url=url("$page->module");
                                } else {
                                    $url=url("$page->url");
                                }
                                if ($num_child>0) {
                                    $page->menu_icons=="" ? $page->menu_icons="fa-plus-circle" : $page->menu_icons=$page->menu_icons;
                                    $link='onclick="window.location.href=\''.$url.'\'"';
                                    if (substr($page->module,0,3)=='nav') {
                                        $link='';
                                    }
                                    $html .= '<li class="dropdown">
                                                    <a class="' . $page->module . ' has-arrow waves-effect waves-dark" aria-expanded="false" href="javascript:void(0)" '.$link.'>
                                                    <i class="fa '.$page->menu_icons.'"></i> '.__('menu.'.$page->menu_name).'</a>
                                                    ';
                                } else {
                                    $page->menu_icons=="" ? $page->menu_icons="fa-plus-circle" : $page->menu_icons=$page->menu_icons;
                                    $html .= '<li>
                                                    <a href="'.$url.'" class="'.$page->module.'">
                                                    <i class="fa '.$page->menu_icons.'"></i> '.__('menu.'.$page->menu_name).'</a>
                                                    ';
                                    $html .= $this->client_generatechild(3,$get_child);
                                }
                                $html .= '</li>';
                            }
                        }
                    } else {
                        if ($page->parent_id!=0) {
                            $query_child=MenusModel::where('parent_id',$page->menu_id)->orderBy('ordering', 'asc');
                            $get_child=$query_child->get();
                            $num_child=$query_child->count();
                            if (($page->menu_type=='Internal')) {
                                $url=url("$page->module");
                            } else {
                                $url=url("$page->url");
                            }
                            if ($num_child>0) {
                                $page->menu_icons=="" ? $page->menu_icons="fa-plus-circle" : $page->menu_icons=$page->menu_icons;
                                $link='onclick="window.location.href=\''.$url.'\'"';
                                if (substr($page->module,0,3)=='nav') {
                                    $link='';
                                }
                                $html .= '<li class="dropdown">
                                                <a class="' . $page->module . ' has-arrow waves-effect waves-dark" aria-expanded="false" href="javascript:void(0)" '.$link.'>
                                                <i class="fa '.$page->menu_icons.'"></i> '.__('menu.'.$page->menu_name).'</a>
                                                ';
                            } else {
                                $page->menu_icons=="" ? $page->menu_icons="fa-plus-circle" : $page->menu_icons=$page->menu_icons;
                                $html .= '<li>
                                                <a href="'.$url.'" class="'.$page->module.'">
                                                <i class="fa '.$page->menu_icons.'"></i> '.__('menu.'.$page->menu_name).'</a>
                                                ';
                                $html .= $this->client_generatechild(3,$get_child);
                            }
                            $html .= '</li>';
                        }
                    }
                }
                $html .= '</ul>';
                return $html;
            }
            
        }
    }
}
