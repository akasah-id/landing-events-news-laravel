<?php
namespace App\Http\Controllers\Wazzap;
use App\Http\Controllers\Controller;
use App\Http\Controllers\StarterKit\Access;
use App\Models\Wazzap\EventModel;
use App\Models\UsersModel;

use App\Libraries\Helpers;
use App\Http\Requests;
use Illuminate\Http\Request;
use File;
use Storage;

class Event extends Controller
{

    protected $data = array();

    public function __construct()
    {
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->menuID = 116;
        $this->urlModule = 'event';
        $this->viewFile = 'wazzap.event';
        $this->title = 'Events';

        $this->model = new EventModel();
        $this->model_user = new UsersModel();
    }

    public function data()
    {
        Access::grant_check('is_read',$this->menuID);
        $data['access'] = new Access;
        $data['menuID'] = $this->menuID;
        $data['title'] = __("menu.{$this->title} Management");
        $data['urlModule'] = $this->urlModule;
        $data['delBtn'] = Helpers::del_button("$this->urlModule/batch_action");
        
        $data['column'] = json_encode(array(
            array('data'=>'id'),
            array('data'=>'event_title'),
            array('data'=>'event_date'),
            array('data'=>'event_location'),
            array('data'=>'event_participant'),
	        array('data'=>'action'),
        ));
        return view($this->viewFile, $data);
    }

    public function ajax_data(Request $req)
    {
        Access::grant_check('is_read', $this->menuID);
        $data=$this->model->select(\DB::raw('wazzap_event.*, tb_users.username as u_username, tb_users.first_name as u_first_name, tb_users.last_name as u_last_name'))->join('tb_users','wazzap_event.event_creator','tb_users.user_id')->where('deleted','=',0)->get();
        $no = 1;
        $records["draw"] = 1;
        $records["recordsTotal"]=$this->model->count();
        $records["recordsFiltered"]=$this->model->count();
        $records["data"] = [];
        foreach ($data as $key) {
            $action = '';
            if (Access::grant_check('is_update', $this->menuID, true)) {
                $action .= '<a href="' . url("$this->urlModule/view/$key->event_id") . '" class="btn waves-effect waves-light btn-outline-warning btn-sm btn-icon-fixed"><span class="fa fa-edit"></span> '.__("dashboard.Edit").'</a> ';
            }
            $records["data"][] = array(
                "DT_RowId"=>"data_".$key->event_id,
                'id'=>$key->event_id,
                // Specify any columns needed
                'event_title'=>"
                    <a target='_blank' href='".url('file/images/event/'.$key->event_poster)."' title='Image - ".$key->event_title."'> <i class='fa fa-image'></i> </a>
                    <a href='https://akasah.id/@$key->u_username/$key->event_uri' target='_blank'>$key->event_title</a> by $key->u_first_name $key->u_last_name",
				'event_date' => session()->get('language')=='id' ? Helpers::konversi_tanggal($key->event_datetime,'l, j F Y H:i') : date('l, j F Y H:i',strtotime($key->event_datetime)),
				'event_location' => "<a href='$key->event_gmap_url' target='_blank'>$key->event_city, $key->event_country<br>$key->event_location</a>" ,
				'event_participant' => "<a href='#' onclick='event_participant(\"$key->event_id\")'>".count(json_decode($key->event_participant_join))."/$key->event_participant_target</a>",
                'action'=>$action,
            );
            $no++;
        }
        // header('Content-Type: application/json');
        if ($req->ajax()) {
            echo json_encode($records);
        } else {
            return redirect($this->urlModule);
        }
    }

    public function view($id=0)
    {
        $data['urlModule']=$this->urlModule;
        $data['information']=[];
        $data['id']=$id;
        $data['title_module']="$this->title Management";
        if ($id==0) {
            Access::grant_check('is_create',$this->menuID);
            $data['title']=__("event.Add $this->title");
        } else {
            Access::grant_check('is_update', $this->menuID);
            $data['information'] = $this->model->getRow($id);
            $name=$data['information']->event_title;
            $data['title']=__("event.Edit $this->title")." - $name";
            $data['event_datetime']=date('l j F Y - H:i',strtotime($data['information']->event_datetime));
        }
        return view("{$this->viewFile}_view",$data);
    }

    public function save(Request $request, $id)
    {
        $this->validate($request, [
            // Specify any fields needed
            'event_title' => 'required',
            'event_caption' => 'required',
            'event_desc' => 'required',
            'event_datetime' => 'required',
            'event_gmap_url' => 'required',
            'event_gmap_lat' => 'required',
            'event_gmap_lng' => 'required',
            'event_city' => 'required',
            'event_state' => 'required',
            'event_country' => 'required',
            'event_location' => 'required',
            'event_participant_target' => 'required',
        ]);
        $data = $request->except(['_token']);
        if ($data) {
        	// File Upload
            $getfile = $this->model->find($id);
            if ($request->hasFile('event_image')) {
                if ($id!=0) {
                  $event_image=$getfile->event_image;
                  if ($event_image != '') {
                      @unlink('file/images/event/'.$event_image);
                  }
                }
                $file = $request->file('event_image');
                $filename = date('d-M-Y') . '-' . uniqid() . '-event.' . $data['event_image']->extension();
                $destination = 'file/images/event/';
                if ($file->move($destination, $filename)) {
                    $data['event_image'] = $filename;
                } else {
                    $data['event_image'] = '';
                }
            }
            if ($request->hasFile('event_poster')) {
                if ($id!=0) {
                  $event_poster=$getfile->event_poster;
                  if ($event_poster != '') {
                      @unlink('file/images/event/'.$event_poster);
                  }
                }
                $file = $request->file('event_poster');
                $filename = date('d-M-Y') . '-' . uniqid() . '-event.' . $data['event_poster']->extension();
                $destination = 'file/images/event/';
                if ($file->move($destination, $filename)) {
                    $data['event_poster'] = $filename;
                } else {
                    $data['event_poster'] = '';
                }
            }

            if ($id == 0) {
                Access::grant_check('is_create', $this->menuID);
                $data['event_creator'] = session()->get('user_id');
                $data['event_datetime'] = \DateTime::createFromFormat('l j F Y - H:i', $request->event_datetime)->format('Y-m-d H:i');
                $data['event_uri'] = str_slug($request->event_title,'-').'-'.mt_rand(10,99); ;
                $data['event_participant_join'] = "[]";
                $data['event_participant_not_join'] =  "[]";
                $data['created_by']=session()->get('first_name').' '.session()->get('last_name');
                $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $data['created_at'] = date('Y-m-d H:i:s');
                $result = $this->model->saveRow($data);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_add'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_save'),'success');
                    return redirect("$this->urlModule");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_save'),'error');
                    return redirect("$this->urlModule/new")->withInput();
                }
            } else {
                Access::grant_check('is_update', $this->menuID);
                $data['event_creator'] = session()->get('user_id');
                if ($getfile->event_title != $request->event_title) {
                    $data['news_slug'] = str_slug($request->event_title,'-').'-'.mt_rand(10,99);
                }
                $data['event_datetime'] = \DateTime::createFromFormat('l j F Y - H:i', $request->event_datetime)->format('Y-m-d H:i');
	            $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $result = $this->model->saveRow($data, $id);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_edit'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_update'),'success');
                    return redirect("$this->urlModule");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_update'),'error');
                    return redirect("$this->urlModule/view/$id")->withInput();
                }
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.no_request'),'error');
            return redirect($this->urlModule)->withInput();
        }
    }

    public function delete($req, $id)
    {
        Access::grant_check('is_delete', $this->menuID);
        // Delete File
        $data = $this->model->find($id);
        $event_image=$data->event_image;
        if ($event_image != '') {
            // @unlink('file/images/event/'.$event_image);
        }
        $event_poster=$data->event_poster;
        if ($event_poster != '') {
            // @unlink('file/images/event/'.$event_poster);
        }
        $result = $this->model->where('event_id', $id)->update(array('deleted' => 1));
        if ($result) {
            Helpers::auditTrail($req, sprintf(__('dashboard.log_delete'),$id));
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_delete'),'error');        
        }
        return redirect($this->urlModule);
    }

    public function batch_action(Request $req)
    {
      if ($req->action=='Delete') {
            Access::grant_check('is_delete', $this->menuID);
            $id=json_decode($req->id_item);
            if (count($id)>1) {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_bulk'),'success');        
            } else {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_delete'),'success');        
            }
            foreach ($id as $key) {
                $this->delete($req, $key->id_item);
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_bulk'),'error');
        }
        return redirect($this->urlModule);
    }

    public function getParticipant($event_id)
    {
        Access::grant_check('is_read', $this->menuID);
        $data=$this->model->where('event_id','=',$event_id)->where('deleted','=',0)->first();
        $getParticipant = $this->model_user->select('first_name','last_name','email','phone')->whereIn('user_id',json_decode($data->event_participant_join))->get();
        echo json_encode($getParticipant);
    }
}