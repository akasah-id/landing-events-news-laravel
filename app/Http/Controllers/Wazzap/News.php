<?php
namespace App\Http\Controllers\Wazzap;
use App\Http\Controllers\Controller;
use App\Http\Controllers\StarterKit\Access;
use App\Models\Wazzap\NewsModel;
use App\Models\Wazzap\NewsCategoryModel;
use App\Models\Wazzap\NewsRatingModel;

use App\Libraries\Helpers;
use App\Http\Requests;
use Illuminate\Http\Request;
use File;
use Storage;

class News extends Controller
{

    protected $data = array();

    public function __construct()
    {
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->menuID = 109;
        $this->urlModule = 'news';
        $this->viewFile = 'wazzap.news';
        $this->title = 'News';

        $this->model = new NewsModel();
        $this->model_news_category = new NewsCategoryModel();
        $this->model_news_rating = new NewsRatingModel();
    }

    public function data()
    {
        Access::grant_check('is_read',$this->menuID);
        $data['access'] = new Access;
        $data['menuID'] = $this->menuID;
        $data['title'] = __("menu.{$this->title} Management");
        $data['urlModule'] = $this->urlModule;
        $data['delBtn'] = Helpers::del_button("$this->urlModule/batch_action");
        
        $data['column'] = json_encode(array(
            array('data'=>'id'),
            array('data'=>'news_title'),
            array('data'=>'news_category'),
            array('data'=>'news_view'),
	        array('data'=>'action'),
        ));
        return view($this->viewFile, $data);
    }

    public function ajax_data(Request $req)
    {
        Access::grant_check('is_read', $this->menuID);
        if (session()->get('group_id')==4) {
            $data=$this->model->select(\DB::raw('wazzap_news.*, tb_users.username as u_username, tb_users.first_name as u_first_name, tb_users.last_name as u_last_name'))->join('tb_users','wazzap_news.news_creator','tb_users.user_id')->whereIn('news_category_id',['["5"]','["6"]','["7"]','["10"]',])->where('deleted','=',0)->orderBy('created_at','DESC')->get();
        } else {
            $data=$this->model->select(\DB::raw('wazzap_news.*, tb_users.username as u_username, tb_users.first_name as u_first_name, tb_users.last_name as u_last_name'))->join('tb_users','wazzap_news.news_creator','tb_users.user_id')->where('deleted','=',0)->orderBy('created_at','DESC')->get();
        }
        
        $no = 1;
        $records["draw"] = 1;
        $records["recordsTotal"]=$this->model->count();
        $records["recordsFiltered"]=$this->model->count();
        $records["data"] = [];
        foreach ($data as $key) {
            $action = '';
            if (Access::grant_check('is_update', $this->menuID, true)) {
                $action .= '<a href="' . url("$this->urlModule/view/$key->news_id") . '" class="btn waves-effect waves-light btn-outline-warning btn-sm btn-icon-fixed"><span class="fa fa-edit"></span> '.__("dashboard.Edit").'</a> ';
            }
            $category = '';
            foreach ($this->model_news_category->whereIn('news_category_id',json_decode($key->news_category_id))->get() as $key2 => $value2) {
                $category .= ($category=='' ? '' : ' ')."<a target='_blank' href='".url('/'.$value2->category_hashtag)."'>$value2->category_name</a>";
            }
            if ($key->news_status == 'Active') {
                $status = "<a target='_blank' href='#' title='Active'> <i class='fa fa-check'></i> $key->news_status</a>";
                $news_title = $key->news_title;
            } else {
                $status = "<a target='_blank' href='#' title='Inactive'> <i class='fa fa-minus'></i> $key->news_status</a>";
                $news_title = "<span style='text-decoration: line-through;'>$key->news_title</span>";
            }

            $records["data"][] = array(
                "DT_RowId"=>"data_".$key->news_id,
                'id'=>$key->news_id,
                // Specify any columns needed
                'news_title'=>"
                    <a href='".url('/'.$value2->category_hashtag.'/'.$key->news_slug)."' target='_blank'>$news_title</a> by $key->u_first_name $key->u_last_name <br>".$status."
                    <a target='_blank' href='".url('file/images/news/'.$key->news_image)."' title='Image - ".$key->news_title."'> <i class='fa fa-image'></i> </a> 
                    <a target='_blank' href='".url('file/sounds/news/'.$key->news_voice)."' title='Audio - ".$key->news_title."'> <i class='fa fa-microphone'></i> </a>",
                'news_category'=>$category,
                'news_view'=>"$key->news_view <br>Dibuat: ".Helpers::konversi_tanggal($key->created_at,'l, j F Y | H:i').'<br>Diedit: '.Helpers::konversi_tanggal($key->updated_at,'l, j F Y | H:i'),
                'action'=>$action,
            );
            $no++;
        }
        // header('Content-Type: application/json');
        if ($req->ajax()) {
            echo json_encode($records);
        } else {
            return redirect($this->urlModule);
        }
    }

    public function view($id=0)
    {
        $data['urlModule']=$this->urlModule;
        $data['information']=[];
        $data['id']=$id;
        $data['title_module']=$this->title;
        $data['news_category']=$this->model_news_category->where('deleted','=',0)->get();
        if ($id==0) {
            Access::grant_check('is_create',$this->menuID);
            $data['title']=__("news.Add $this->title");
        } else {
            Access::grant_check('is_update', $this->menuID);
            $data['information'] = $this->model->getRow($id);
            $name=$data['information']->news_title;
            $data['title']=__("news.Edit $this->title")." - $name";
        }
        return view("{$this->viewFile}_view",$data);
    }

    public function save(Request $request, $id)
    {
        $this->validate($request, [
            // Specify any fields needed
            'news_category_id' => 'required',
            'news_metakey' => 'required',
            'news_metadesc' => 'required',
            'news_title' => 'required',
            'news_subtitle' => 'required',
            'news_status' => 'required',
            'news_layout' => 'required',
            'news_content' => 'required',
            'news_lang' => 'required',
        ]);
        $data = $request->except(['_token']);
        if ($data) {
            $getfile = $this->model->find($id);
        	// File Upload
            if ($request->hasFile('news_metaimage')) {
                if ($id!=0) {
                  $news_metaimage=$getfile->news_metaimage;
                  if ($news_metaimage != '') {
                      @unlink('file/images/news/'.$news_metaimage);
                  }
                }
                $file = $request->file('news_metaimage');
                $filename = date('d-M-Y').'-news_metaimage-'.uniqid().'.'.$data['news_metaimage']->extension();
                $destination = 'file/images/news/';
                if ($file->move($destination, $filename)) {
                    $data['news_metaimage'] = $filename;
                } else {
                    $data['news_metaimage'] = '';
                }
            }
            if ($request->hasFile('news_voice')) {
                if ($id!=0) {
                  $news_voice=$getfile->news_voice;
                  if ($news_voice != '') {
                      @unlink('file/sounds/news/'.$news_voice);
                  }
                }
                $file = $request->file('news_voice');
                $filename = date('d-M-Y').'-news_voice-'.uniqid().'.'.$data['news_voice']->extension();
                $destination = 'file/sounds/news/';
                if ($file->move($destination, $filename)) {
                    $data['news_voice'] = $filename;
                } else {
                    $data['news_voice'] = '';
                }
            }
            if ($request->hasFile('news_image')) {
                if ($id!=0) {
                  $news_image=$getfile->news_image;
                  if ($news_image != '') {
                      @unlink('file/images/news/'.$news_image);
                  }
                }
                $file = $request->file('news_image');
                $filename = date('d-M-Y').'-news_image-'.uniqid().'.'.$data['news_image']->extension();
                $destination = 'file/images/news/';
                if ($file->move($destination, $filename)) {
                    $data['news_image'] = $filename;
                } else {
                    $data['news_image'] = '';
                }
            }

            if ($id == 0) {
                Access::grant_check('is_create', $this->menuID);
                $data['news_creator'] = session()->get('user_id');
                $data['news_category_id'] = json_encode($data['news_category_id']);
                $data['news_slug'] = str_slug($request->news_title,'-').'-'.mt_rand(10,99);

                $data['created_by']=session()->get('first_name').' '.session()->get('last_name');
                $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $data['created_at'] = date('Y-m-d H:i:s');
                $result = $this->model->saveRow($data);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_add'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_save'),'success');
                    return redirect("$this->urlModule");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_save'),'error');
                    return redirect("$this->urlModule/new")->withInput();
                }
            } else {
                Access::grant_check('is_update', $this->menuID);
                $data['news_creator'] = session()->get('user_id');
                if ($getfile->news_title != $request->news_title) {
                    $data['news_slug'] = str_slug($request->news_title,'-').'-'.mt_rand(10,99);
                }
                $data['news_category_id'] = json_encode($data['news_category_id']);
	            $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $result = $this->model->saveRow($data, $id);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_edit'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_update'),'success');
                    return redirect("$this->urlModule");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_update'),'error');
                    return redirect("$this->urlModule/view/$id")->withInput();
                }
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.no_request'),'error');
            return redirect($this->urlModule)->withInput();
        }
    }

    public function delete($req, $id)
    {
        Access::grant_check('is_delete', $this->menuID);
        // Delete File
        // $data = $this->model->find($id);
        // $avatar=$data->avatar;
        // if ($avatar != '') {
            // @unlink('file/images/module/'.$avatar);
        // }
        $result = $this->model->where('news_id', $id)->update(array('deleted' => 1));
        if ($result) {
            Helpers::auditTrail($req, sprintf(__('dashboard.log_delete'),$id));
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_delete'),'error');        
        }
        return redirect($this->urlModule);
    }

    public function batch_action(Request $req)
    {
      if ($req->action=='Delete') {
            Access::grant_check('is_delete', $this->menuID);
            $id=json_decode($req->id_item);
            if (count($id)>1) {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_bulk'),'success');        
            } else {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_delete'),'success');        
            }
            foreach ($id as $key) {
                $this->delete($req, $key->id_item);
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_bulk'),'error');
        }
        return redirect($this->urlModule);
    }
}