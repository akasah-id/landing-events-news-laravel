<?php
namespace App\Http\Controllers\Wazzap;
use App\Http\Controllers\Controller;
use App\Http\Controllers\StarterKit\Access;
use App\Models\Wazzap\NewsCategoryModel;

use App\Libraries\Helpers;
use App\Http\Requests;
use Illuminate\Http\Request;
use File;
use Storage;

class NewsCategory extends Controller
{

    protected $data = array();

    public function __construct()
    {
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->menuID = 110;
        $this->urlModule = 'news_category';
        $this->viewFile = 'wazzap.news_category';
        $this->title = 'News Category';

        $this->model = new NewsCategoryModel();
    }

    public function data()
    {
        Access::grant_check('is_read',$this->menuID);
        $data['access'] = new Access;
        $data['menuID'] = $this->menuID;
        $data['title'] = __("menu.{$this->title}");
        $data['urlModule'] = $this->urlModule;
        $data['delBtn'] = Helpers::del_button("$this->urlModule/batch_action");
        
        $data['column'] = json_encode(array(
            array('data'=>'id'),
            array('data'=>'category_name'),
            array('data'=>'category_hashtag'),
            array('data'=>'category_related'),
	        array('data'=>'action'),
        ));
        return view($this->viewFile, $data);
    }

    public function ajax_data(Request $req)
    {
        Access::grant_check('is_read', $this->menuID);
        $data=$this->model->where('deleted','=',0)->get();
        $no = 1;
        $records["draw"] = 1;
        $records["recordsTotal"]=$this->model->count();
        $records["recordsFiltered"]=$this->model->count();
        $records["data"] = [];
        foreach ($data as $key) {
            $action = '';
            if (Access::grant_check('is_update', $this->menuID, true)) {
                $action .= '<a href="' . url("$this->urlModule/view/$key->news_category_id") . '" class="btn waves-effect waves-light btn-outline-warning btn-sm btn-icon-fixed"><span class="fa fa-edit"></span> '.__("dashboard.Edit").'</a> ';
            }

            $category_related = '';
            // dd($key->category_related);
            if ($key->category_related!=null) {
                foreach (json_decode($key->category_related) as $value) {
                    $cat = $this->model->where('news_category_id',$value)->first();
                    $category_related .= $category_related == '' ? $cat->category_name : ", $cat->category_name";
                }
            }
            $records["data"][] = array(
                "DT_RowId"=>"data_".$key->news_category_id,
                'id'=>$key->news_category_id,
                // Specify any columns needed
                'category_name'=>$key->category_name,
                'category_hashtag'=>"<strong>$key->category_hashtag</strong><br>$key->category_desc",
                'category_related'=>$category_related,
                'action'=>$action,
            );
            $no++;
        }
        // header('Content-Type: application/json');
        if ($req->ajax()) {
            echo json_encode($records);
        } else {
            return redirect($this->urlModule);
        }
    }

    public function view($id=0)
    {
        $data['urlModule']=$this->urlModule;
        $data['information']=[];
        $data['id']=$id;
        $data['title_module']=$this->title;
        $data['category_related']=$this->model->where('deleted','=',0)->get();
        if ($id==0) {
            Access::grant_check('is_create',$this->menuID);
            $data['title']=__("news_category.Add $this->title");
        } else {
            Access::grant_check('is_update', $this->menuID);
            $data['information'] = $this->model->getRow($id);
            $name=$data['information']->category_name;
            $data['title']=__("news_category.Edit $this->title")." - $name";
        }
        return view("{$this->viewFile}_view",$data);
    }

    public function save(Request $request, $id)
    {
        $this->validate($request, [
            // Specify any fields needed
            'category_name' => 'required',
            'category_hashtag' => 'required',
            'category_desc' => 'required',
        ]);
        $data = $request->except(['_token']);
        if ($data) {
        	// File Upload
            if ($request->hasFile('category_image')) {
                if ($id!=0) {
                  $getava = $this->model->find($id);
                  $category_image=$getava->category_image;
                  if ($category_image != '') {
                      @unlink('file/images/news_category/'.$category_image);
                  }
                }
                $file = $request->file('category_image');
                $filename = date('d-M-Y') . '-' . uniqid() . '-news_category.' . $data['category_image']->extension();
                $destination = 'file/images/news_category/';
                if ($file->move($destination, $filename)) {
                    $data['category_image'] = $filename;
                } else {
                    $data['category_image'] = '';
                }
            }

            $data['category_related'] = @count($data['category_related']) > 0 ? json_encode($data['category_related']) : '';

            if ($id == 0) {
                Access::grant_check('is_create', $this->menuID);
                $data['created_by']=session()->get('first_name').' '.session()->get('last_name');
                $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $data['created_at'] = date('Y-m-d H:i:s');
                $result = $this->model->saveRow($data);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_add'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_save'),'success');
                    return redirect("$this->urlModule");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_save'),'error');
                    return redirect("$this->urlModule/new")->withInput();
                }
            } else {
                Access::grant_check('is_update', $this->menuID);
	            $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $result = $this->model->saveRow($data, $id);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_edit'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_update'),'success');
                    return redirect("$this->urlModule");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_update'),'error');
                    return redirect("$this->urlModule/view/$id")->withInput();
                }
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.no_request'),'error');
            return redirect($this->urlModule)->withInput();
        }
    }

    public function delete($req, $id)
    {
        Access::grant_check('is_delete', $this->menuID);
        // Delete File
        $data = $this->model->find($id);
        $category_image=$data->category_image;
        if ($category_image != '') {
            @unlink('file/images/news_category/'.$category_image);
        }
        $result = $this->model->where('news_category_id', $id)->update(array('deleted' => 1));
        if ($result) {
            Helpers::auditTrail($req, sprintf(__('dashboard.log_delete'),$id));
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_delete'),'error');        
        }
        return redirect($this->urlModule);
    }

    public function batch_action(Request $req)
    {
      if ($req->action=='Delete') {
            Access::grant_check('is_delete', $this->menuID);
            $id=json_decode($req->id_item);
            if (count($id)>1) {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_bulk'),'success');        
            } else {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_delete'),'success');        
            }
            foreach ($id as $key) {
                $this->delete($req, $key->id_item);
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_bulk'),'error');
        }
        return redirect($this->urlModule);
    }
}