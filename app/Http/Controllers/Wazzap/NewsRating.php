<?php
namespace App\Http\Controllers\Wazzap;
use App\Http\Controllers\Controller;
use App\Http\Controllers\StarterKit\Access;
use App\Models\Wazzap\NewsRatingModel;
use App\Models\Wazzap\NewsModel;

use App\Libraries\Helpers;
use App\Http\Requests;
use Illuminate\Http\Request;
use File;
use Storage;

class NewsRating extends Controller
{

    protected $data = array();

    public function __construct()
    {
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->menuID = 111;
        $this->urlModule = 'news_rating';
        $this->viewFile = 'wazzap.news_rating';
        $this->title = 'News Rating';

        $this->model = new NewsRatingModel();
        $this->model_news = new NewsModel();
    }

    public function data()
    {
        Access::grant_check('is_read',$this->menuID);
        $data['access'] = new Access;
        $data['menuID'] = $this->menuID;
        $data['title'] = __("menu.{$this->title}");
        $data['urlModule'] = $this->urlModule;
        $data['delBtn'] = Helpers::del_button("$this->urlModule/batch_action");
        
        $data['column'] = json_encode(array(
            array('data'=>'id'),
            array('data'=>'news'),
            array('data'=>'rate'),
	        array('data'=>'action'),
        ));
        return view($this->viewFile, $data);
    }

    public function ajax_data(Request $req)
    {
        Access::grant_check('is_read', $this->menuID);
        $data=$this->model->select('news_id', \DB::raw('avg(news_rating_rate) as total_rate'))
                 ->groupBy('news_id')
                 ->get();
        $no = 1;
        $records["draw"] = 1;
        $records["recordsTotal"]=$this->model->count();
        $records["recordsFiltered"]=$this->model->count();
        $records["data"] = [];
        foreach ($data as $key) {
            $action = '';
            if (Access::grant_check('is_update', $this->menuID, true)) {
                $action .= '<a href="#" data-toggle="modal" onclick="get_review('.$key->news_id.')" data-target="#responsive-modal" class="btn waves-effect waves-light btn-outline-warning btn-sm btn-icon-fixed"><span class="fa fa-star"></span> '.__("news_rating.View Raters").'</a> ';
            }
            $news_title = $this->model_news->select('news_title')->where('news_id','=',$key->news_id)->first();
            $review = $key->total_rate;
            $review_count = round(5 - $review);
            $review_count_left = 5-$review_count;
            $review_star = '';
            for ($i=0; $i < $review_count_left; $i++) { 
                $review_star .= '<span class="fa fa-star checked"></span>';
            }
            for ($i=0; $i < $review_count; $i++) { 
                $review_star .= '<span class="fa fa-star"></span>';
            }
            $records["data"][] = array(
                "DT_RowId"=>"data_".$key->news_rating_id,
                'id'=>$key->news_rating_id,
                // Specify any columns needed
                'news'=>$news_title->news_title,
                'rate'=>$review==null ? 'Unrated' : "$review_star (".round($review,2).")",
                'action'=>$action,
            );
            $no++;
        }
        // header('Content-Type: application/json');
        if ($req->ajax()) {
            echo json_encode($records);
        } else {
            return redirect($this->urlModule);
        }
    }

    public function view($id=0)
    {
        $data['urlModule']=$this->urlModule;
        $data['information']=[];
        $data['id']=$id;
        $data['title_module']=$this->title;
        if ($id==0) {
            Access::grant_check('is_create',$this->menuID);
            $data['title']=__("news_rating.Add $this->title");
        } else {
            Access::grant_check('is_update', $this->menuID);
            $data['information'] = $this->model->getRow($id);
            $name=$data['information']->name;
            $data['title']=__("news_rating.Edit $this->title")." - $name";
        }
        return view("{$this->viewFile}_view",$data);
    }

    public function save(Request $request, $id)
    {
        $this->validate($request, [
            // Specify any fields needed
            'name'=>'required',
        ]);
        $data = $request->except(['_token']);
        if ($data) {
        	// File Upload
            // if ($request->hasFile('image')) {
                // if ($id!=0) {
                  // $getava = $this->model->find($id);
                  // $image=$getava->image;
                  // if ($image != '') {
                      // @unlink('file/images/news_rating/'.$image);
                  // }
                // }
                // $file = $request->file('image');
                // $filename = date('d-M-Y') . '-' . uniqid() . '-news_rating.' . $data['image']->extension();
                // $destination = 'file/images/news_rating/';
                // if ($file->move($destination, $filename)) {
                    // $data['image'] = $filename;
                // } else {
                    // $data['image'] = '';
                // }
            // }

            if ($id == 0) {
                Access::grant_check('is_create', $this->menuID);
                $data['created_by']=session()->get('first_name').' '.session()->get('last_name');
                $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $data['created_at'] = date('Y-m-d H:i:s');
                $result = $this->model->saveRow($data);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_add'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_save'),'success');
                    return redirect("$this->urlModule");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_save'),'error');
                    return redirect("$this->urlModule/new")->withInput();
                }
            } else {
                Access::grant_check('is_update', $this->menuID);
	            $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $result = $this->model->saveRow($data, $id);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_edit'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_update'),'success');
                    return redirect("$this->urlModule");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_update'),'error');
                    return redirect("$this->urlModule/view/$id")->withInput();
                }
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.no_request'),'error');
            return redirect($this->urlModule)->withInput();
        }
    }

    public function delete($req, $id)
    {
        Access::grant_check('is_delete', $this->menuID);
        // Delete File
        // $data = $this->model->find($id);
        // $avatar=$data->avatar;
        // if ($avatar != '') {
            // @unlink('file/images/news_rating/'.$avatar);
        // }
        $result = $this->model->where('news_rating_id', $id)->update(array('deleted' => 1));
        if ($result) {
            Helpers::auditTrail($req, sprintf(__('dashboard.log_delete'),$id));
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_delete'),'error');        
        }
        return redirect($this->urlModule);
    }

    public function batch_action(Request $req)
    {
      if ($req->action=='Delete') {
            Access::grant_check('is_delete', $this->menuID);
            $id=json_decode($req->id_item);
            if (count($id)>1) {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_bulk'),'success');        
            } else {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_delete'),'success');        
            }
            foreach ($id as $key) {
                $this->delete($req, $key->id_item);
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_bulk'),'error');
        }
        return redirect($this->urlModule);
    }

    public function get_review(Request $req, $news_id)
    {
        Access::grant_check('is_read', $this->menuID);
        $data=$this->model->select('news.news_title', 'news_rating.news_rating_rate','tb_users.first_name','tb_users.last_name')
                 ->join('news','news.news_id','news_rating.news_id')
                 ->join('tb_users','tb_users.user_id','news_rating.user_id')
                 ->where('news_rating.news_id',$news_id)
                 ->get();
        if ($req->ajax()) {
            echo json_encode($data);
        } else {
            return redirect($this->urlModule);
        }
    }

}