<?php
namespace App\Http\Controllers\Wazzap;
use App\Http\Controllers\Controller;
use App\Http\Controllers\StarterKit\Access;
use App\Models\Wazzap\PodcastModel;
use App\Models\Wazzap\NewsCategoryModel;
use App\Models\UsersModel;

use App\Libraries\Helpers;
use App\Http\Requests;
use Illuminate\Http\Request;
use File;
use Storage;

class Podcast extends Controller
{

    protected $data = array();

    public function __construct()
    {
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->menuID = 117;
        $this->urlModule = 'podcast';
        $this->viewFile = 'wazzap.podcast';
        $this->title = 'Podcast';

        $this->model = new PodcastModel();
        $this->model_news_category = new NewsCategoryModel();
        $this->model_users = new UsersModel();
    }

    public function data()
    {
        Access::grant_check('is_read',$this->menuID);
        $data['access'] = new Access;
        $data['menuID'] = $this->menuID;
        $data['title'] = __("menu.{$this->title} Management");
        $data['urlModule'] = $this->urlModule;
        $data['delBtn'] = Helpers::del_button("$this->urlModule/batch_action");
        
        $data['column'] = json_encode(array(
            array('data'=>'id'),
            array('data'=>'podcast_title'),
            array('data'=>'podcast_caption'),
            array('data'=>'podcast_contributors'),
            array('data'=>'podcast_categories_id'),
	        array('data'=>'action'),
        ));
        return view($this->viewFile, $data);
    }

    public function ajax_data(Request $req)
    {
        Access::grant_check('is_read', $this->menuID);
        $data=$this->model->select(\DB::raw('wazzap_podcast.*, tb_users.username as u_username, tb_users.first_name as u_first_name, tb_users.last_name as u_last_name'))->join('tb_users','wazzap_podcast.podcast_creator','tb_users.user_id')->where('deleted','=',0)->get();
        $no = 1;
        $records["draw"] = 1;
        $records["recordsTotal"]=$this->model->count();
        $records["recordsFiltered"]=$this->model->count();
        $records["data"] = [];
        foreach ($data as $key) {
            $action = '';
            if (Access::grant_check('is_update', $this->menuID, true)) {
                $action .= '<a href="' . url("$this->urlModule/view/$key->podcast_id") . '" class="btn waves-effect waves-light btn-outline-warning btn-sm btn-icon-fixed"><span class="fa fa-edit"></span> '.__("dashboard.Edit").'</a> ';
            }
            $contributors = '';
            foreach ($this->model_users->whereIn('user_id',json_decode($key->podcast_contributors))->get() as $key2 => $value2) {
                $contributors .= ($contributors=='' ? '' : ' ')."<a target='_blank' href='".url('/@'.$value2->username)."'>$value2->first_name $value2->last_name</a><br>";
            }
            $categories = '';
            foreach ($this->model_news_category->whereIn('news_category_id',json_decode($key->podcast_categories_id))->get() as $key3 => $value3) {
                $categories .= ($categories=='' ? '' : ' ')."<a target='_blank' href='".url('/#'.$value3->category_name)."'>$value3->category_name</a><br>";
            }
            $records["data"][] = array(
                "DT_RowId"=>"data_".$key->podcast_id,
                'id'=>$key->podcast_id,
                // Specify any columns needed
                'podcast_title'=>"
                    <a target='_blank' href='".url('file/images/podcast/'.$key->podcast_poster)."' title='Image - ".$key->podcast_title."'> <i class='fa fa-image'></i> </a>
                    <a target='_blank' href='".url('file/sounds/podcast/'.$key->podcast_file)."' title='Audio - ".$key->podcast_title."'> <i class='fa fa-microphone'></i> </a>
                    <a href='https://akasah.id/podcast/$key->podcast_uri' target='_blank'>$key->podcast_title</a> by $key->u_first_name $key->u_last_name <br>
                    ",
                'podcast_caption'=>$key->podcast_caption,
                'podcast_contributors'=>$contributors,
                'podcast_categories_id'=>$categories,
                'action'=>$action,
            );
            $no++;
        }
        // header('Content-Type: application/json');
        if ($req->ajax()) {
            echo json_encode($records);
        } else {
            return redirect($this->urlModule);
        }
    }

    public function view($id=0)
    {
        $data['urlModule']=$this->urlModule;
        $data['information']=[];
        $data['id']=$id;
        $data['title_module']="$this->title Management";
        $data['news_category']=$this->model_news_category->where('deleted','=',0)->get();
        $data['contributors']=$this->model_users->get();
        if ($id==0) {
            Access::grant_check('is_create',$this->menuID);
            $data['title']=__("podcast.Add $this->title");
        } else {
            Access::grant_check('is_update', $this->menuID);
            $data['information'] = $this->model->getRow($id);
            $name=$data['information']->podcast_title;
            $data['title']=__("podcast.Edit $this->title")." - $name";
        }
        return view("{$this->viewFile}_view",$data);
    }

    public function save(Request $request, $id)
    {
        $this->validate($request, [
            // Specify any fields needed
            'podcast_title' => 'required',
            'podcast_caption' => 'required',
            'podcast_desc' => 'required',
            'podcast_duration' => 'required',
            'podcast_categories_id' => 'required',
            'podcast_contributors' => 'required',
        ]);
        $data = $request->except(['_token']);
        if ($data) {
        	// File Upload
            $getfile = $this->model->find($id);
            if ($request->hasFile('podcast_poster')) {
                if ($id!=0) {
                  $podcast_poster=$getfile->podcast_poster;
                  if ($podcast_poster != '') {
                      @unlink('file/images/podcast/'.$podcast_poster);
                  }
                }
                $file = $request->file('podcast_poster');
                $filename = date('d-M-Y') . '-' . uniqid() . '-podcast.' . $data['podcast_poster']->extension();
                $destination = 'file/images/podcast/';
                if ($file->move($destination, $filename)) {
                    $data['podcast_poster'] = $filename;
                } else {
                    $data['podcast_poster'] = '';
                }
            }
            if ($request->hasFile('podcast_file')) {
                if ($id!=0) {
                  $podcast_file=$getfile->podcast_file;
                  if ($podcast_file != '') {
                      @unlink('file/sounds/podcast/'.$podcast_file);
                  }
                }
                $file = $request->file('podcast_file');
                $filename = date('d-M-Y') . '-' . uniqid() . '-podcast.' . $data['podcast_file']->extension();
                $destination = 'file/sounds/podcast/';
                if ($file->move($destination, $filename)) {
                    $data['podcast_file'] = $filename;
                } else {
                    $data['podcast_file'] = '';
                }
            }

            if ($id == 0) {
                Access::grant_check('is_create', $this->menuID);
                $data['podcast_creator'] = session()->get('user_id');
                $data['podcast_categories_id'] = json_encode($data['podcast_categories_id']);
                $data['podcast_contributors'] = json_encode($data['podcast_contributors']);
                $data['podcast_uri'] = str_slug($request->podcast_title,'-').'-'.mt_rand(10,99); ;
                $data['created_by']=session()->get('first_name').' '.session()->get('last_name');
                $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $data['created_at'] = date('Y-m-d H:i:s');
                $result = $this->model->saveRow($data);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_add'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_save'),'success');
                    return redirect("$this->urlModule");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_save'),'error');
                    return redirect("$this->urlModule/new")->withInput();
                }
            } else {
                Access::grant_check('is_update', $this->menuID);
                $data['podcast_creator'] = session()->get('user_id');
                $data['podcast_categories_id'] = json_encode($data['podcast_categories_id']);
                $data['podcast_contributors'] = json_encode($data['podcast_contributors']);
                if ($getfile->podcast_title != $request->podcast_title) {
                    $data['podcast_uri'] = str_slug($request->podcast_title,'-').'-'.mt_rand(10,99);
                }
	            $data['updated_by']=session()->get('first_name').' '.session()->get('last_name'); 
                $result = $this->model->saveRow($data, $id);
                if ($result) {
                    Helpers::auditTrail($request, sprintf(__('dashboard.log_edit'), $result));
                    Helpers::flashMsg(__('login.Success'),__('dashboard.success_update'),'success');
                    return redirect("$this->urlModule");
                } else {
                    Helpers::flashMsg(__('login.Error'),__('dashboard.failed_update'),'error');
                    return redirect("$this->urlModule/view/$id")->withInput();
                }
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.no_request'),'error');
            return redirect($this->urlModule)->withInput();
        }
    }

    public function delete($req, $id)
    {
        Access::grant_check('is_delete', $this->menuID);
        // Delete File
        // $data = $this->model->find($id);
        // $avatar=$data->avatar;
        // if ($avatar != '') {
            // @unlink('file/images/podcast/'.$avatar);
        // }
        $result = $this->model->where('podcast_id', $id)->update(array('deleted' => 1));
        if ($result) {
            Helpers::auditTrail($req, sprintf(__('dashboard.log_delete'),$id));
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_delete'),'error');        
        }
        return redirect($this->urlModule);
    }

    public function batch_action(Request $req)
    {
      if ($req->action=='Delete') {
            Access::grant_check('is_delete', $this->menuID);
            $id=json_decode($req->id_item);
            if (count($id)>1) {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_bulk'),'success');        
            } else {
                Helpers::flashMsg(__('login.Success'),__('dashboard.success_delete'),'success');        
            }
            foreach ($id as $key) {
                $this->delete($req, $key->id_item);
            }
        } else {
            Helpers::flashMsg(__('login.Error'),__('dashboard.failed_bulk'),'error');
        }
        return redirect($this->urlModule);
    }
}