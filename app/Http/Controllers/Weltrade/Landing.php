<?php
namespace App\Http\Controllers\Weltrade;
use App\Http\Controllers\Controller;
use App\Http\Controllers\StarterKit\Access;
use App\Models\Wazzap\NewsModel;
use App\Models\Wazzap\NewsCategoryModel;

use App\Libraries\Helpers;
use App\Http\Requests;
use Illuminate\Http\Request;
use File;
use Storage;

class Landing extends Controller
{

    protected $data = array();

    public function __construct()
    {
        $this->middleware(function ($request, $next){
            Helpers::setLanguage();
            return $next($request);
        });
        $this->domain = 'weltrade.place';
        $this->domainidn = 'weltrade-idn.com';
        $this->model = new NewsModel();
        $this->model_news_category = new NewsCategoryModel();
    }

    public function main($referral='gg_campaign')
    {
    	$data['referral'] = $referral;
    	$data['weltrade'] = $this->domain;
        $data['weltradeidn'] = $this->domainidn;
        $view='main';
        $ref_val = explode('_', $referral);
        $pages = array('competition','services','proecn','education','partner','bonus100','democontest','stocks','gift','loyalty');
        if (in_array(@$ref_val[1], $pages)) {
            $view = $ref_val[1];
        } else {
            $view = 'main';
        }
    	return view("weltrade.$view",$data);
    }

    public function news()
    {
        $data=$this->model->select(\DB::raw('wazzap_news.*, tb_users.username as u_username, tb_users.first_name as u_first_name, tb_users.last_name as u_last_name'))->join('tb_users','wazzap_news.news_creator','tb_users.user_id')->where('deleted','=',0)->orderBy('created_at','desc')->limit(50)->get();
        $dataWebinar=$this->model->select(\DB::raw('wazzap_news.*, tb_users.username as u_username, tb_users.first_name as u_first_name, tb_users.last_name as u_last_name'))->join('tb_users','wazzap_news.news_creator','tb_users.user_id')->where('news_category_id','=','["7"]')->where('deleted','=',0)->orderBy('created_at','desc')->limit(50)->get();
        $records["webinar"] = [];
        $records["analisa-harian"] = [];
        $records["sinyal-trading"] = [];
        $records["prakiraan-mingguan"] = [];
        foreach ($data as $key) {
            $category = '';
            $category_hashtag = '';
            foreach ($this->model_news_category->whereIn('news_category_id',json_decode($key->news_category_id))->get() as $key2 => $value2) {
                $category = $value2->category_name;
                $category_hashtag = $value2->category_hashtag;
            }
            // if ($key->news_status == 'Active') {
                $news_title = $key->news_title;
                $data_row = array(
                    'id'=>$key->news_id,
                    // Specify any columns needed
                    'title'=>$key->news_title,
                    'subtitle'=>$key->news_subtitle,
                    'slug'=>$key->news_slug,
                    'status'=>$key->news_status,
                    'image'=>url('file/images/news/'.$key->news_image),
                    'meta_image'=>url('file/images/news/'.$key->news_metaimage),
                    'creator'=>"$key->u_first_name $key->u_last_name",
                    'category'=>$category,
                    'category_hashtag'=>$category_hashtag,
                    'date'=>Helpers::konversi_tanggal($key->created_at,'j F Y') ,
                    'datetime'=>Helpers::konversi_tanggal($key->created_at,'j F Y | H:i') ,
                    'voicetext'=>$key->news_voicetext,
                );
                if ($category_hashtag == 'analisa-harian' || $category_hashtag == 'sinyal-trading') {
                    $records["analisa-harian"][] = $data_row;
                    if ($category_hashtag == 'sinyal-trading') {
                        $records["sinyal-trading"][] = $data_row;
                    }
                } elseif ($category_hashtag == 'prakiraan-mingguan') {
                    $records["prakiraan-mingguan"][] = $data_row;
                }
            // }
        }
        foreach ($dataWebinar as $key) {
            $category = '';
            $category_hashtag = '';
            foreach ($this->model_news_category->whereIn('news_category_id',json_decode($key->news_category_id))->get() as $key2 => $value2) {
                $category = $value2->category_name;
                $category_hashtag = $value2->category_hashtag;
            }
            // if ($key->news_status == 'Active') {
                $news_title = $key->news_title;
                $data_row = array(
                    'id'=>$key->news_id,
                    // Specify any columns needed
                    'title'=>$key->news_title,
                    'subtitle'=>$key->news_subtitle,
                    'slug'=>$key->news_slug,
                    'status'=>$key->news_status,
                    'image'=>url('file/images/news/'.$key->news_image),
                    'meta_image'=>url('file/images/news/'.$key->news_metaimage),
                    'creator'=>"$key->u_first_name $key->u_last_name",
                    'category'=>$category,
                    'category_hashtag'=>$category_hashtag,
                    'date'=>Helpers::konversi_tanggal($key->created_at,'j F Y') ,
                    'datetime'=>Helpers::konversi_tanggal($key->created_at,'j F Y | H:i') ,
                    'voicetext'=>$key->news_voicetext,
                );
                if ($category_hashtag == 'webinar') {
                    $records["webinar"][] = $data_row;
                }
            // }
        }
        // return dd($records);
        return $records;
    }

    public function home()
    {
        $data['news'] = $this->news();
        $data['weltrade'] = $this->domain;
        $data['ref'] = '';
        $view='home';
        return view("weltrade.$view",$data);
    }
    public function homeCampaign($ref='')
    {
        $data['news'] = $this->news();
        $data['weltrade'] = $this->domain;
        $data['ref'] = "?r1=ids&r2=$ref";
        $view='home';
        return view("weltrade.$view",$data);
    }
    public function homePartner($ref='')
    {
        $data['news'] = $this->news();
        $data['weltrade'] = $this->domain;
        $data['ref'] = "?r1=ipartner&r2=$ref";
        $view='home';
        return view("weltrade.$view",$data);
    }

    public function homeNewsDetail($slug)
    {
        $data=$this->model->select(\DB::raw('wazzap_news.*, tb_users.username as u_username, tb_users.first_name as u_first_name, tb_users.last_name as u_last_name, tb_users.intro as intro, tb_users.avatar as avatar'))->join('tb_users','wazzap_news.news_creator','tb_users.user_id')->where('deleted','=',0)->where('news_slug','=',$slug)->first();
        if (!$data) {
            return redirect('/');
        }
        \DB::table('wazzap_news')
           ->where('news_slug', $slug)
           ->update([
               'news_view' => \DB::raw('news_view + 1'),
           ]);
        $category = '';
        $category_hashtag = '';
        foreach ($this->model_news_category->whereIn('news_category_id',json_decode($data['news_category_id']))->get() as $key2 => $value2) {
            $category = $value2->category_name;
            $category_hashtag = $value2->category_hashtag;
        }
        $data_row = array(
            'id'=>$data['news_id'],
            // Specify any columns needed
            'title'=>$data['news_title'],
            'subtitle'=>$data['news_subtitle'],
            'content'=>$data['news_content'],
            'intro'=>$data['intro'],
            'avatar'=>$data['avatar'],
            'image'=>$data['news_image'],
            'meta_image'=>$data['news_metaimage'],
            'creator'=>$data['u_first_name'].' '.$data['u_last_name'],
            'category'=>$category,
            'category_hashtag'=>$category_hashtag,
            'date'=>Helpers::konversi_tanggal($data['created_at'],'j F Y') ,
            'datetime'=>Helpers::konversi_tanggal($data['created_at'],'j F Y | H:i') ,
            'voicetext'=>$data['news_voicetext'],
        );

        $data['newsDetail'] = $data_row;
        $data['weltrade'] = $this->domain;
        $data['ref'] = "";
        return view("weltrade.home_news_detail",$data);
    }

    public function homeAnalisa()
    {
        $newsData = $this->news();
        $category_hashtag = 'analisa-harian';
        $data['category_hashtag'] = $category_hashtag;
        $data['category']=$this->model_news_category->where('category_hashtag',$category_hashtag)->first();
        $data['news'] = [];
        $data['datenow'] = Helpers::konversi_tanggal(now(),'l, j F Y');
        foreach ($newsData[$category_hashtag] as $key => $value) {
            if ($value['category_hashtag'] == $category_hashtag) {
                $data['news'][] = $value;
            }
        }
        
        $data['weltrade'] = $this->domain;
        $data['ref'] = '';
        return view("weltrade.home_news",$data);
    }

    public function homeSinyal()
    {
        $newsData = $this->news();
        $category_hashtag = 'sinyal-trading';
        $data['category_hashtag'] = $category_hashtag;
        $data['category']=$this->model_news_category->where('category_hashtag',$category_hashtag)->first();
        $data['news'] = [];
        $data['datenow'] = Helpers::konversi_tanggal(now(),'l, j F Y');
        foreach ($newsData[$category_hashtag] as $key => $value) {
            if ($value['category_hashtag'] == $category_hashtag) {
                $data['news'][] = $value;
            }
        }
        
        $data['weltrade'] = $this->domain;
        $data['ref'] = '';
        return view("weltrade.home_news",$data);
    }

    public function homePrakiraan()
    {
        $newsData = $this->news();
        $category_hashtag = 'prakiraan-mingguan';
        $data['category_hashtag'] = $category_hashtag;
        $data['category']=$this->model_news_category->where('category_hashtag',$category_hashtag)->first();
        $data['news'] = [];
        $data['datenow'] = Helpers::konversi_tanggal(now(),'l, j F Y');
        foreach ($newsData[$category_hashtag] as $key => $value) {
            if ($value['category_hashtag'] == $category_hashtag) {
                $data['news'][] = $value;
            }
        }
        
        $data['weltrade'] = $this->domain;
        $data['ref'] = '';
        return view("weltrade.home_news",$data);
    }

    public function homeWebinar()
    {
        $newsData = $this->news();
        $category_hashtag = 'webinar';
        $data['category_hashtag'] = $category_hashtag;
        $data['category']=$this->model_news_category->where('category_hashtag',$category_hashtag)->first();
        $data['news'] = [];
        $data['datenow'] = Helpers::konversi_tanggal(now(),'l, j F Y');
        foreach ($newsData[$category_hashtag] as $key => $value) {
            if ($value['category_hashtag'] == $category_hashtag) {
                $data['news'][] = $value;
            }
        }
        
        $data['weltrade'] = $this->domain;
        $data['ref'] = '';
        return view("weltrade.home_news",$data);
    }

    public function homeLoyalty()
    {
        $data['news'] = $this->news();
        $data['weltrade'] = $this->domain;
        $data['ref'] = '';
        $view='home_loyalty';
        return view("weltrade.$view",$data);
    }
}