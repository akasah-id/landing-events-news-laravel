<?php 

namespace App\Libraries;

class Helpers {

    public static function limitHtml($string,$limitChar=70)
    {
        // strip tags to avoid breaking any html
        $string = strip_tags($string);
        // truncate string
        $stringCut = substr($string, 0, $limitChar);
        $endPoint = strrpos($stringCut, ' ');

        //if the string doesn't contain any space then it will cut without word basis.
        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
        $string .= '...';
        return $string;
    }

    public static function slugify($string)
    {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        return strtolower($slug);
    }

    public static function auditTrail( $request , $note )
    {
        $data = array(
            'module'    => $request->segment(1),
            'task'      => $request->segment(2),
            'user_id' => session()->get('user_id'),
            'ipaddress' => $request->getClientIp(),
            'useragent' => $request->header('User-Agent'),
            'note'      => $note,
            'created_at'      => date('Y-m-d H:i:s'),
        );
        \DB::table( 'tb_logs')->insert($data);
    }

    public static function flashMsg($title,$msg,$type)
    {
        \Session::flash('title',$title);
        \Session::flash('message',$msg);
        \Session::flash('type',$type);
    }

    public static function setLanguage($lang=null)
    {
        if (session()->has('language')) {
            $lang = $lang==null ? session()->get('language') : $lang;
            \App::setLocale($lang);
        } else {
            $getlang=\Request::server('HTTP_ACCEPT_LANGUAGE');
            if (substr($getlang,0,2)=='id') {
                $lang='id';
            } else {
                $lang=\App::getLocale();
            }
        }
        \App::setLocale($lang);
        session(array('language' => $lang));
        return $lang;
    }

    public static function round_alt($number,$dec)
    {
        return number_format((float)$number, $dec, '.', '');
    }

    public static function del_button($url)
    {
        return '<form class="pull-right" style="display: initial;" id="batch_action" action="'.url($url).'" method="post">'.@csrf_field().'<input type="hidden" name="action" value="Delete"><span><b id="record">0</b> '.__('dashboard.records selected:').'</span> &nbsp&nbsp<button type="button" onclick="batch_action_btn()" class="btn-sm btn waves-effect waves-light btn-outline-info table-group-action-submit"><i class="fa fa-trash"></i> '.__('dashboard.Delete').'</button></form>';
    }

    public static function date_format_to_mysql($strdate)
    {
        $myDateTime = DateTime::createFromFormat('d/m/Y', $strdate);
        $newDateString = $myDateTime->format('Y-m-d');
        return $newDateString;
    }

    public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public static function to_currency($number,$currency_symbol='IDR', $decimals = 2)
    {
        if($number >= 0)
        {
            $ret = "$currency_symbol ".number_format($number, $decimals, '.', ',');
        }
        else
        {
            $ret = '&#8209;'.$currency_symbol.number_format(abs($number), $decimals, '.', ',');
        }

        return preg_replace('/(?<=\d{2})0+$/', '', $ret);
    }


    public static function randomKey($length) {
        $pool = array_merge(range(0,9), range('A', 'Z'));

        for($i=0; $i < $length; $i++) {
            @$key .= $pool[mt_rand(0, count($pool) - 1)];
        }
        return $key;
    }

    public static function konversi_tanggal($waktu, $format) { //{tanggalIndoTiga tgl=0000-00-00 00:00:00 format="l, d/m/Y H:i:s"}
        if($waktu == "0000-00-00" || !$waktu || $waktu == "0000-00-00 00:00:00") {
            $rep = "";
        } else {
            if(preg_match('/-/', $waktu)) {
                $tahun = substr($waktu,0,4);
                $bulan = substr($waktu,5,2);
                $tanggal = substr($waktu,8,2);
            } else {
                $tahun = substr($waktu,0,4);
                $bulan = substr($waktu,4,2);
                $tanggal = substr($waktu,6,2);
            }

            $jam = substr($waktu,11,2);
            $menit= substr($waktu,14,2);
            $detik = substr($waktu,17,2);
            $hari_en = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            $hari_id = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
            $bulan_en = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $bulan_id = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
            $ret = @date($format, @mktime($jam, $menit, $detik, $bulan, $tanggal, $tahun));

            $replace_hari = str_replace($hari_en, $hari_id, $ret);
            $rep = str_replace($bulan_en, $bulan_id, $replace_hari);
            $rep = nl2br($rep);
        }
        return $rep;
    }

}