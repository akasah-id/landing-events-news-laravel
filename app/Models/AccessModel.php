<?php

namespace App\Models;

class AccessModel extends GeneralModel
{
    protected $table = 'tb_access';
    protected $primaryKey = 'access_id';

    public function __construct()
    {
        parent::__construct();
    }
}
