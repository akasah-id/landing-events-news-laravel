<?php

namespace App\Models;

class AuditModel extends GeneralModel
{
    protected $table = 'tb_logs';
    protected $primaryKey = 'logs_id';

    public function __construct()
    {
        parent::__construct();
    }
}