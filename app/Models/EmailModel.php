<?php

namespace App\Models;

class EmailModel extends GeneralModel
{
	protected $table = 'tb_email_log';
    protected $primaryKey = 'email_id';

    public function __construct()
    {
        parent::__construct();
    }
}
