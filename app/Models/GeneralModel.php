<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class       GeneralModel
 * @package App\Models
 */
class GeneralModel extends Model
{
    protected $table;
    protected $key;

    /**
     * Fungsi untuk proses insert / update data
     * @param $data
     * @param $id
     * @return mixed
     */
    public function saveRow($data, $id = NULL)
    {
        $table = with(new static)->table;
        $key = with(new static)->primaryKey;
        if ($id == NULL) {
            // Insert Here
            $id = \DB::table($table)->insertGetId($data);
        } else {
            // Update here
            \DB::table($table)->where($key, $id)->update($data);
        }
        return $id;
    }

    /**
     * Fungsi untuk menapilkan data row dari requested id
     * @param $id
     * @return array
     */
    public function getRow($id)
    {
        $table = with(new static)->table;
        $key = with(new static)->primaryKey;

        $result = \DB::table($table)->where($key, $id)->get();
        if (count($result) <= 0) {
            $result = array();
        } else {
            $result = $result[0];
        }
        return $result;
    }

    /**
     * Fungsi untuk menapilkan semua data
     * @return mixed
     */
    public function getAll(){
        $table = with(new static)->table;
        $key = with(new static)->primaryKey;

        $result = \DB::table($table)
            ->orderBy($key, 'DESC')
            ->get();
        return $result;
    }

    /**
     * Fungsi untuk menampilkan excluded data
     * @param $column
     * @param array $value
     * @return mixed
     */
    public function getExclude($column, $value = array()){
        $table = with(new static)->table;
        $key = with(new static)->primaryKey;

        $result = \DB::table($table)
            ->whereNotIn($column, $value)
            ->orderBy($key, 'DESC')
            ->get();

        return $result;
    }

    /**
     * Fungsi untuk menampilkan included data
     * @param $column
     * @param array $value
     * @return mixed
     */
    public function getInclude($column, $value = array()){
        $table = with(new static)->table;
        $key = with(new static)->primaryKey;

        $result = \DB::table($table)
            ->whereIn($column, $value)
            ->orderBy($key, 'DESC')
            ->get();

        return $result;
    }

    /**
     * Fungsi untuk count all data
     * @return mixed
     */
    public function getCountAll()
    {
        $table = with(new static)->table;
        $result = \DB::table($table)
            ->count();

        return $result;
    }
}