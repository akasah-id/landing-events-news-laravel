<?php

namespace App\Models;

class GroupsModel extends GeneralModel
{
    protected $table = 'tb_groups';
    protected $primaryKey = 'group_id';

    public function __construct() {
        parent::__construct();
    }
}
