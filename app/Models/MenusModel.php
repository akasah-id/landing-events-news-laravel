<?php

namespace App\Models;

class MenusModel extends GeneralModel
{
    protected $table = 'tb_menu';
    protected $primaryKey = 'menu_id';

    public function __construct()
    {
        parent::__construct();
    }
}
