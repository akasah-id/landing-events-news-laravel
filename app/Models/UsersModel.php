<?php

namespace App\Models;

class UsersModel extends GeneralModel
{
    protected $table = 'tb_users';
    protected $primaryKey = 'user_id';

    public function __construct()
    {
        parent::__construct();
    }

    public function group_name()
    {
        return $this->hasOne('App\Models\GroupsModel');
    }
}
