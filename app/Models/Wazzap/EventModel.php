<?php

namespace App\Models\Wazzap;
use App\Models\GeneralModel as GeneralModel;
use Illuminate\Database\Eloquent\Model;

class EventModel extends GeneralModel
{
	protected $table = 'wazzap_event';
    protected $primaryKey = 'event_id';

    public function __construct()
    {
        parent::__construct();
    }
}
