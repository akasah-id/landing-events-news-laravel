<?php

namespace App\Models\Wazzap;
use App\Models\GeneralModel as GeneralModel;
use Illuminate\Database\Eloquent\Model;

class NewsCategoryModel extends GeneralModel
{
	protected $table = 'wazzap_news_category';
    protected $primaryKey = 'news_category_id';

    public function __construct()
    {
        parent::__construct();
    }
}
