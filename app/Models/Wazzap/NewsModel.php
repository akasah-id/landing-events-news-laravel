<?php

namespace App\Models\Wazzap;
use App\Models\GeneralModel as GeneralModel;
use Illuminate\Database\Eloquent\Model;

class NewsModel extends GeneralModel
{
	protected $table = 'wazzap_news';
    protected $primaryKey = 'news_id';

    public function __construct()
    {
        parent::__construct();
    }
}
