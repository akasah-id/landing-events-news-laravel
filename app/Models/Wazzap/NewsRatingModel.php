<?php

namespace App\Models\Wazzap;
use App\Models\GeneralModel as GeneralModel;
use Illuminate\Database\Eloquent\Model;

class NewsRatingModel extends GeneralModel
{
	protected $table = 'wazzap_news_rating';
    protected $primaryKey = 'news_rating_id';

    public function __construct()
    {
        parent::__construct();
    }
}
