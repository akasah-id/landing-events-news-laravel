<?php

namespace App\Models\Wazzap;
use App\Models\GeneralModel as GeneralModel;
use Illuminate\Database\Eloquent\Model;

class PodcastModel extends GeneralModel
{
	protected $table = 'wazzap_podcast';
    protected $primaryKey = 'podcast_id';

    public function __construct()
    {
        parent::__construct();
    }
}
