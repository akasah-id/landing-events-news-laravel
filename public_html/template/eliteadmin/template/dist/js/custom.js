$(document).ready(function() {
    $('form').submit(function (event) {
        // $(".required").each(function() {
        $(this).find(".required").each(function (index) {
            item = $(this).val();
            if (item == '' || item == null || item.substring(0, 2) == '--') {
                $(this).focus();
                $.toast({
                    heading: 'Warning!'
                    , text: $(this).closest('.form-group').find('.control-label').html() + ' Required'
                    , position: 'bottom-right'
                    , loaderBg: '#fff'
                    , icon: 'warning'
                    , hideAfter: 5000
                    , stack: 6
                })
                event.preventDefault();
            }
        });
    });

    if ($('.bs-datepicker2').length>0) {
      $('.bs-datepicker2').datetimepicker({
          maxDate: moment(), // Current day
      });
    }
});

// Datatables
var reload=0;
var serverside=false;
var rows_selected=[];

function reinit_data (ajax_url,column) {
      $("#datatable-complete").dataTable().fnDestroy();
      $('#record').html('0');
      init_datatable(1,ajax_url,column);
      rows_selected = [];
}

function init_datatable (reload,ajax_url,column,serverside) {
   var sScrollX = "";
   if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
     var sScrollX = "100%";
   };
   var table = $('#datatable-complete').DataTable({
    "sScrollX": sScrollX,
    "bScrollCollapse": true,
    "bAutoWidth":false,
    "processing": serverside,
    "serverSide": serverside,
    // buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
    // buttons: ['excel','pdf'],
    dom: 'lrtip',
      'ajax': ajax_url,
      'cache':false,
      // 'searching':false,
      'columns': column,
      'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'width': '1%',
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
            return '<input type="checkbox" name="id[]" id="data_' + $('<div/>').text(data).html() + '" value="' + $('<div/>').text(data).html() + '">';}
      }],
      'order': [[0, 'desc']],
      'rowCallback': function(row, data, dataIndex){
         // Get row ID
         var rowId = data[0];

         // If row ID is in the list of selected row IDs
         if($.inArray(rowId, rows_selected) !== -1){
            $(row).find('input[type="checkbox"]').prop('checked', true);
            $(row).addClass('selected');
         }
      }
   });


if (reload==0) {

// Updates "Select all" control in a data table
//
function updateDataTableSelectAllCtrl(table){
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = true;
      }
   }
}

   // Handle click on checkbox
   $('#datatable-complete tbody').on('click', 'input[type="checkbox"]', function(e){
      var getid=$(this).attr('id');
      var $row=('#'+getid);

      // Get row data
      var data = table.row($row).data();

      // Get row ID
      var rowId = data['id'];

      // Determine whether row ID is in the list of selected row IDs
      var index = $.inArray(rowId, rows_selected);

      // If checkbox is checked and row ID is not in list of selected row IDs
      if(this.checked && index === -1){
         rows_selected.push(rowId);

      // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
      } else if (!this.checked && index !== -1){
         rows_selected.splice(index, 1);
      }

      if(this.checked){
        var sumber=parseFloat($('#record').html());
        $('#record').html(sumber+1);
         // $row.addClass('selected');
         $('#'+getid).addClass('selected');
      } else {
        var sumber=parseFloat($('#record').html());
        $('#record').html(sumber-1);
         // $row.removeClass('selected');
         $('#'+getid).removeClass('selected');
      }

      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);

      // Prevent click event from propagating to parent
      e.stopPropagation();
   });

   // Handle click on table cells with checkboxes
   $('#datatable-complete').on('click', 'tbody td, thead th:first-child', function(e){
      $(this).parent().find('input[type="checkbox"]').trigger('click');
   });

   // Handle click on "Select all" control
   $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
      if(this.checked){
         $('#datatable-complete tbody input[type="checkbox"]:not(:checked)').trigger('click');
      } else {
         $('#datatable-complete tbody input[type="checkbox"]:checked').trigger('click');
      }

      // Prevent click event from propagating to parent
      e.stopPropagation();
   });

   // Handle table draw event
   table.on('draw', function(){
      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);
   });

   // Handle form submission event
   $('#batch_action').on('submit', function(e){
       // var form = this;
       // var get_data=[];

      // Iterate over all selected checkboxes
       // $.each(rows_selected, function(index, rowId){
       //   get_data.push({id_item:rowId});
       // });

     // Create a hidden element

       // var item_data=JSON.stringify(get_data);
       // $(form).append(
       //     $('<input>')
       //        .attr('type', 'hidden')
       //        .attr('name', 'id_item')
       //        .val(item_data)
       // );

      // Remove added elements
      $('input[name="id\[\]"]', form).remove();

      // Prevent actual form submission
      // e.preventDefault();
   });

$("#datatable-complete thead #filterrow td input").on( 'keyup change', function () {
  table
      .column($(this).parent().index()+':visible')
      .search(this.value)
      .draw();
      } );
};
}


function stopPropagation(evt) {
if (evt.stopPropagation !== undefined) {
  evt.stopPropagation();
} else {
  evt.cancelBubble = true;
}
}


function reset (ajax_url,column) {
$('#datatable-complete thead tr#filterrow input').each( function () {
    $(this).val('');
    $(this).keyup();
} );
reinit_data(ajax_url,column);
}

function item_delete(url) {
swal({
    title: 'Are you sure to do this ?',
    text: 'This would be irreversible !',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, Do it !'
    },function() {
          window.location.href=url;
});
}

function batch_action_btn () {
var text='This would be irreversible !';
if ($('.table-group-action-input').val()!='Delete') {
text='';
}
swal({
  title: 'Are you sure to do this?',
  text: text,
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, Delete Data!'
  },function() {
  var get_data = [];
  // Iterate over all selected checkboxes
  $.each(rows_selected, function (index, rowId) {
      get_data.push({id_item: rowId});
  });
  var item_data = JSON.stringify(get_data);
  $('#batch_action').append(
      $('<input>')
          .attr('type', 'hidden')
          .attr('name', 'id_item')
          .val(item_data)
  );
        $('#batch_action').submit();
});
}

function single_action_btn(url) {
    var text = 'This would be irreversible !';
    if ($('.table-group-action-input').val() != 'Delete') {
        text = '';
    }
    swal({
        title: 'Are you sure to do this?',
        text: text,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete Data!'
    }, function () {
        location.href = url;
    });
}

// Currency
var n='';
var currency='';
function to_currency(n, currency) {
currency==null ? currency='' : currency=currency;
n = parseInt(n);
var hasil=currency + " " + n.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
return hasil;
};

(function ($) {
    /**
     * Fungsi untuk menampilkan
     * @param url rest api
     */
    $.fn.requestApi = function (url, func) {
        $(this).on('keypress', function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                var value = $(this).val();
                $.ajax({
                    url: url + value,
                    type: 'GET'
                }).done(function (data) {
                    if ($.isFunction(func)) {
                        func(data);
                    }
                });

                return false;
            }
        });
    };

    /**
     * Limit textbox input keystrokes to only receive number only.
     */
    $.fn.integerOnly = function () {
        $(this).keypress(function (e) {
            var chars = $.possibleChars('Y-m-d');
            var chr = String.fromCharCode(e.charCode == 'undefined' ? e.keyCode : e.charCode);

            return e.ctrlKey || e.metaKey || (chr < ' ' || !chars || chars.indexOf(chr) > -1);
        });
    };

    /**
     * Menterjemahkan input format dan memberikan daftar karakter berdasarkan input format
     *
     * @param {String} format
     * @returns {String}
     */
    $.possibleChars = function (format) {
        var chars = '';
        var literal = false;
        // Check whether a format character is doubled
        var lookAhead = function (match) {
            var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) === match);
            if (matches)
                iFormat++;
            return matches;
        };
        for (var iFormat = 0; iFormat < format.length; iFormat++)
            if (literal)
                if (format.charAt(iFormat) === "'" && !lookAhead("'"))
                    literal = false;
                else
                    chars += format.charAt(iFormat);
            else
                switch (format.charAt(iFormat)) {
                    case 'd':
                    case 'm':
                    case 'y':
                    case '@':
                        chars += '0123456789';
                        break;
                    case 'D':
                    case 'M':
                        return null; // Accept anything
                    case '.':
                        chars += '.';
                        break;
                    case "'":
                        if (lookAhead("'"))
                            chars += "'";
                        else
                            literal = true;
                        break;
                    default:
                        chars += format.charAt(iFormat);
                }
        return chars;
    };

    /**
     * Toggle checkbox, and set all checkbox to checked
     *
     * @name checkAll
     */
    $.fn.checkAll = function () {
        var checked = false;

        return this.click(function () {
            $(':checkbox').map(function () {
                this.checked = !checked;
                return true;
            });
            checked = this.checked;
        });
    };

    /**
     * Create file uploader widget. Only works for single selector, by element ID only.
     * It requires {@link https://fineuploader.com fine-uploader} component to work properly.
     *
     * @param {Object} config The configuration options
     * @see https://fineuploader.com/
     */
    $.fn.FineUploader = function (config) {
        var me = $(this);
        var options = jQuery.extend({
            apiUrl: '',
            templateId: 'qq-template-manual-trigger',
            allowedExtensions: ['jpg', 'jpeg', 'png'],
            params: {},
            autoUpload: false,
            customHeaders: {},
            genericIconPath: null,
            waitingIconPath: null,
            multiple: true,
            itemLimit: 0,
            sizeLimit: 5242880, // 5 MB = 5 * 1024 * 1024 bytes,
            deleteFile: false
        }, config);
        var uploader = new qq.FineUploader({
            element: me[0],
            template: options.templateId,
            multiple: options.multiple,
            request: {
                endpoint: options.apiUrl,
                customHeaders: options.customHeaders,
                params: options.params
            },
            deleteFile: {
                enabled: options.deleteFile,
                endpoint: options.apiUrl,
            },
            thumbnails: {
                placeholders: {
                    waitingPath: options.waitingIconPath,
                    notAvailablePath: options.genericIconPath
                }
            },
            validation: {
                allowedExtensions: options.allowedExtensions,
                itemLimit: options.itemLimit,
                sizeLimit: options.sizeLimit
            },
            retry: {
                enableAuto: true,
                showButton: true
            },
            autoUpload: options.autoUpload,
            debug: false
        });

        if (!options.autoUpload) {
            $('.trigger-upload', me).click(function () {
                uploader.uploadStoredFiles();
            });
        }

        return uploader;
    };
})(jQuery);


$(function () {
    "use strict";

    $(function () {
        $(".preloader").fadeOut();
    });
    jQuery(document).on('click', '.mega-dropdown', function (e) {
        e.stopPropagation()
    });
    // ============================================================== 
    // This is for the top header part and sidebar part
    // ==============================================================  
    var set = function () {
        var width = (window.innerWidth > 0) ? window.innerWidth : this.screen.width;
        var topOffset = 190;
        if (width < 1170) {
            $("body").addClass("mini-sidebar");
            $(".sidebartoggler i").addClass("ti-menu");
        }
        else {
            $("body").removeClass("mini-sidebar");
       }
         var height = ((window.innerHeight > 0) ? window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $(".page-wrapper").css("min-height", (height) + "px");
        }
    };
    $(window).ready(set);
    $(window).on("resize", set);
    // ============================================================== 
    // Theme options
    // ==============================================================     
    $(".sidebartoggler").on('click', function () {
        if ($("body").hasClass("mini-sidebar")) {
            $("body").trigger("resize");
            $("body").removeClass("mini-sidebar");
            $('.navbar-brand span').show();
        }
        else {
            $("body").trigger("resize");
            $("body").addClass("mini-sidebar");
            $('.navbar-brand span').hide();
        }
    });
    // this is for close icon when navigation open in mobile view
    $(".nav-toggler").click(function () {
        $("body").toggleClass("show-sidebar");
        $(".nav-toggler i").toggleClass("ti-menu");
        $(".nav-toggler i").addClass("ti-close");
    });
    $(".search-box a, .search-box .app-search .srh-btn").on('click', function () {
        $(".app-search").toggle(200);
    });
    // ============================================================== 
    // Right sidebar options
    // ============================================================== 
    $(".right-side-toggle").click(function () {
        $(".right-sidebar").slideDown(50);
        $(".right-sidebar").toggleClass("shw-rside");
    });
    // ============================================================== 
    // This is for the floating labels
    // ============================================================== 
    $('.floating-labels .form-control').on('focus blur', function (e) {
        $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
    }).trigger('blur');
    
    // ============================================================== 
    //tooltip
    // ============================================================== 
    $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    // ============================================================== 
    //Popover
    // ============================================================== 
    $(function () {
         $('[data-toggle="popover"]').popover()
    })
       
    // ============================================================== 
    // Perfact scrollbar
    // ============================================================== 
    $('.right-side-panel, .message-center, .right-sidebar').perfectScrollbar();
    // ============================================================== 
    // Resize all elements
    // ============================================================== 
    $("body").trigger("resize");
    // ============================================================== 
    // To do list
    // ============================================================== 
    $(".list-task li label").click(function () {
        $(this).toggleClass("task-done");
    });
    // ============================================================== 
    // Collapsable cards
    // ==============================================================
    $('a[data-action="collapse"]').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.card').find('[data-action="collapse"] i').toggleClass('ti-minus ti-plus');
        $(this).closest('.card').children('.card-body').collapse('toggle');
    });
    // Toggle fullscreen
    $('a[data-action="expand"]').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.card').find('[data-action="expand"] i').toggleClass('mdi-arrow-expand mdi-arrow-compress');
        $(this).closest('.card').toggleClass('card-fullscreen');
    });
    // Close Card
    $('a[data-action="close"]').on('click', function () {
        $(this).closest('.card').removeClass().slideUp('fast');
    });
    // ============================================================== 
    // fixed navigattion while scrolll
    // ==============================================================
    function collapseNavbar() {
        if ($(window).scrollTop() > 80) {
            $("body").addClass("fixed-sidebar");
            $(".left-sidebar").addClass("animated slideInDown");
             
        } else {
            $("body").removeClass("fixed-sidebar");
            $(".left-sidebar").removeClass("animated slideInDown");
        }
    }
    $(window).scroll(collapseNavbar);
    collapseNavbar()
    // ============================================================== 
    // Color variation
    // ==============================================================
    
    var mySkins = [
        "skin-default",
        "skin-green",
        "skin-red",
        "skin-blue",
        "skin-purple",
        "skin-megna",
        "skin-default-dark",
        "skin-green-dark",
        "skin-red-dark",
        "skin-blue-dark",
        "skin-purple-dark",
        "skin-megna-dark"
    ]
        /**
         * Get a prestored setting
         *
         * @param String name Name of of the setting
         * @returns String The value of the setting | null
         */
    function get(name) {
        if (typeof (Storage) !== 'undefined') {
            return localStorage.getItem(name)
        }
        else {
            window.alert('Please use a modern browser to properly view this template!')
        }
    }
    /**
     * Store a new settings in the browser
     *
     * @param String name Name of the setting
     * @param String val Value of the setting
     * @returns void
     */
    function store(name, val) {
        if (typeof (Storage) !== 'undefined') {
            localStorage.setItem(name, val)
        }
        else {
            window.alert('Please use a modern browser to properly view this template!')
        }
    }
    
    /**
     * Replaces the old skin with the new skin
     * @param String cls the new skin class
     * @returns Boolean false to prevent link's default action
     */
    function changeSkin(cls) {
        $.each(mySkins, function (i) {
            $('body').removeClass(mySkins[i])
        })
        $('body').addClass(cls)
        store('skin', cls)
        return false
    }

    function setup() {
        var tmp = get('skin')
        if (tmp && $.inArray(tmp, mySkins)) changeSkin(tmp)
            // Add the change skin listener
        $('[data-skin]').on('click', function (e) {
            if ($(this).hasClass('knob')) return
            e.preventDefault()
            changeSkin($(this).data('skin'))
        })
    }
    setup()
    $("#themecolors").on("click", "a", function () {
        $("#themecolors li a").removeClass("working"), 
        $(this).addClass("working")
    })
});

/*! Idle Timer v1.1.0 2016-03-21 | https://github.com/thorst/jquery-idletimer | (c) 2016 Paul Irish | Licensed MIT */
!function(a){a.idleTimer=function(b,c){var d;"object"==typeof b?(d=b,b=null):"number"==typeof b&&(d={timeout:b},b=null),c=c||document,d=a.extend({idle:!1,timeout:3e4,events:"mousemove keydown wheel DOMMouseScroll mousewheel mousedown touchstart touchmove MSPointerDown MSPointerMove"},d);var e=a(c),f=e.data("idleTimerObj")||{},g=function(b){var d=a.data(c,"idleTimerObj")||{};d.idle=!d.idle,d.olddate=+new Date;var e=a.Event((d.idle?"idle":"active")+".idleTimer");a(c).trigger(e,[c,a.extend({},d),b])},h=function(b){var d=a.data(c,"idleTimerObj")||{};if(("storage"!==b.type||b.originalEvent.key===d.timerSyncId)&&null==d.remaining){if("mousemove"===b.type){if(b.pageX===d.pageX&&b.pageY===d.pageY)return;if("undefined"==typeof b.pageX&&"undefined"==typeof b.pageY)return;var e=+new Date-d.olddate;if(200>e)return}clearTimeout(d.tId),d.idle&&g(b),d.lastActive=+new Date,d.pageX=b.pageX,d.pageY=b.pageY,"storage"!==b.type&&d.timerSyncId&&"undefined"!=typeof localStorage&&localStorage.setItem(d.timerSyncId,d.lastActive),d.tId=setTimeout(g,d.timeout)}},i=function(){var b=a.data(c,"idleTimerObj")||{};b.idle=b.idleBackup,b.olddate=+new Date,b.lastActive=b.olddate,b.remaining=null,clearTimeout(b.tId),b.idle||(b.tId=setTimeout(g,b.timeout))},j=function(){var b=a.data(c,"idleTimerObj")||{};null==b.remaining&&(b.remaining=b.timeout-(+new Date-b.olddate),clearTimeout(b.tId))},k=function(){var b=a.data(c,"idleTimerObj")||{};null!=b.remaining&&(b.idle||(b.tId=setTimeout(g,b.remaining)),b.remaining=null)},l=function(){var b=a.data(c,"idleTimerObj")||{};clearTimeout(b.tId),e.removeData("idleTimerObj"),e.off("._idleTimer")},m=function(){var b=a.data(c,"idleTimerObj")||{};if(b.idle)return 0;if(null!=b.remaining)return b.remaining;var d=b.timeout-(+new Date-b.lastActive);return 0>d&&(d=0),d};if(null===b&&"undefined"!=typeof f.idle)return i(),e;if(null===b);else{if(null!==b&&"undefined"==typeof f.idle)return!1;if("destroy"===b)return l(),e;if("pause"===b)return j(),e;if("resume"===b)return k(),e;if("reset"===b)return i(),e;if("getRemainingTime"===b)return m();if("getElapsedTime"===b)return+new Date-f.olddate;if("getLastActiveTime"===b)return f.lastActive;if("isIdle"===b)return f.idle}return e.on(a.trim((d.events+" ").split(" ").join("._idleTimer ")),function(a){h(a)}),d.timerSyncId&&a(window).bind("storage",h),f=a.extend({},{olddate:+new Date,lastActive:+new Date,idle:d.idle,idleBackup:d.idle,timeout:d.timeout,remaining:null,timerSyncId:d.timerSyncId,tId:null,pageX:null,pageY:null}),f.idle||(f.tId=setTimeout(g,f.timeout)),a.data(c,"idleTimerObj",f),e},a.fn.idleTimer=function(b){return this[0]?a.idleTimer(b,this[0]):this}}(jQuery);