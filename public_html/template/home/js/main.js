$(document).ready(function(){
    if($.fn.owlCarousel){

        //hero banner slider
        $('.hero-banner').owlCarousel({
            items: 1,
            nav: true,
            loop: true,
            autoplay: true,
            autoplayTimeout: 10000,
            autoplaySpeed: 2000,
            navSpeed: 1000,
            dotsSpeed: 1000,
            navText: [
                "<i class='fas fa-angle-left'></i>",
                "<i class='fas fa-angle-right'></i>"
            ],
            responsiveClass: true,
            responsive: {
                0:{
                    nav: false
                },
                768:{
                    nav: true
                }
            }
        });

        //slider event
        $('.slider-event').on('initialized.owl.carousel changed.owl.carousel', function(e) {
            if (!e.namespace)  {
                return;
            }
            var carousel = e.relatedTarget;
            $('.count-slide-event').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
        }).owlCarousel({
            loop: true,
            responsiveClass: true,
            responsive: {
                1400:{
                    items: 3,
                    margin: 75,
                    center: true
                },
                1200:{
                    items: 3,
                    margin: 60,
                    center: true
                },
                992:{
                    items: 3,
                    margin: 50,
                    center: true
                },
                0:{
                    items: 1,
                    margin: 30
                }
            }
        });

        //slider webinar
        $('.slider-webinar').on('initialized.owl.carousel changed.owl.carousel', function(e) {
            if (!e.namespace)  {
                return;
            }
            var carousel = e.relatedTarget;
            $('.count-slide-blog').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
        }).owlCarousel({
            loop: false,
            margin: 30,
            responsiveClass: true,
            responsive: {
                1200:{
                    items: 2,
                },
                992:{
                    items: 1,
                },
                0:{
                    items: 1
                }
            }
        });
        //slider analysys
        $('.slider-analysis').on('initialized.owl.carousel changed.owl.carousel', function(e) {
            if (!e.namespace)  {
                return;
            }
            var carousel = e.relatedTarget;
            $('.count-slide-blog').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
        }).owlCarousel({
            loop: false,
            margin: 30,
            responsiveClass: true,
            responsive: {
                1200:{
                    items: 3,
                },
                992:{
                    items: 2,
                },
                0:{
                    items: 1
                }
            }
        });
    }

    if($.fn.countUp){
        $('.count-num').countUp();
    }

    //toggle dropdown mobile nav
    $(function($){
        $('.main-nav .megamenu > a').on('click', function(e){
            if($(this).parents('.megamenu').hasClass('collapsed')){
                $(this).next('.megamenu-content').slideUp(300, function(){
                    $(this).parents('.megamenu').removeClass('collapsed');
                });
            }else{
                $('.main-nav .megamenu > a').next('.megamenu-content').slideUp(300, function(){
                    $('.main-nav .megamenu > a').parents('.megamenu').removeClass('collapsed');
                });
                $(this).next('.megamenu-content').slideDown(300, function(){
                    $(this).parents('.megamenu').addClass('collapsed');
                });
            }

            e.preventDefault();
        });
    });

    //show or hide menu mobile
    $(function($){
        $('.btn-nav-mobile').on('click', function(e){
            $('.header').addClass('toggle-nav');
            e.preventDefault();
        });

        $('.button-close-nav').on('click', function(e){
            $('.header').removeClass('toggle-nav');
            e.preventDefault();
        })
    });

    //disable event btn dropdown
    $(function($){
        $('.dropdown-btn-outer > .btn').on('click', function(e){
            e.preventDefault();
        });
    });

    //header scroll
    $(function($){
        var prevScrollPos = $(window).scrollTop();

        $(window).on('scroll', function(){
            var currentScrollPos = $(window).scrollTop();

            if(prevScrollPos < currentScrollPos){
                $('.header').addClass('hide-top-header');
            }else{
                $('.header').removeClass('hide-top-header');
            }

            prevScrollPos = currentScrollPos;
        });
    })
});