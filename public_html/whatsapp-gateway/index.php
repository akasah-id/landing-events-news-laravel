<?php 
ob_start();
session_start();
// $_SESSION["flash"] = false;
$urlendpoint = 'http://sms114.xyz/sms/api_whatsapp_send_json.php'; // url endpoint api
$apikey      = '71056e02a488eaa58980712ad796eef8'; // api key 
$waid        = '6NJ3YV'; // whatsapp id 
// create header json  
$senddata = array(
	'apikey' => $apikey,  
	'waid' => $waid, 
	'datapacket'=>array()
);
$respon = @$_SESSION["flash"];

if (@$_GET['send']==true) {
	$number=$_GET['phone'];
	$message=$_GET['msg'];
	array_push($senddata['datapacket'],array(
		'number' => trim($number),
		'message' => $message
	));
	// sending  
	$data=json_encode($senddata);
	$curlHandle = curl_init($urlendpoint);
	curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $data);
	curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data))
	);
	curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
	curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 30);
	$respon = curl_exec($curlHandle);
	curl_close($curlHandle);
	// header('Content-Type: application/json');
	// echo $respon;
	$_SESSION["flash"] = $respon;


	$url = "http://" . $_SERVER['SERVER_NAME'].'/whatsapp-gateway/';
	header("location: $url");
} else {
	$_SESSION["flash"] = false;
}

 ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>WA Gateway</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<h1 class="text-center">WA Gateway</h1>
		<hr>
		<div class="container">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php if ($respon): ?>
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>Status</strong> <?php echo $respon ?>
					</div>
				<?php endif ?>
				<center>
					<form action="" method="GET" class="form" role="form">
						<input type="hidden" name="send" id="inputSend" class="form-control" value="true">
			
						<div class="form-group">
							<label for="input" class="col-sm-2 control-label">Phone Address</label>
							<div class="col-sm-10">
								<input type="text" name="phone" class="form-control" value="6282220000796" required="required">
							</div>
						</div>
						<div class="form-group">
							<label for="textareaMsg" class="col-sm-2 control-label">Message</label>
							<div class="col-sm-10">
								<textarea name="msg" id="textareaMsg" class="form-control" rows="3" required="required">RAHASIA! Kode konfirmasi WELTRADE Anda adalah <?php echo rand(1000,9999) ?>


JANGAN DIBAGIKAN, TERMASUK KE PIHAK WELTRADE!
Hubungi 0812-7317-080 untuk bantuan resmi WELTRADE</textarea>
							</div>
						</div>
					
						
						<br><br><br><br><br><br>
						<button type="submit" class="btn btn-primary">Send</button>
					</form>
				</center>
			</div>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	</body>
</html>