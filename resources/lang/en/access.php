<?php 
return [
	'Access Management' => 'Access Management',
	'caption' => 'Access configuration for Create, Read, Update, and Delete (CRUD) for each module',
	'Group' => 'Group',
	'Module' => 'Module',
	'Access' => 'Access',
	'Add Access' => 'Add Access',
	'Edit Access' => 'Edit Access',
	'modulemenu' => 'Module/Menu',
	'Create' => 'Create',
	'Read' => 'Read',
	'Update' => 'Update',
	'Delete' => 'Delete',
	'tes' => 'tes',
];

 ?>