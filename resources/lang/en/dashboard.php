<?php 
return [
	// Common
	'success_save' => 'Data has been saved successfully',
    'failed_save' => 'Data failed to save. Please try again',
    'success_update' => 'Data has been updated successfully',
    'failed_update' => 'Data failed to update. Please try again',
    'success_delete' => 'Data has been deleted successfully',
    'failed_delete' => 'Data failed to delete. Please try again',
    'success_bulk' => 'Bulk action success',
    'failed_bulk' => 'Bulk action failed',
    'not_found' => 'Data not found',
    'no_request' => 'No request data to proses. Please try again',
    'data_exist' => 'Data %s already exist',
    'log_add' => 'New data with id %s has been saved successfully',
    'log_edit' => 'Data with id %s has been updated successfully',
    'log_delete' => 'Data with id %s has been deleted successfully',
    'log_bulk' => 'Bulk action success',

	'Search & enter' => 'Search & enter',
	'My Profile' => 'My Profile',
	'Logout' => 'Logout',
	'My Menu' => 'My Menu',
	'Dashboard' => 'Dashboard',
	'Home' => 'Home',
	'rate' => 'Sell Rate',
	'latest' => 'Latest Deposit History',
	'top10' => 'Ten recent transactions',
	'Create New' => 'Create New',
	'Add' => 'Add',
	'Edit' => 'Edit',
	'Delete' => 'Delete',
	'Save' => 'Save',
	'Cancel' => 'Reset',
	'Reset / Refresh' => 'Reset / Refresh',
	'records selected:' => 'records selected:',

	'Action' => 'Action',
	'Name' => 'Name',
	'formName' => 'Name',
	'formDesc' => 'Description',
	'Access' => 'Access',
	
	'Concept' => 'Concept',
	'Published' => 'Published',
	'Unpublished' => 'Unpublished',

	'welcome' => 'Welcome back ',
	'editprofile' => 'Edit My Profile ',
	'recentact' => 'Recent Activities ',
];

 ?>