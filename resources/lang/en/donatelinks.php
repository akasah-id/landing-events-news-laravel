<?php 
return [
	'formCaption' => 'Available donation method',
	'method' => 'Donation Method',
	'link' => 'Link',
	'Add Donate Links' => 'Add Donate Links',
	'Edit Donate Links' => 'Edit Donate Links',
	'formdonate_links_method_id' => 'Donation Method',
	'formdonatelinks_link' => 'Donate Button Link',
]; ?>