<?php 
return [
	'formCaption' => 'Available donation method',
	'pic' => 'Picture',
	'name_desc' => 'Name & Desc',
	'link' => 'Link',
	'Add Donate Method' => 'Add Donate Method',
	'Edit Donate Method' => 'Edit Donate Method',
	'formdonatemethod_pic' => 'Picture',
	'formdonatemethod_name' => 'Method/Provider Name',
	'formdonatemethod_link' => 'Method/Provider Link',
]; ?>