<?php 
return [
	'Email Log' => 'Email Log',
	'caption' => 'All email logs',
	'Send Email' => 'Send Email',
	'formCaption' => 'Send a private email or to the whole customers',
	'user' => 'User',
	'subject' => 'Subject',
	'type' => 'Type',
	'formEmail' => 'Email',
	'formtitle' => 'Subject',
	'formtype' => 'Type',
	'formMsg' => 'Message',
	'Add Email' => 'Add Email',
	'Edit Email' => 'Edit Email',
	
	'hello' => 'Hello',
	'login' => 'Login Now',
	'userreg' => 'User Registration',
	'welcomehead' => 'Welcome to Akasah.id - Education is The Game!',
	'welcomemsg' => "we're very proud and honoured to be your partner on your journey to be more successful. Thank you for registering yourself here, it's very necessary for you to manage and monitor your transaction details. Therefore, here are your account details:",
	'forgotpass' => 'Reset Password',
	'forgotpassmsg' => "It looks like you forgot or miss your password. We're sorry for that, but don't worry, we already made a new secured one for you.",
	'password' => 'Password',
	'hello' => 'Hello',
];
 ?>