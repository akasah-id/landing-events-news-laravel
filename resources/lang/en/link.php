<?php 
return [
	'Name' => 'Name',
	'URL' => 'URL',
	'Link Shortened' => 'Link Shortened',
	'formCaption' => 'Shroten your long URL into a great short url',
	'Add Link' => 'Add Link',
	'Edit Link' => 'Edit Link',
	'formlink_name' => 'Link Name',
	'formlink_url' => 'Link URL',
	'formlink_shortened' => 'Link Shortened',
	'link_exist' => 'Shortened link already exist',
	'URL' => 'URL',
];
 ?>