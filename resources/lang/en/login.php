<?php

return [
    'Login - Akasah' => 'Login - Akasah.id',
    'Username' => 'Username',
    'Password' => 'Password',
    'Remember me' => 'Remember me',
    'Forgot pwd?' => 'Forgot password?',
    'Log In' => 'Log In',
    'Error' => 'Error',
    'We could not find your email address' => 'We could not find your email address',
    'Warning' => 'Warning',
    'Your account is not active' => 'Your account is not active',
    'The password you entered is incorrect, or was mistyped' => 'The password you entered is incorrect, or was mistyped',
    'Success' => 'Success',
    'Login Success, Welcome back' => 'Login Success, Welcome back ',
    'Recover Password' => 'Recover Password',
    'Enter your Email and instructions will be sent to you!' => 'Enter your Email and instructions will be sent to you!',
    'Reset' => 'Reset',
    'Recovery password instruction has been sent' => 'Recovery password instruction has been sent',
    'Back' => 'Back',
    'no_account' => 'Don\'t have an account?',
    'Sign Up' => 'Sign Up',
    'signupmsg' => 'Let\'s create a better world, Together! No Bullshit!',
    'Your' => 'First Name',
    'Name' => 'Last Name',
    '@yourname' => '@username',
    'youremail@mail.com' => 'youremail@mail.com',
    'pass****' => 'pass****',
    'Register' => 'Register',
    'signup_success' => 'Sign up success, confirmation link sent to your email',
    'signup_failed' => 'Sign up failed, please try again',
    'confirm_success' => 'Email confirmed, please log in :)',
    'wrong_code' => 'Wrong confirmation token, please try again',
    'confirm_already' => 'Email already confirmed, please log in :)',
];
