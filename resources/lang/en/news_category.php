<?php 
return [
	'formCaption' => 'News Category along with hashtag and related categories',
	'Category Name' => 'Category Name',
	'Category Hashtag' => 'Category Hashtag',
	'Category Related' => 'Category Related',
	'Add News Category' => 'Add News Category',
	'Edit News Category' => 'Edit News Category',
	'formcategory_image' => 'Category Image',
	'formcategory_name' => 'Category Name',
	'formcategory_hashtag' => 'Category Hashtag',
	'formcategory_desc' => 'Category Desc',
	'formcategory_related' => 'Category Related',
]; ?>