<?php 
return [
	'formCaption' => 'News Rating along with users who rated it',
	'News' => 'News',
	'Overall Rate' => 'Overall Rate',
	'View Raters' => 'View Reviewers',
	'Reviewers' => 'Reviewers',
	'Okay' => 'Okay',
]; ?>