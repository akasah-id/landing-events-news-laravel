<?php 
return [
	'formCaption' => 'Inspiration, motivation, and discussion wrapped up in a Podcast',
	'Title' => 'Title',
	'Caption' => 'Caption',
	'Contributors' => 'Contributors',
	'Categories' => 'Categories',
	'Close' => 'Close',
	'Add Podcast' => 'Add Podcast',
	'Edit Podcast' => 'Edit Podcast',
	'formpodcast_title' => 'Title',
	'formpodcast_caption' => 'Caption',
	'formpodcast_poster' => 'Poster',
	'formpodcast_desc' => 'Description & Transcript',
	'formpodcast_file' => 'Podcast Audio File',
	'formpodcast_duration' => 'Duration (in seconds)',
	'formpodcast_categories_id' => 'Categories',
	'formpodcast_contributors' => 'Contributors',
]; ?>