<?php 
return [
	'Access Management' => 'Manajemen Akses',
	'caption' => 'Konfigurasi Akses untuk Tambah(Create), Baca(Read), Perbaharui(Update), dan Hapus(Delete) (CRUD) untuk setiap Modul',
	'Group' => 'Grup',
	'Module' => 'Modul',
	'Access' => 'Akses',
	'Add Access' => 'Tambah Akses',
	'Edit Access' => 'Sunting Akses',
	'modulemenu' => 'Modul/Menu',
	'Create' => 'Tambah',
	'Read' => 'Baca',
	'Update' => 'Perbaharui',
	'Delete' => 'Hapus',
	'tes' => 'tes',
];

 ?>