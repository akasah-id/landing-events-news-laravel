<?php 
return [
	'formCaption' => 'Mari kita buat artikel yang bermanfaat',
	'view' => 'Ditampilkan',
	'creators' => 'Pembuat dan Kontributor',
	'title' => 'Judul',
	'status' => 'Status',
	'Add Articles' => 'Tambah Artikel',
	'Edit Articles' => 'Sunting Artikel',
	'formarticles_creator' => 'Pembuat',
	'formarticles_contributors' => 'Kontributor',
	'formarticles_faq_links' => 'Link FAQ',
	'formarticles_faq_name' => 'Nama',
	'formarticles_faq_url' => 'Link',
	'formarticles_faq_addmore' => 'Tambah',
	'formarticles_faq_remove' => 'Hapus',
	
	'Previous' => 'Sebelumnya',
	'Next' => 'Selanjutnya',
	'ThanksNote' => 'Thank you for your contribution here, this one story could possibily save at least one person in his/her life!',
	'Article Saved!' => 'Artikel Tersimpan!',
	'Category & Meta' => 'Kontributor & Meta',
	'Headlines' => 'Judul',
	'Contents' => 'Konten',
	'formarticles_category' => 'Kategori',
	'formarticles_metaimage' => 'Meta Gambar',
	'formarticles_metakey' => 'Meta Kunci',
	'formarticles_metadesc' => 'Meta Deskripsi',
	'formarticles_title' => 'Judul',
	'formarticles_subtitle' => 'Subjudul',
	'formarticles_voicetext' => 'Teks Suara',
	'formarticles_voice' => 'File Suara',
	'formarticles_status' => 'Status',
	'formarticles_layout' => 'Tata Letak',
	'formarticles_landscapefull' => 'Landscape Penuh',
	'formarticles_landscapehalf' => 'Landscape Setengah',
	'formarticles_potrait' => 'Potrait',
	'formarticles_image' => 'Gambar',
	'formarticles_content' => 'Konten',
	'formarticles_image_source' => 'Sumber Gambar (Jika ada)',
	'formarticles_content_source' => 'Sumber Konten (Jika ada)',
	'formarticles_lang' => 'Bahasa',
	'id' => 'Indonesia',
	'en' => 'Inggris',
]; ?>