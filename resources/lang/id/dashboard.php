<?php 
return [
	// Common
	'success_save' => 'Data berhasil disimpan',
    'failed_save' => 'Data gagal disimpan. Silahkan coba lagi',
    'success_update' => 'Data berhasil diperbaharui',
    'failed_update' => 'Data gagal diperbaharui. Silahkan coba lagi',
    'success_delete' => 'Data berhasil dihapus',
    'failed_delete' => 'Data gagal dihapus. Silahkan coba lagi',
    'success_bulk' => 'Aksi massal berhasil',
    'failed_bulk' => 'Aksi massal gagal',
    'not_found' => 'Data tidak ditemukan',
    'no_request' => 'Tidak ada permintaan data. Silahkan coba lagi',
    'data_exist' => 'Data %s sudah ada',
    'log_add' => 'New data with id %s has been saved successfully',
    'log_edit' => 'Data with id %s has been updated successfully',
    'log_delete' => 'Data with id %s has been deleted successfully',
    'log_bulk' => 'Bulk action success',

	'Search & enter' => 'Pencarian',
	'My Profile' => 'Profil Saya',
	'Logout' => 'Keluar',
	'My Menu' => 'Menu Saya',
	'Dashboard' => 'Dasbor',
	'Home' => 'Beranda',
	'rate' => 'Nilai Jual',
	'latest' => 'Sejarah Deposit Terakhir',
	'top10' => '10 Transaksi Terbaru',
	'Create New' => 'Tambah Baru',
	'Add' => 'Tambah',
	'Edit' => 'Sunting',
	'Delete' => 'Hapus',
	'Save' => 'Simpan',
	'Cancel' => 'Kosongkan',
	'Reset / Refresh' => 'Muat Ulang Data',
	'records selected:' => 'data terpilih:',

	'Action' => 'Aksi',
	'Name' => 'Nama',
	'formName' => 'Nama',
	'formDesc' => 'Deskripsi',
	'Access' => 'Akses',
	
	'Concept' => 'Konsep',
	'Published' => 'Terbit',
	'Unpublished' => 'Disembunyikan',

	'welcome' => 'Selamat Datang ',
	'editprofile' => 'Sunting Profil Saya',
	'recentact' => 'Aktifitas Terbaru',
];

 ?>