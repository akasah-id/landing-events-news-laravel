<?php 
return [
	'formCaption' => 'Metode Donasi Tersedia',
	'pic' => 'Gambar',
	'name_desc' => 'Nama dan Deskripsi',
	'link' => 'Link',
	'Add Donate Method' => 'Tambah Metode Donasi',
	'Edit Donate Method' => 'Sunting Metode Donasi',
	'formdonatemethod_pic' => 'Gambar',
	'formdonatemethod_name' => 'Nama Metode/Penyedia',
	'formdonatemethod_link' => 'Link Metode/Penyedia',
]; ?>