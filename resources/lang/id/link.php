<?php 
return [
	'Name' => 'Nama',
	'URL' => 'URL',
	'Link Shortened' => 'Link Singkat',
	'formCaption' => 'Pendekkan URL panjang Anda dengan URL yang hebat dan pendek',
	'Add Link' => 'Tambah Link',
	'Edit Link' => 'Sunting Link',
	'formlink_name' => 'Nama Link',
	'formlink_url' => 'URL Link',
	'formlink_shortened' => 'Link Singkat',
	'link_exist' => 'Link Singkat sudah ada',
	'URL' => 'URL',
];
 ?>