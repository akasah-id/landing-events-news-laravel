<?php 
return [
	// Admin Menu
	'Dashboard' => 'Dasbor',
	'System Support' => 'Dukungan Sistem',
	'Menu Management' => 'Manajemen Menu',
	'User Management' => 'Manajemen Pengguna',
	'Group Management' => 'Manajemen Grup',
	'Log Activity' => 'Log Aktivitas',
	'Email Log' => 'Email Log',
	'Master Data' => 'Data Master',
	'Lock Screen' => 'Layar Kunci',
	'Link Shortener' => 'Penyingkat Link',
	'News Management' => 'Manajemen Berita',
	'News Category' => 'Kategori Berita',
	'News Rating' => 'Penilaian Berita',
	'Tools' => 'Alat Bantuan',
	'File Manager' => 'Manajemen File',
	'Events Management' => 'Manajemen Acara',
	'Podcast Management' => 'Manajemen Podcast',
	'Wazzap' => 'Wazzap',
	'Education' => 'Edukasi',
	'Profession' => 'Profesi',
	'Skill Sets' => 'Set Keterampilan',
	'Skill Requirements' => 'Syarat Keterampilan',
	'Articles' => 'Artikel',
	'Donate Method' => 'Metode Donasi',
	'Donate Links' => 'Link Donasi',
	'My Donation' => 'Donasi Saya',
	'Post Weltrade' => 'Artikel',

	'Menu' => 'Manajemen Menu',
	'Update Menu Order' => 'Perbaharui Urutan Menu',
	'Success' => 'Sukses',
	'Data successfully updated' => 'Data berhasil diperbaharui',
	'Add Menu' => 'Tambah Menu',
	'Edit Menu' => 'Sunting Menu',
	'addMenu' => 'Tambah Menu',
	'addMenuCaption' => 'Menu ini untuk halaman admin',
	'menuName' => 'Nama',
	'menuType' => 'Tipe',
	'menuModule' => 'Modul',
	'menuURL' => 'URL',
	'menuIcon' => 'Ikon',
	'tes' => 'tes',
];

 ?>