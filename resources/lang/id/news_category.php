<?php 
return [
	'formCaption' => 'Kategori Berita beserta tagar dan kategori terkait',
	'Category Name' => 'Nama Kategori',
	'Category Hashtag' => 'Tagar Kategori',
	'Category Related' => 'Kategori Terkait',
	'Add News Category' => 'Tambah Kategori Berita',
	'Edit News Category' => 'Sunting Kategori Berita',
	'formcategory_image' => 'Gambar Kategori',
	'formcategory_name' => 'Nama Kategori',
	'formcategory_hashtag' => 'Tagar Kategori',
	'formcategory_desc' => 'Deskripsi Kategori',
	'formcategory_related' => 'Kategori Terkait',
]; ?>