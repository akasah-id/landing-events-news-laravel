<?php 
return [
	'formCaption' => 'Penilaian Berita beserta pengguna yang menilai dan mengulasnya',
	'News' => 'Berita',
	'Overall Rate' => 'Nilai Keseluruhan',
	'View Raters' => 'Tampilkan Pengulas',
	'Reviewers' => 'Pengulas',
	'Okay' => 'Oke',
]; ?>