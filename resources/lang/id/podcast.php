<?php 
return [
	'formCaption' => 'Inspirasi, motivasi dan diskusi dirangkum dalam sebuah Podcast',
	'Title' => 'Judul',
	'Caption' => 'Keterangan',
	'Contributors' => 'Kontributor',
	'Categories' => 'Kategori',
	'Close' => 'Tutup',
	'Add Podcast' => 'Tambah Podcast',
	'Edit Podcast' => 'Sunting Podcast',
	'formpodcast_title' => 'Judul',
	'formpodcast_caption' => 'Keterangan',
	'formpodcast_poster' => 'Gambar',
	'formpodcast_desc' => 'Deskripsi dan Transkrip',
	'formpodcast_file' => 'File Suara Podcast',
	'formpodcast_duration' => 'Durasi (detik)',
	'formpodcast_categories_id' => 'Kategori',
	'formpodcast_contributors' => 'Kontributor',
]; ?>