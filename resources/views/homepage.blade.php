<!DOCTYPE html>
<html lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Akasah" />
	<link rel="shortcut icon" href="<?php echo url('/') ?>/template/cooming_soon/favicon.png">

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/style.css" type="text/css" />

	<!-- One Page Module Specific Stylesheet -->
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/onepage.css" type="text/css" />
	<!-- / -->

	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/css/et-line.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/css/fonts.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />


	<!-- Document Title
	============================================= -->
	<title>Akasah.id - Education is the game!</title>

</head>

<body class="stretched dark side-push-panel">

	<div class="body-overlay"></div>

	<div id="side-panel" class="dark">

		<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>

		<div class="side-panel-wrap">

			<div class="widget widget_links clearfix">

				<h4>About Akasah</h4>

				<div style="font-size: 14px; line-height: 1.7;">
					<address style="line-height: 1.7;">
						Jakarta, Indonesia<br>
					</address>

					<div class="clear topmargin-sm"></div>

					<abbr title="Phone Number">Phone:</abbr> (+62) 8 222 0000 796<br>
					<abbr title="Email Address">Email:</abbr> hello@triakasah.asia
				</div>

			</div>

			<div class="widget quick-contact-widget clearfix">

				<h4>Connect Socially</h4>

				<a href="https://www.facebook.com/triakasahfp/" target="_blank" class="social-icon si-small si-colored si-facebook" title="Facebook">
					<i class="icon-facebook"></i>
					<i class="icon-facebook"></i>
				</a>
				<a href="https://twitter.com/triakasah" target="_blank" class="social-icon si-small si-colored si-twitter" title="Twitter">
					<i class="icon-twitter"></i>
					<i class="icon-twitter"></i>
				</a>
				<a href="https://github.com/triakasah" target="_blank" class="social-icon si-small si-colored si-github" title="Github">
					<i class="icon-github"></i>
					<i class="icon-github"></i>
				</a>
				<a href="https://www.linkedin.com/in/triakasah/" target="_blank" class="social-icon si-small si-colored si-linkedin" title="LinkedIn">
					<i class="icon-linkedin"></i>
					<i class="icon-linkedin"></i>
				</a>
				<a href="https://plus.google.com/+TriAkasah" target="_blank" class="social-icon si-small si-colored si-gplus" title="Google Plus">
					<i class="icon-gplus"></i>
					<i class="icon-gplus"></i>
				</a>
				<a href="https://www.instagram.com/tri.akasah/" target="_blank" class="social-icon si-small si-colored si-instagram" title="Instagram">
					<i class="icon-instagram"></i>
					<i class="icon-instagram"></i>
				</a>

			</div>

		</div>

	</div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="full-header transparent-header static-sticky" data-sticky-offset="full" data-sticky-offset-negative="100">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="#" class="standard-logo" data-dark-logo="<?php echo url('/') ?>/template/cooming_soon/logo.png"><img src="<?php echo url('/') ?>/template/cooming_soon/logo.png" alt="Akasah Logo"></a>
						<a href="#" class="retina-logo" data-dark-logo="<?php echo url('/') ?>/template/cooming_soon/logo.png"><img src="<?php echo url('/') ?>/template/cooming_soon/logo.png" alt="Akasah Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">

						<!-- <ul class="one-page-menu" data-easing="easeInOutExpo" data-speed="1250" data-offset="65">
							<li><a href="#" data-href="#wrapper"><div>Home</div></a></li>
							<li><a href="#" data-href="#section-about"><div>About</div></a></li>
							<li><a href="#" data-href="#section-works"><div>Works</div></a></li>
							<li><a href="#" data-href="#section-services"><div>Services</div></a></li>
							<li><a href="#" data-href="#section-blog"><div>Blog</div></a></li>
							<li><a href="#" data-href="#section-contact"><div>Contact</div></a></li>
						</ul> -->

						<div id="side-panel-trigger" class="side-panel-trigger"><a href="#"><i class="icon-reorder"></i></a></div>

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->

		<!-- Slider
		============================================= -->
		<section id="slider" class="slider-element slider-parallax full-screen force-full-screen">

			<div class="slider-parallax-inner">

				<div class="full-screen force-full-screen dark section nopadding nomargin noborder ohidden">

					<div class="container center">
						<div class="vertical-middle ignore-header">
							<div class="emphasis-title">
								<h1>
									<span class="text-rotater nocolor" data-separator="|" data-rotate="fadeIn" data-speed="2200">
										<span class="t-rotate t700 font-body opm-large-word">education.|is.|the.|game.|akasah.id</span>
									</span>
								</h1>
							</div>
						</div>
					</div>

					<div class="video-wrap">
	                    <video id="slide-video" poster="<?php echo url('/') ?>/template/cooming_soon/images/videos/1.jpg" preload="auto" loop muted autoplay>
	                        <source src='<?php echo url('/') ?>/template/cooming_soon/images/videos/1.webm' type='video/webm' />
	                        <source src='<?php echo url('/') ?>/template/cooming_soon/images/videos/1.mp4' type='video/mp4' />
	                    </video>
	                    <div class="video-overlay" style="background-color: rgba(0,0,0,0.2);"></div>
	                </div>

					<a href="#" data-scrollto="#readmore" data-easing="easeInOutExpo" data-speed="1250" data-offset="65" class="one-page-arrow dark"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

				</div>

			</div>

		</section><!-- #slider end -->

		<section id="readmore">
			<iframe scrolling="no" onload="resizeIframe(this)" src="<?php echo url('/') ?>/template/cooming_soon/index_2.html" style='border: 0; position:absolute; left:0; right:0; width:100%; height:1000px'></iframe>
		</section>

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="<?php echo url('/') ?>/template/cooming_soon/master/js/jquery.js"></script>
	<script src="<?php echo url('/') ?>/template/cooming_soon/master/js/plugins.js"></script>
	<script>
	  function resizeIframe(obj) {
	    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
	  }
	</script>

	<!-- Footer Scripts
	============================================= -->
	<script src="<?php echo url('/') ?>/template/cooming_soon/master/js/functions.js"></script>
</body>
</html>