<!DOCTYPE html>
<html lang="id">

<head>
    <!-- Standard Meta -->
    <meta charset="utf-8">
    <meta name="theme-color" content="#487EAC" />
    <meta name="robots" content="index, follow" />
    <meta name="keywords" content="weltrade, trading, weltrade indonesia, bisnis online, usaha alternatif, trading online" />
    <meta name="description" content="@yield('title') 15 tahun berpengalaman dan konsisten melayani trader berkelas didunia! Komitmen kami tetap selalu berada disisi sahabat trader Indonesia tak pernah pudar dan akan selalu terjaga! Rayakan 15 Tahun Weltrade, Komisi IB Tertinggi, Kontes Demo $100.000, Kontes IB $50.000, Trade2Win $10.000," />
    <!-- Site Properties -->
    <title>@yield('title')</title>
    <meta name="author" content="WELTRADE">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="https://<?php echo $weltrade ?>/images/wt_preview.png">
    <meta property="og:locale" content="id-ID">
    <meta property="og:title" content="@yield('title')">
    <meta property="og:description" content="@yield('title') 15 tahun berpengalaman dan konsisten melayani trader berkelas didunia! Komitmen kami tetap selalu berada disisi sahabat trader Indonesia tak pernah pudar dan akan selalu terjaga! Rayakan 15 Tahun Weltrade, Komisi IB Tertinggi, Kontes Demo $100.000, Kontes IB $50.000, Trade2Win $10.000,">
    <meta property="og:url" content="https://<?php echo $weltrade ?>/">
    <meta itemprop="image" content="https://<?php echo $weltrade ?>/images/wt180x180.png">
    <meta property="twitter:image:src" content="https://<?php echo $weltrade ?>/images/wt180x180.png">
    <link rel="author" href="https://www.facebook.com/weltradecom/" />
    <link rel="publisher" href="https://www.facebook.com/weltradecom/" />
    <link rel="canonical" href="https://<?php echo $weltrade ?>/" />
    <!-- Critical preload -->
    <link rel="preload" href="<?php echo url('/') ?>/template/landing/js/vendors/uikit.min.js" as="script">
    <link rel="preload" href="<?php echo url('/') ?>/template/landing/css/vendors/uikit.min.css" as="style">
    <link rel="preload" href="<?php echo url('/') ?>/template/landing/css/style.css" as="style">
    <!-- Icon preload -->
    <link rel="preload" href="<?php echo url('/') ?>/template/landing/fonts/fa-brands-400.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?php echo url('/') ?>/template/landing/fonts/fa-solid-900.woff2" as="font" type="font/woff2" crossorigin>
    <!-- Font preload -->
    <link rel="preload" href="<?php echo url('/') ?>/template/landing/fonts/rubik-v9-latin-500.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?php echo url('/') ?>/template/landing/fonts/rubik-v9-latin-300.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="<?php echo url('/') ?>/template/landing/fonts/rubik-v9-latin-regular.woff2" as="font" type="font/woff2" crossorigin>
    <!-- Favicon and apple icon -->
    <link rel="shortcut icon" href="https://<?php echo $weltrade ?>/images/wt180x180.png" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="https://<?php echo $weltrade ?>/images/wt180x180.png">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="<?php echo url('/') ?>/template/landing/css/vendors/uikit.min.css">
    <link rel="stylesheet" href="<?php echo url('/') ?>/template/landing/css/style.css">
</head>

<body>
    <!-- preloader begin -->
    <div class="in-loader">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <!-- preloader end -->
    <header>
        <!-- header content begin -->
        <div class="uk-section uk-padding-remove-vertical in-header-home ">
            <!-- module navigation begin -->
            <nav class="uk-navbar-container uk-navbar-transparent" data-uk-sticky="show-on-up: true; top: 80; animation: uk-animation-fade;">
                <div class="uk-container" data-uk-navbar>
                    <div class="uk-navbar-left uk-width-auto">
                        <div class="uk-navbar-item">
                            <!-- module logo begin -->
                            <a class="uk-logo" href="https://<?php echo $weltradeidn ?>">
                                <img class="uk-margin-small-right in-offset-top-10" src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-logo-white.svg" alt="Weltrade" width="180" height="23" data-uk-img>
                            </a>
                            <!-- module logo begin -->
                        </div>
                    </div>
                    <div class="uk-navbar-right uk-width-expand uk-flex uk-flex-right">
                        <ul class="uk-navbar-nav uk-visible@m">
                            <li><a href="https://<?php echo $weltrade ?>/promotions/demo-contest/?r1=ids&r2=landing_<?php echo $referral ?>_democontest"><i class="fas fa-gift uk-margin-small-right" style="font-size: 1.33333em;"></i>15 Tahun Weltrade</a></li>
                            <li><a href="https://<?php echo $weltrade ?>/partners/?r1=ids&r2=landing_<?php echo $referral ?>_partner"><i class="fas fa-handshake uk-margin-small-right" style="font-size: 1.33333em;"></i>Komisi IB Tertinggi</a></li>
                            <li><a href="https://<?php echo $weltrade ?>/webterminal/?r1=ids&r2=landing_<?php echo $referral ?>"><i class="fas fa-cloud uk-margin-small-right" style="font-size: 1.33333em;"></i>Web Platform</a></li>
                        </ul>
                        <div class="uk-navbar-item uk-visible@m in-optional-nav">
                            <a href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802" class="uk-button uk-button-text"><i class="fas fa-phone-alt uk-margin-small-right"></i>WhatsApp Kami</a>
                            <a href="https://account.<?php echo $weltrade ?>/auth/registration/?r1=ids&r2=landing_<?php echo $referral ?>" class="uk-button uk-button-primary uk-button-small uk-border-pill"><i class="fas fa-user-circle uk-margin-small-right"></i>Buka Akun</a>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- module navigation end -->
            <div class="uk-container">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div class="uk-card uk-card-secondary uk-card-small uk-card-body uk-border-rounded">
                            <div class="uk-grid uk-text-small" data-uk-grid>
                                <div class="uk-width-expand@m uk-text-center">
                                    <a class="uk-margin-right" href="https://<?php echo $weltrade ?>/promotions/demo-contest/?r1=ids&r2=landing_<?php echo $referral ?>_democontest"><i class="fas fa-coins uk-margin-small-right" style="font-size: 1.33333em;"></i>Kontes Demo "Stock Battle" $100.000</a>
                                    <a class="uk-margin-right" href="https://<?php echo $weltrade ?>/promotions/ib_contest/?r1=ids&r2=landing_<?php echo $referral ?>_ibcontest"><i class="fas fa-coins uk-margin-small-right" style="font-size: 1.33333em;"></i>Kontes IB $50.000</a>
                                    <a class="uk-margin-right" href="https://<?php echo $weltrade ?>/promotions/trade2win/?r1=ids&r2=landing_<?php echo $referral ?>_trade2win"><i class="fas fa-coins uk-margin-small-right" style="font-size: 1.33333em;"></i>Trade2Win $10.000</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- header content end -->
    </header>
    <main>
        @yield('main_section')
        <!-- section content begin -->
        <div class="uk-section uk-section-primary uk-section-xsmall">
            <div class="uk-container in-wave-1">
                <div class="uk-grid uk-grid-divider uk-child-width-1-2@s uk-child-width-1-4@m in-margin-top@s in-margin-bottom@s" data-uk-grid>
                    <div>
                        <div class="uk-grid uk-grid-small uk-flex uk-flex-middle">
                            <div class="uk-width-auto">
                                <img src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-wave-icon-1.svg" alt="wave-icon" width="48" height="48" data-uk-img>
                            </div>
                            <div class="uk-width-expand">
                                <p>Depo Instan <br>WD 30 Menit</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="uk-grid uk-grid-small uk-flex uk-flex-middle">
                            <div class="uk-width-auto">
                                <img src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-wave-icon-2.svg" alt="wave-icon" width="48" height="48" data-uk-img>
                            </div>
                            <div class="uk-width-expand">
                                <p>Spread Terendah <br>EUR/USD 0.3</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="uk-grid uk-grid-small uk-flex uk-flex-middle">
                            <div class="uk-width-auto">
                                <img src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-wave-icon-3.svg" alt="wave-icon" width="48" height="48" data-uk-img>
                            </div>
                            <div class="uk-width-expand">
                                <p>Bebas Komisi <br>Bebas Swap</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="uk-grid uk-grid-small uk-flex uk-flex-middle">
                            <div class="uk-width-auto">
                                <img src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-wave-icon-12.svg" alt="wave-icon" width="48" height="48" data-uk-img>
                            </div>
                            <div class="uk-width-expand">
                                <p>Kontes Demo <br>Kontes IB</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- section content begin -->
        <div class="uk-section uk-padding-large">
            <div class="uk-container">
                <div class="uk-grid">
                    <div class="uk-width-1-1 in-wave-16">
                        <div class="uk-card uk-card-default uk-box-shadow-small uk-border-rounded in-margin-medium-top@s">
                            <div class="uk-grid" data-uk-grid>
                                <div class="uk-width-expand@s uk-width-2-5@m uk-card-media-right uk-cover-container">
                                    <img class="uk-width-1-1 uk-margin-remove-bottom" src="<?php echo url('/') ?>/template/landing/img/in-wave-image-2.jpg" alt="sample-image">
                                </div>
                                <div class="uk-width-1-2@s uk-width-expand@m uk-flex uk-flex-middle">
                                    <div class="uk-card-body">
                                        <h1>Dipercaya Lebih Dari<br><span class="in-highlight">15 Tahun</span></h1>
                                        <p>
Lebih dari 15 tahun Weltrade berpengalaman di industri inni dan konsisten tetap melayani seluruh trader berkelas diseluruh dunia! Komitmen kami tetap berada disisi sahabat trader Indonesia tak pernah pudar dan akan selalu terjaga! <br>
Lebih dari 500 ribu trader berkelas dari 180 negara telah membuktikannya, kami tunggu kedatangan Anda di Weltrade dan segera nikmati berbagai keuntungan besar bersama kami!</p>
                                        <h4 class="uk-margin-remove-bottom uk-visible@m">Pemegang Berbagai Penghargaan</h4>
                                        <hr class="uk-visible@m">
                                        <div class="uk-child-width-1-3@m uk-text-center uk-visible@m" data-uk-grid>
                                            <div>
                                                <img src="<?php echo url('/') ?>/template/landing/img/in-wave-award.svg" alt="wave-award">
                                                <h6 class="uk-margin-small-top uk-margin-remove-bottom">Most Trusted in Asia</h6>
                                                <p class="uk-text-small uk-margin-remove-top">2021</p>
                                            </div>
                                            <div>
                                                <img src="<?php echo url('/') ?>/template/landing/img/in-wave-award.svg" alt="wave-award">
                                                <h6 class="uk-margin-small-top uk-margin-remove-bottom">Best Foreign Broker</h6>
                                                <p class="uk-text-small uk-margin-remove-top">2019</p>
                                            </div>
                                            <div>
                                                <img src="<?php echo url('/') ?>/template/landing/img/in-wave-award.svg" alt="wave-award">
                                                <h6 class="uk-margin-small-top uk-margin-remove-bottom">Best Affiliate Program</h6>
                                                <p class="uk-text-small uk-margin-remove-top">2015</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-1 uk-margin-medium-top">
                        <div class="uk-grid-divider" data-uk-grid>
                            <div class="uk-width-auto@m uk-flex uk-flex-middle">
                                <p class="uk-text-lead uk-text-muted uk-margin-right">Metode Pembayaran</p>
                            </div>
                            <div class="uk-width-expand@m">
                                <div class="uk-grid-medium uk-child-width-1-4@s uk-child-width-1-5@m uk-text-center in-client-logo-3" data-uk-grid>
                                    <div class="uk-tile uk-tile-default">
                                        <img class="uk-margin-remove" src="<?php echo url('/') ?>/template/landing/img/in-wave-visa.svg" alt="client-logo" width="147" height="42">
                                    </div>
                                    <div class="uk-tile uk-tile-default">
                                        <img class="uk-margin-remove" src="<?php echo url('/') ?>/template/landing/img/in-wave-mastercard.svg" alt="client-logo" width="147" height="42">
                                    </div>
                                    <div class="uk-tile uk-tile-default">
                                        <img class="uk-margin-remove" src="<?php echo url('/') ?>/template/landing/img/in-wave-skrill.svg" alt="client-logo" width="147" height="42">
                                    </div>
                                    <div class="uk-tile uk-tile-default">
                                        <img class="uk-margin-remove" src="<?php echo url('/') ?>/template/landing/img/in-wave-neteller.svg" alt="client-logo" width="147" height="42">
                                    </div>
                                    <div class="uk-tile uk-tile-default" style="padding: 10px 0px;">
                                        <img class="uk-margin-remove" src="<?php echo url('/') ?>/template/landing/img/in-wave-paypal.svg" alt="client-logo" width="147" height="42">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
        <!-- section content begin -->
        <div class="uk-section in-wave-17 in-offset-top-20 in-offset-bottom-40">
            <div class="uk-container">
                <div class="uk-grid" data-uk-grid>
                    <div class="uk-width-1-3@m">
                        <h1>Bisa untung dari trading?<br><span class="in-highlight">Bisa Banget!</span></h1>
                        <p>Butuh bantuan untuk mempelajarinya? jangan khawatir kami disini senantiasa menemani Anda untuk belajar!</p>
                        <a class="uk-button uk-button-primary uk-border-rounded" href="https://account.<?php echo $weltrade ?>/auth/registration/?r1=ids&r2=landing_<?php echo $referral ?>">Buka Akun</a>
                        <p class="uk-text-small">Belum siap? <a href="https://account.<?php echo $weltrade ?>/auth/registration/?r1=ids&r2=landing_<?php echo $referral ?>">Buka akun demo</a></p>
                    </div>
                    <div class="uk-width-expand@m">
                        <div class="uk-grid uk-grid-collapse uk-child-width-1-3@m uk-child-width-1-2@s uk-text-center">
                            <div class="uk-tile uk-tile-default">
                                <img class="uk-margin-remove-bottom" src="<?php echo url('/') ?>/template/landing/img/in-wave-icon-17.svg" alt="wave-icon" width="64">
                                <h5 class="uk-margin-small-top">Layanan Dukungan 24/7</h5>
                            </div>
                            <div class="uk-tile uk-tile-default">
                                <img class="uk-margin-remove-bottom" src="<?php echo url('/') ?>/template/landing/img/in-wave-icon-9.svg" alt="wave-icon" width="64">
                                <h5 class="uk-margin-small-top">Keamanan Dana dan Data</h5>
                            </div>
                            <div class="uk-tile uk-tile-default">
                                <img class="uk-margin-remove-bottom" src="<?php echo url('/') ?>/template/landing/img/in-wave-icon-15.svg" alt="wave-icon" width="64">
                                <h5 class="uk-margin-small-top">Eksekusi Secepat Kilat</h5>
                            </div>
                            <div class="uk-tile uk-tile-default">
                                <img class="uk-margin-remove-bottom" src="<?php echo url('/') ?>/template/landing/img/in-wave-icon-8.svg" alt="wave-icon" width="64">
                                <h5 class="uk-margin-small-top">Regulasi Internasional</h5>
                            </div>
                            <div class="uk-tile uk-tile-default uk-visible@m">
                                <img class="uk-margin-remove-bottom" src="<?php echo url('/') ?>/template/landing/img/in-wave-icon-14.svg" alt="wave-icon" width="64">
                                <h5 class="uk-margin-small-top">Leverage 1:1000</h5>
                            </div>
                            <div class="uk-tile uk-tile-default uk-visible@m">
                                <img class="uk-margin-remove-bottom" src="<?php echo url('/') ?>/template/landing/img/in-wave-icon-4.svg" alt="wave-icon" width="64">
                                <h5 class="uk-margin-small-top">Instrumen Trading Lengkap</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- section content begin -->
        <hr>
        <div class="uk-section" style="padding-top: 0px;">
            <div class="uk-container in-wave-19">
                <div class="uk-grid uk-flex uk-flex-center">
                    <div class="uk-width-expand@m">
                        <div class="uk-grid-divider uk-child-width-1-3@s uk-child-width-1-3@m uk-margin-medium-top uk-margin-bottom" data-uk-grid>
                            <div>
                                <div class="uk-flex uk-flex-middle">
                                    <div class="uk-margin-small-right">
                                        <i class="fas fa-edit fa-2x in-icon-wrap small transparent"></i>
                                    </div>
                                    <div>
                                        <h4 class="uk-margin-remove">Daftar</h4>
                                    </div>
                                </div>
                                <p>Silahkan daftar, caranya mudah banget.</p>
                            </div>
                            <div>
                                <div class="uk-flex uk-flex-middle">
                                    <div class="uk-margin-small-right">
                                        <i class="fas fa-money-bill-wave fa-2x in-icon-wrap small transparent"></i>
                                    </div>
                                    <div>
                                        <h4 class="uk-margin-remove">Depo - Bonus 100%</h4>
                                    </div>
                                </div>
                                <p>Gandakan keuntungan dengan bonus 100%.</p>
                            </div>
                            <div>
                                <div class="uk-flex uk-flex-middle">
                                    <div class="uk-margin-small-right">
                                        <i class="fas fa-chart-line fa-2x in-icon-wrap small transparent"></i>
                                    </div>
                                    <div>
                                        <h4 class="uk-margin-remove">Trading</h4>
                                    </div>
                                </div>
                                <p>Begitu saja, Anda sudah bisa trading.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
        <!-- section content end -->
        <!-- section content begin -->
        <div class="uk-section uk-section-muted uk-padding-remove-vertical uk-background-contain uk-background-center-right uk-background-image@m" style="background-image: url(<?php echo url('/') ?>/template/landing/img/in-wave-platformbg.jpg);">
            <div class="uk-container in-wave-18">
                <div class="uk-grid-large uk-flex uk-flex-middle" data-uk-grid>
                    <div class="uk-width-1-2@m">
                        <div class="uk-margin-bottom">
                            <a href="https://play.google.com/store/apps/details?id=net.metaquotes.metatrader4" class="uk-button in-button-app">
                                <i class="fab fa-google-play fa-2x"></i>
                                <span class="wrapper">Download di<span>Play Store</span></span>
                            </a>
                            <a href="https://apps.apple.com/us/app/metatrader-4/id496212596" class="uk-button in-button-app uk-margin-medium-left">
                                <i class="fab fa-apple fa-2x"></i>
                                <span class="wrapper">Download di<span>App Store</span></span>
                            </a>
                            <a href="https://<?php echo $weltrade ?>/webterminal/" class="uk-button in-button-app uk-margin-medium-left uk-visible@m">
                                <i class="fab fa-windows fa-2x"></i>
                                <span class="wrapper">Web<span>Platform</span></span>
                            </a>
                        </div>
                        <hr>
                        <h1 class="uk-margin-remove">Trading di <span class="in-highlight">platform</span> terbaik</h1>
                        <p class="uk-margin-medium-bottom in-margin-remove-bottom@s"></p>
                    </div>
                    <div class="uk-width-1-2@m">
                        <img class="uk-align-center uk-align-left@m" src="<?php echo url('/') ?>/template/landing/img/in-wave-mockup-5.png" alt="wave-mockup" width="533" height="330">
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
        <!-- section content begin -->
        <div class="uk-section">
            <div class="uk-container">
                <div class="uk-grid" data-uk-grid>
                    <div class="uk-width-3-5@m">
                        <h1 class="uk-margin-small-bottom"><span class="in-highlight">Pengetahuan</span> juga sangat penting!</h1>
                        <p class="uk-text-lead uk-text-muted uk-margin-remove-top">Mari kita edukasi diri kita bersama, belajar dan berbagi pengetahuan untuk lebih memahami dan mendalami bisnis ini, jangan ragu untuk menghubungi karena kami siap membantu.</p>
                        <span class="uk-label uk-text-small uk-text-uppercase uk-border-pill">Belajar</span>
                        <i class="fas fa-arrows-alt-h fa-sm uk-margin-small-left uk-margin-small-right"></i>
                        <span class="uk-label uk-text-small uk-text-uppercase uk-border-pill">Pahami</span>
                        <i class="fas fa-arrows-alt-h fa-sm uk-margin-small-left uk-margin-small-right"></i>
                        <span class="uk-label uk-text-small uk-text-uppercase uk-border-pill">Trading</span>
                    </div>
                    <div class="uk-width-2-5@m">
                        <div class="uk-card uk-card-default uk-card-body uk-border-rounded">
                            <div class="uk-grid uk-grid-small">
                                <div class="uk-width-expand@m">
                                    <h3 class="uk-margin-remove-bottom">Pelajari lebih lengkap</h3>
                                    <p class="uk-margin-small-top">Silahkan daftar dan buka akun, kemudian mulai mencoba dengan akun demo.</p>
                                    <a class="uk-button uk-button-primary uk-border-rounded" href="https://account.<?php echo $weltrade ?>/auth/registration/?r1=ids&r2=landing_<?php echo $referral ?>">Buka Akun</a>
                                </div>
                                <div class="uk-width-auto@m uk-visible@m">
                                    <div class="in-icon-wrapper transparent uk-margin-top">
                                        <i class="fas fa-user-graduate fa-5x"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->

        <!-- section content begin -->
        <div class="uk-section">
            <div class="uk-container">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div class="uk-card uk-card-primary uk-box-shadow-small uk-border-rounded uk-background-cover" style="background-image: url(<?php echo url('/') ?>/template/landing/img/in-card-background-1.jpg);">
                            <div class="uk-card-body">
                                <h2 class="uk-margin-remove-bottom">Gratis Akun Demo</h2>
                                <p class="uk-margin-small-top">Praktekan strategi Anda di akun demo yang bebas resiko sebelum menggunakan akun real.</p>
                                <a class="uk-button uk-button-primary uk-border-rounded" href="https://account.<?php echo $weltrade ?>/auth/registration/?r1=ids&r2=landing_<?php echo $referral ?>">Buka Akun</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section content end -->
    </main>
    <footer>
        <div class="in-price-list">
                <div class="uk-container">
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <div data-uk-slider="autoplay: true; autoplay-interval: 5000">
                                <ul class="uk-slider-items uk-child-width-1-3@s uk-child-width-1-5@m uk-text-small uk-text-center" data-uk-grid>
                                    <li>
                                        XAUUSD <span class="uk-label uk-label-success uk-border-pill uk-margin-small-left"> 0.8 pips</span>
                                    </li>
                                    <li>
                                        EURUSD <span class="uk-label uk-label-success uk-border-pill uk-margin-small-left"> 0.3 pips</span>
                                    </li>
                                    <li>
                                        GBPUSD <span class="uk-label uk-label-success uk-border-pill uk-margin-small-left"> 0.5 pips</span>
                                    </li>
                                    <li>
                                        USDJPY <span class="uk-label uk-label-success uk-border-pill uk-margin-small-left"> 0.7 pips</span>
                                    </li>
                                    <li>
                                        USDCHF <span class="uk-label uk-label-success uk-border-pill uk-margin-small-left"> 0.8 pips</span>
                                    </li>
                                    <li>
                                        EURGBP <span class="uk-label uk-label-success uk-border-pill uk-margin-small-left"> 0.9 pips</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- footer content begin -->
        <div class="uk-section uk-section-muted uk-padding-large uk-padding-remove-horizontal uk-margin-medium-top" style="padding-top: 0px;">
            <div class="uk-container">
                <div class="uk-grid-medium" data-uk-grid>
                    <div class="uk-width-1-1 uk-margin-large-top in-offset-bottom-20">
                        <h6><i class="fas fa-building fa-lg uk-text-danger uk-margin-small-right"></i>Kantor pusat</h6>
                        <p class="uk-text-small">Layanan di Situs Web disediakan dan diatur oleh Systemgates Ltd, sebuah perusahaan yang didirikan di Saint Vincent dan Grenadines dengan nomor perusahaan 24513 IBC 2018 dengan alamat terdaftar di unit Suite 305, Griffith Corporate Center, PO BOX 1510, Beachmont, Kingstown, Saint Vincent dan Grenadines. Perusahaan diatur oleh hukum Saint Vincent dan Grenadines. Perusahaan Rekanan Sofiante LP dengan nomor reg. perusahaan LP1881 menyediakan konten dan menjalankan bisnis, dengan alamat pendaftarannya di Office 29, Clifton House, Fitzwilliam Street Lower, Dublin 2, Dublin, Republik Irlandia.
                            <br>Not for residents of the USA, Canada and Russia.</a></p>
                        <hr>
                        <div class="uk-grid uk-flex uk-flex-middle" data-uk-grid>
                            <div class="uk-width-1-2@m">
                                <div class="uk-grid-small uk-flex uk-child-width-1-4@s uk-flex uk-child-width-1-5@m in-payment-method uk-text-center" data-uk-grid>
                                    <div>
                                        <div class="uk-card uk-card-default uk-card-small uk-card-body">
                                            <img src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-wave-visa.svg" alt="wave-payment" width="59" height="22" data-uk-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="uk-card uk-card-default uk-card-small uk-card-body">
                                            <img src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-wave-mastercard.svg" alt="wave-payment" width="59" height="22" data-uk-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="uk-card uk-card-default uk-card-small uk-card-body">
                                            <img src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-wave-skrill.svg" alt="wave-payment" width="59" height="22" data-uk-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="uk-card uk-card-default uk-card-small uk-card-body uk-visible@m">
                                            <img src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-wave-neteller.svg" alt="wave-payment" width="59" height="22" data-uk-img>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="uk-card uk-card-default uk-card-small uk-card-body" style="padding: 20px 0px;">
                                            <img src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-wave-paypal.svg" alt="wave-payment" width="59" height="22" data-uk-img>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2@m uk-text-right@m">
                                <div class="in-footer-socials in-margin-bottom-40@s">
                                    <a href="https://www.youtube.com/channel/UC0j8b6Qoa9rxoDVfqBPbsiQ"><i class="fab fa-youtube"></i></a>
                                    <a href="https://facebook.com/brokerweltradeID/"><i class="fab fa-facebook-square"></i></a>
                                    <a href="https://www.instagram.com/weltrade.id"><i class="fab fa-instagram"></i></a>
                                    <a href="https://twitter.com/weltrade_idn"><i class="fab fa-twitter"></i></a>
                                    <a href="https://www.linkedin.com/in/weltrade-idn/"><i class="fab fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-section uk-section-secondary uk-padding-remove-vertical">
            <div class="uk-container">
                <div class="uk-grid">
                    <div class="uk-width-3-4@m uk-visible@m">
                        <ul class="uk-subnav uk-subnav-divider">
                            <li><a href="https://<?php echo $weltrade ?>/legal/#client-agreement">Ketentuan Penggunaan</a></li>
                            <li><a href="https://<?php echo $weltrade ?>/legal/#privacy-policy">Kebijakan Pribadi</a></li>
                            <li><a href="https://<?php echo $weltrade ?>/legal/#risk-disclosure">Pengungkapan Resiko</a></li>
                            <li><a href="https://<?php echo $weltrade ?>/legal/#aml-policy">Kebijakan AML</a></li>
                        </ul>
                    </div>
                    <div class="uk-width-expand@m uk-text-right@m">
                        <p>© 2006-<?php echo date('Y') ?> Systemgates Ltd. <br>Hak cipta dilindungi.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer content end -->
        <!-- module totop begin -->
        <div class="uk-visible@m">
            <a href="#" class="in-totop fas fa-chevron-up" data-uk-scroll></a>
        </div>
        <!-- Start of LiveChat (www.livechatinc.com) code -->
<script>
    window.__lc = window.__lc || {};
    window.__lc.license = 13446936;
    ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
</script>
<noscript><a href="https://www.livechatinc.com/chat-with/13446936/" rel="nofollow">Chat with us</a>, powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
<!-- End of LiveChat code -->

        <!-- module totop begin -->
    </footer>
    <!-- Javascript -->
    <script src="<?php echo url('/') ?>/template/landing/js/vendors/uikit.min.js"></script>
    <script src="<?php echo url('/') ?>/template/landing/js/vendors/indonez.min.js"></script>
    <script src="<?php echo url('/') ?>/template/landing/js/config-theme.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $.ajax({
                url: 'https://<?php echo $weltrade ?>/?r1=ids&r2=landing_<?php echo $referral ?>',
                type: 'GET',
            })
            .done(function() {
                console.log("Success accessing <?php echo $referral ?> ");
            })
            .fail(function(e) {
                console.log(e);
            })
            .always(function() {
                console.log("complete");
            });
            
        });
    </script>
</body>

</html>