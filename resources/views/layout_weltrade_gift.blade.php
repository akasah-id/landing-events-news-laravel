<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Standard Meta -->
    <meta charset="utf-8">
    <meta name="theme-color" content="#487EAC" />
    <meta name="robots" content="index, follow" />
    <meta name="keywords" content="weltrade, trading, weltrade indonesia, bisnis online, usaha alternatif, trading online" />
    <meta name="description" content="@yield('title') 15 tahun berpengalaman dan konsisten melayani trader berkelas didunia! Komitmen kami tetap selalu berada disisi sahabat trader Indonesia tak pernah pudar dan akan selalu terjaga! Rayakan 15 Tahun Weltrade, Komisi IB Tertinggi, Kontes Demo $150.000, Kontes IB $50.000, Trade2Win $10.000," />
    <!-- Site Properties -->
    <title>@yield('title')</title>
    <meta name="author" content="WELTRADE">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="https://<?php echo $weltrade ?>/images/wt_preview.png">
    <meta property="og:locale" content="id-ID">
    <meta property="og:title" content="@yield('title')">
    <meta property="og:description" content="@yield('title') 15 tahun berpengalaman dan konsisten melayani trader berkelas didunia! Komitmen kami tetap selalu berada disisi sahabat trader Indonesia tak pernah pudar dan akan selalu terjaga! Rayakan 15 Tahun Weltrade, Komisi IB Tertinggi, Kontes Demo $150.000, Kontes IB $50.000, Trade2Win $10.000,">
    <meta property="og:url" content="https://<?php echo $weltrade ?>/">
    <meta itemprop="image" content="https://<?php echo $weltrade ?>/images/wt180x180.png">
    <meta property="twitter:image:src" content="https://<?php echo $weltrade ?>/images/wt180x180.png">
    <link rel="author" href="https://www.facebook.com/weltradecom/" />
    <link rel="publisher" href="https://www.facebook.com/weltradecom/" />
    <link rel="canonical" href="https://<?php echo $weltrade ?>/" />

    <!--== Favicon ==-->
    <link rel="shortcut icon" href="https://<?php echo $weltrade ?>/images/wt180x180.png" type="image/x-icon">

    <!--== Google Fonts ==-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;500;700&display=swap" rel="stylesheet">

    <!--== Bootstrap CSS ==-->
    <link href="<?php echo url('/') ?>/template/loyalty/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <!--== Icofont CSS ==-->
    <link href="<?php echo url('/') ?>/template/loyalty/assets/css/icofont.css" rel="stylesheet"/>
    <!--== Swiper CSS ==-->
    <link href="<?php echo url('/') ?>/template/loyalty/assets/css/swiper.min.css" rel="stylesheet"/>
    <!--== Main Style CSS ==-->
    <link href="<?php echo url('/') ?>/template/loyalty/assets/css/style.css" rel="stylesheet" />

    <!-- Modal CSS -->
    <link href="<?php echo url('/') ?>/template/loyalty/assets/css/modal.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    
    <!--=== Modernizr Min Js ===-->
    <script src="<?php echo url('/') ?>/template/loyalty/assets/js/modernizr.js"></script>
    <!--=== jQuery Min Js ===-->
    <script src="<?php echo url('/') ?>/template/loyalty/assets/js/jquery-main.js"></script>
</head>

<body>

<!--wrapper start-->
<div class="wrapper page-projects-wrapper">

  <!--== Start Header Wrapper ==-->
  <header class="header-wrapper">
    <div class="header-area header-default header-transparent sticky-header">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-4 col-sm-6 col-lg-2">
            <div class="header-logo-area">
              <a href="#">
                <img class="logo-main" src="<?php echo url('/') ?>/template/loyalty/assets/img/in-logo.svg" alt="Logo" width="161" height="48" />
                <img class="logo-light" src="<?php echo url('/') ?>/template/loyalty/assets/img/in-logo-white.svg" alt="Logo" width="161" height="48" />
              </a>
            </div>
          </div>
          <div class="col-lg-5 d-none d-lg-block">
            <div class="header-navigation-area">
              <ul class="main-menu nav position-relative">
                <li><a href="https://<?php echo $weltrade ?>/promotions/demo-contest/?r1=ids&r2=<?php echo $referral ?>_democontest">KONTES DEMO "STOCK BATTLE" $100.000</a></li>
              </ul>
            </div>
          </div>
          <div class="col-8 col-sm-6 col-lg-5">
            <div class="header-action-area">
              <div class="header-lang-dropdown">
                <a href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802" target="_blank"><button type="button" class="btn-lang">WhatsApp Kami</button></a>
              </div>
              <div class="header-action-btn">
                <a class="btn-theme" href="https://account.<?php echo $weltrade ?>/auth/registration/?r1=ids&r2=<?php echo $referral ?>" target="_blank"><span>BUKA AKUN</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!--== End Header Wrapper ==-->
  
  <main class="main-content">
    <!--== Start Page Title Area ==-->
    <section class="page-title-area bg-img" data-bg-img="<?php echo url('/') ?>/template/loyalty/assets/img/photos/bg2.jpg">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-12 m-auto">
            <div class="page-title-content text-center">
              <h2 class="title">Hadiah Keren</h2>
              <div class="bread-crumbs"><a href="#"> Untuk Trader Indonesia </a><span class="breadcrumb-sep"> // </span><span class="active"> Dari Weltrade</span></div>
            </div>
          </div>
        </div>
      </div>
      <div class="bg-overlay3"></div>
    </section>
    <!--== End Page Title Area ==-->

    @yield('main_section')

    <!--== Start Divider Area Wrapper ==-->
    <section class="divider-area divider-default-area">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-sm-8 col-md-7">
            <div class="content">
              <h2 class="title">Ayo daftar dan dapatkan hadiah keren dari Weltrade!</h2> <p></p>
            </div>
          </div>
          <div class="col-sm-4 col-md-5">
            <div class="divider-btn">
              <a class="btn-theme btn-white" href="https://account.<?php echo $weltrade ?>/auth/registration/?r1=ids&r2=<?php echo $referral ?>" target="_blank">Buka Akun <i class="icofont-rounded-double-right"></i></a>
            </div>
          </div>
        </div>
      </div>
      <div class="shape-group">
        <div class="shape-style4">
          <img src="<?php echo url('/') ?>/template/loyalty/assets/img/shape/4.webp" alt="Image" width="560" height="250">
        </div>
      </div>
    </section>
    <!--== End Divider Area Wrapper ==-->
  </main>

  <!--== Start Footer Area Wrapper ==-->
  <footer class="footer-area default-style">
    <div class="footer-main">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4 col-xl-3">
            <div class="widget-item">
              <h4 class="widget-title">Promo Terbaru</h4>
              <h4 class="widget-title widget-collapsed-title collapsed" data-bs-toggle="collapse" data-bs-target="#dividerId-1">Promo Terbaru</h4>
              <div id="dividerId-1" class="collapse widget-collapse-body">
                <div class="widget-blog-wrap">
                  <div class="blog-post-item">
                    <div class="content">
                      <h4 class="title"><i class="icon icofont-minus"></i> <a href="https://<?php echo $weltrade ?>/promotions/demo-contest/?r1=ids&r2=<?php echo $referral ?>_democontest" target="_blank">Kontes Demo Stock Battle, Menangkan total hadiah $100.000!</a></h4>
                      <div class="meta-date"><a href="#"><i class="icofont-calendar"></i> 3 Jan 2022 - 11 Mar 2022</a></div>
                    </div>
                  </div>
                  <div class="blog-post-item">
                    <div class="content">
                      <h4 class="title"><i class="icon icofont-minus"></i> <a href="https://<?php echo $weltrade ?>/promotions/demo-contest/?r1=ids&r2=<?php echo $referral ?>_democontest" target="_blank">Gandakan Keuntungan Dengan Bonus Deposit 100%</a></h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 offset-lg-0 col-xl-3 offset-xl-1">
            <div class="widget-item">
              <h4 class="widget-title">Trading</h4>
              <h4 class="widget-title widget-collapsed-title collapsed" data-bs-toggle="collapse" data-bs-target="#dividerId-2">Trading</h4>
              <div id="dividerId-2" class="collapse widget-collapse-body">
                <nav class="widget-menu-wrap">
                  <ul class="nav-menu nav">
                    <li><a href="https://<?php echo $weltrade ?>/trading/type_account/?r1=ids&r2=<?php echo $referral ?>_type_account"><i class="icofont-minus"></i>Tipe Akun</a></li>
                    <li><a href="https://<?php echo $weltrade ?>/trading/tools/?r1=ids&r2=<?php echo $referral ?>_tools"><i class="icofont-minus"></i>Instrumen Trading</a></li>
                    <li><a href="https://<?php echo $weltrade ?>/trading/paysystems/?r1=ids&r2=<?php echo $referral ?>_paysystems"><i class="icofont-minus"></i>Deposit dan Withdraw</a></li>
                    <li><a href="https://<?php echo $weltrade ?>/tools/stocks/?r1=ids&r2=<?php echo $referral ?>_stocks"><i class="icofont-minus"></i>Saham CFDs</a></li>
                    <li><a href="https://digital-trading.live/id"><i class="icofont-minus"></i>Trading Digital</a></li>
                    <li><a href="https://<?php echo $weltrade ?>/tools/calendar/?r1=ids&r2=<?php echo $referral ?>_calendar"><i class="icofont-minus"></i>Kalender Ekonomi</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 col-xl-3">
            <div class="widget-item ml-40 ml-lg-20 md-ml-0">
              <h4 class="widget-title">Hal Hal Penting</h4>
              <h4 class="widget-title widget-collapsed-title collapsed" data-bs-toggle="collapse" data-bs-target="#dividerId-3">Hal Hal Penting</h4>
              <div id="dividerId-3" class="collapse widget-collapse-body">
                <nav class="widget-menu-wrap">
                  <ul class="nav-menu nav">
                    <li><a href="https://<?php echo $weltrade ?>/company/?r1=ids&r2=<?php echo $referral ?>_company"><i class="icofont-minus"></i>Tentang Weltrade</a></li>
                    <li><a href="https://<?php echo $weltrade ?>/legal/?r1=ids&r2=<?php echo $referral ?>_legal"><i class="icofont-minus"></i>Syarat dan Ketentuan</a></li>
                    <li><a href="https://<?php echo $weltrade ?>/contact/?r1=ids&r2=<?php echo $referral ?>_contact"><i class="icofont-minus"></i>Layanan Pelanggan</a></li>
                    <li><a href="https://<?php echo $weltrade ?>/partners/?r1=ids&r2=<?php echo $referral ?>_partners"><i class="icofont-minus"></i>Mitra (IB)</a></li>
                    <li><a href="https://weltrade-idn.net/gg_education"><i class="icofont-minus"></i>Belajar dan Edukasi</a></li>
                    <li><a href="https://weltrade-idn.net/gg_services"><i class="icofont-minus"></i>Kondisi Trading</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-2 col-xl-2">
            <div class="widget-item ml-35 lg-ml-0">
              <h4 class="widget-title">Follow Us</h4>
              <h4 class="widget-title widget-collapsed-title collapsed" data-bs-toggle="collapse" data-bs-target="#dividerId-4">Follow Us</h4>
              <div id="dividerId-4" class="collapse widget-collapse-body">
                <nav class="widget-menu-wrap">
                  <ul class="nav-menu nav">
                    <li><a href="https://web.facebook.com/brokerweltradeID"><i class="icofont-minus"></i>Facebook</a></li>
                    <li><a href="https://www.instagram.com/weltrade.id/"><i class="icofont-minus"></i>Instragram</a></li>
                    <li><a href="https://www.youtube.com/channel/UC0j8b6Qoa9rxoDVfqBPbsiQ"><i class="icofont-minus"></i>Youtube</a></li>
                    <li><a href="https://twitter.com/weltrade_idn"><i class="icofont-minus"></i>Twitter</a></li>
                    <li><a href="https://www.linkedin.com/in/weltrade-idn/"><i class="icofont-minus"></i>LinkedIn</a></li>
                    <li><a href="https://www.tiktok.com/@weltrade.idn"><i class="icofont-minus"></i>TikTok</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-shape bg-img" data-bg-img="<?php echo url('/') ?>/template/loyalty/assets/img/photos/footer1.webp"></div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="footer-bottom-content">
          <div class="row">
            <div class="col-md-12">
              <div class="widget-copyright">
                <p>© 2006-2021 Systemgates Ltd</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--== End Footer Area Wrapper ==-->
  
  <!--== Scroll Top Button ==-->
  <div class="scroll-to-top"><span class="icofont-rounded-double-left icofont-rotate-90"></span></div>
</div>
<!-- Start of LiveChat (www.livechatinc.com) code -->
<script>
    window.__lc = window.__lc || {};
    window.__lc.license = 13446936;
    ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
</script>
<noscript><a href="https://www.livechatinc.com/chat-with/13446936/" rel="nofollow">Chat with us</a>, powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
<!-- End of LiveChat code -->


<!--=======================Javascript============================-->

<!--=== jQuery Migration Min Js ===-->
<script src="<?php echo url('/') ?>/template/loyalty/assets/js/jquery-migrate.js"></script>
<!--=== Popper Min Js ===-->
<script src="<?php echo url('/') ?>/template/loyalty/assets/js/popper.min.js"></script>
<!--=== Bootstrap Min Js ===-->
<script src="<?php echo url('/') ?>/template/loyalty/assets/js/bootstrap.min.js"></script>
<!--=== jquery Swiper Min Js ===-->
<script src="<?php echo url('/') ?>/template/loyalty/assets/js/swiper.min.js"></script>
<!--=== jquery Countdown Js ===-->
<script src="<?php echo url('/') ?>/template/loyalty/assets/js/jquery.countdown.min.js"></script>
<!--=== Isotope Min Js ===-->
<script src="<?php echo url('/') ?>/template/loyalty/assets/js/isotope.pkgd.min.js"></script>

<!--=== Custom Js ===-->
<script src="<?php echo url('/') ?>/template/loyalty/assets/js/custom.js"></script>

</body>

</html>