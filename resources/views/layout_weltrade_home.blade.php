<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="index, follow" />
        <meta name="keywords" content="weltrade, trading, weltrade indonesia, bisnis online, usaha alternatif, trading online" />
        <meta name="description" content="@yield('metadesc')" />
        <!-- Site Properties -->
        <title>@yield('title')</title>
        <meta name="author" content="WELTRADE">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="@yield('ogimage')">
        <meta property="og:locale" content="id-ID">
        <meta property="og:title" content="@yield('title')">
        <meta property="og:description" content="@yield('metadesc')">
        <meta property="og:url" content="<?php echo url()->full() ?>">
        <meta itemprop="image" content="https://<?php echo $weltrade ?>/images/wt180x180.png">
        <meta property="twitter:image:src" content="https://<?php echo $weltrade ?>/images/wt180x180.png">
        <link rel="author" href="https://www.facebook.com/brokerweltradeID/" />
        <link rel="publisher" href="https://www.facebook.com/brokerweltradeID/" />
        <link rel="canonical" href="https://<?php echo $weltrade ?>/<?php echo $ref ?>" />
        <link rel="shortcut icon" href="https://<?php echo $weltrade ?>/images/wt180x180.png" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="https://<?php echo $weltrade ?>/images/wt180x180.png">

        <!--webfonts-->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&amp;display=swap">

        <!--vendors css-->
        <link rel="stylesheet" href="<?php echo url('/') ?>/template/home/vendors/bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="<?php echo url('/') ?>/template/home/vendors/owl-carousel/assets/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="<?php echo url('/') ?>/template/home/vendors/owl-carousel/assets/owl.theme.default.min.css" type="text/css">
        <link rel="stylesheet" href="<?php echo url('/') ?>/template/home/vendors/fancybox/jquery.fancybox.min.css" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

        <!--main css-->
        <link rel="stylesheet" href="<?php echo url('/') ?>/template/home/css/style.css" type="text/css">
    </head>

    <body>

        <!--header-->
        <header class="header">
            <!--top header-->
            <div class="top-header">
                <div class="container">
                    <!--menu left-->
                    <ul class="group-menu">
                        <li>
                            <a target="_blank" href="https://<?php echo $weltrade ?>/webterminal/<?php echo $ref ?>">
                                <i class="fas fa-cloud"></i>
                                <span>MT4/MT5 Web</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=#">
                                <i class="fas fa-phone-alt"></i>
                                <span>Hubungi Kami</span>
                            </a>
                        </li>
                    </ul>
                    <!--/menu left-->

                    <!--group button-->
                    <div class="group-button">
                        <a class="btn btn-sm btn-light" href="https://account.<?php echo $weltrade ?><?php echo "/$ref" ?>">
                            <i class="fas fa-sign-in-alt"></i>
                            <span>Masuk</span>
                        </a>
                        <a class="btn btn-sm btn-success" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo $ref ?>">
                            <i class="fas fa-user-plus"></i>
                            <span>Daftar</span>
                        </a>
                        <div class="dropdown-btn-outer right-dropdown">
                            <a class="btn btn-sm btn-light btn-lng right-dropdown" href="#">
                                <img src="<?php echo url('/') ?>/template/home/images/flag-ind.jpg" alt="">
                                <i class="fas fa-angle-down"></i>
                            </a>
                            <ul>
                                <li>
                                    <a href="https://<?php echo $weltrade ?><?php echo "/$ref" ?>" target="_blank">
                                        <img src="<?php echo url('/') ?>/template/home/images/flag-ind.jpg" alt="">
                                        <span>Website Utama</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo url('/') ?>">
                                        <img src="<?php echo url('/') ?>/template/home/images/flag-ind.jpg" alt="">
                                        <span>Indonesia</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--/group button-->
                </div>
            </div>
            <!--/top header-->

            <!--main header-->
            <div class="main-header">
                <div class="container">
                    <!--logo header-->
                    <a class="logo-header" href="<?php echo url('/') ?>">
                        <img src="<?php echo url('/') ?>/template/home/images/logo-weltrade.jpg" alt="">
                    </a>
                    <!--/logo header-->

                    <!--main nav-->
                    <nav class="main-nav">
                        <div class="btn-nav-mobile">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <ul>
                            <li class="header-menu-mobile">
                                <div class="group-btn">
                                    <a class="btn btn-light btn-sm" href="https://account.<?php echo $weltrade ?><?php echo "/$ref" ?>">
                                        <i class="fas fa-sign-in-alt"></i>
                                        <span>Masuk</span>
                                    </a>
                                    <a class="btn btn-success btn-sm" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo $ref ?>">
                                        <i class="fas fa-user-plus"></i>
                                        <span>Daftar</span>
                                    </a>
                                    <div class="dropdown-btn-outer right-dropdown">
                                        <a class="btn btn-sm btn-light btn-lng right-dropdown" href="#">
                                            <img src="<?php echo url('/') ?>/template/home/images/flag-ind.jpg" alt="">
                                            <i class="fas fa-angle-down"></i>
                                        </a>
                                        <ul>
                                            <li>
                                                <a href="https://<?php echo $weltrade ?><?php echo "/$ref" ?>" target="_blank">
                                                    <img src="<?php echo url('/') ?>/template/home/images/flag-ind.jpg" alt="">
                                                    <span>Website Utama</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo url('/') ?>">
                                                    <img src="<?php echo url('/') ?>/template/home/images/flag-ind.jpg" alt="">
                                                    <span>Indonesia</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div><a class="button-close-nav" href="#">
                                    <span></span>
                                    <span></span>
                                </a>
                            </li>
                            <li class="megamenu">
                                <a href="#">
                                    <span>Trading</span>
                                    <i class="fas fa-angle-down"></i>
                                </a>
                                <div class="megamenu-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-4 side-promo">
                                                <div class="side-promo-inner">
                                                    <img src="<?php echo url('/') ?>/template/home/images/mega-trading.png" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-8 side-menu">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="group-menu">
                                                            <h4>Trading</h4>
                                                            <ul>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/trading/type_account/<?php echo "$ref" ?>">Akun Micro (Cent)</a></li>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/trading/type_account/<?php echo "$ref" ?>">Akun Premium</a></li>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/trading/type_account/<?php echo "$ref" ?>">Akun Pro (ECN)</a></li>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/trading/type_account/<?php echo "$ref" ?>">Akun Kripto</a></li>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/trading/type_account/<?php echo "$ref" ?>">Akun Demo</a></li>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/trading/platform/<?php echo "$ref" ?>">MT4 | iOS | Android</a></li>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/trading/platform/<?php echo "$ref" ?>">MT5 | iOS | Android</a></li>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/trading/platform/<?php echo "$ref" ?>">MetaTrader 4/5 Web</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="group-menu">
                                                            <h4>Instrumen</h4>
                                                            <ul>
                                                                <li><a href="https://<?php echo $weltrade ?>/trading/tools/<?php echo "$ref" ?>" target="_blank">Forex dan Emas</a></li>
                                                                <li><a href="https://<?php echo $weltrade ?>/trading/tools/<?php echo "$ref" ?>" target="_blank">Komoditas</a></li>
                                                                <li><a href="https://<?php echo $weltrade ?>/trading/tools/<?php echo "$ref" ?>" target="_blank">Saham CFD</a></li>
                                                                <li><a href="https://<?php echo $weltrade ?>/trading/tools/<?php echo "$ref" ?>" target="_blank">Kripto</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="group-menu" style="padding-top: 0px !important">
                                                            <h4>Keuangan</h4>
                                                            <ul>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/trading/paysystems/<?php echo "$ref" ?>">Deposit Dana</a></li>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/trading/paysystems/<?php echo "$ref" ?>">Withdraw Dana</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="group-menu">
                                                            <h4>Promosi</h4>
                                                            <ul>
                                                                <!-- <li><a href="<?php echo url('/loyalty') ?>">Loyalty Program</a></li> -->
                                                                <!-- <li><a target="_blank" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Bonus Deposit 200% $10.000</a></li> -->
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/demo-contest/<?php echo "$ref" ?>">Kontes Demo WELTRADE Terminal</a></li>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/partners/cpa/<?php echo "$ref" ?>">Reward IB $50</a></li>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/trading/paysystems/<?php echo "$ref" ?>">Fix Rate $1 = Rp 9.000</a></li>
                                                                <!-- <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/demo-contest/<?php echo "$ref" ?>">Kontes Demo STOCK BATTLE</a></li> -->
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/iphone/<?php echo "$ref" ?>">iPhone 13 Pro Max</a></li>
                                                                <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/trade2win/<?php echo "$ref" ?>">Undian Trade2Win</a></li>
                                                                <!-- <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/bonus-100/<?php echo "$ref" ?>">Bonus Deposit 100%</a></li> -->
                                                                <!-- <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/ib_contest/<?php echo "$ref" ?>">Kontes Partner</a></li> -->
                                                                <!-- <li><a target="_blank" href="https://weltrade-idn.net/free_gift">Gratis Kaos dan Topi</a></li> -->
                                                                <li><a href="<?php echo url('/webinar') ?>">Gratis Webinar Edukasi</a></li>
                                                                <li><a href="<?php echo url('/webinar') ?>">Konsultasi Super VIP</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="megamenu">
                                <a href="#">
                                    <span>Edukasi</span>
                                    <i class="fas fa-angle-down"></i>
                                </a>
                                <div class="megamenu-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-3 side-promo">
                                                <div class="side-promo-inner">
                                                    <img src="<?php echo url('/') ?>/template/home/images/mega-edukasi1.png" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 side-menu">
                                                <div class="group-menu">
                                                    <h4>Analisa Market</h4>
                                                    <ul>
                                                        <li><a href="<?php echo url('/prakiraan-mingguan') ?>">Prakiraan Pasar Mingguan</a></li>
                                                        <li><a href="<?php echo url('/analisa-harian') ?>">Artikel dan Berita Terbaru</a></li>
                                                        <li><a href="<?php echo url('/sinyal-trading') ?>">Sinyal Trading Harian</a></li>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/tools/calendar/<?php echo "$ref" ?>">Kalender Ekonomi Pasar</a></li>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/tools/calculator/<?php echo "$ref" ?>">Kalkulator Trading</a></li>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/tools/metatrader-guide/<?php echo "$ref" ?>">Panduan Platform MT4/MT5</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 side-promo">
                                                <div class="side-promo-inner">
                                                    <img src="<?php echo url('/') ?>/template/home/images/mega-edukasi2.png" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 side-menu">
                                                <div class="group-menu">
                                                    <h4>Pendidikan Trading</h4>
                                                    <ul>
                                                        <li><a href="<?php echo url('/webinar') ?>">Jadwal Mingguan Webinar </a></li>
                                                        <li><a href="#">Video Webinar On-Demand</a></li>
                                                        <li><a href="#">Bincang Bincang Trader</a></li>
                                                        <li><a href="#">Video Kursus Trading</a></li>
                                                        <li><a href="#">Tips dan Strategi Trading</a></li>
                                                        <li><a href="#">Pembelajaran Dasar Trading</a></li>
                                                        <!-- <li><a href="#">Jadwal Seminar Indonesia</a></li> -->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="megamenu">
                                <a href="#">
                                    <span>Mitra</span>
                                    <i class="fas fa-angle-down"></i>
                                </a>
                                <div class="megamenu-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-5 side-promo">
                                                <div class="side-promo-inner">
                                                    <img src="<?php echo url('/') ?>/template/home/images/mega-partner1.png" alt="" style="width: 550px !important;">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 side-menu">
                                                <div class="group-menu">
                                                    <h4>Promosi IB</h4>
                                                    <ul>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/partners/cpa/<?php echo "$ref" ?>">Program Reward IB</a></li>
                                                        <!-- <li><a href="#">Kopdar Mingguan IB</a></li>
                                                        <li><a href="#">Kontes Partner Dunia</a></li>
                                                        <li><a href="#">WELTRADE IB Summit</a></li> -->
                                                    </ul>
                                                </div>
                                                <div class="group-menu" style="padding-top: 0px !important">
                                                    <h4>Menjadi IB</h4>
                                                    <ul>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/partners<?php echo "/$ref" ?>">Introduction Broker (IB)</a></li>
                                                        <li><a target="_blank" href="https://account.<?php echo $weltrade ?>/partner-program/info<?php echo "/$ref" ?>">Daftar Jadi Mitra atau IB </a></li>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/partners/conditions/<?php echo "$ref" ?>">Pembayaran Komisi per Jam</a></li>
                                                        <li><a href="#">Partner Terpercaya WELTRADE</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 side-promo">
                                                <div class="side-promo-inner">
                                                    <img src="<?php echo url('/') ?>/template/home/images/mega-partner2.png" alt="">
                                                </div>
                                            </div>
	                                    </div>
                                    </div>
                                </div>
                            </li>
                            <li class="megamenu">
                                <a href="#">
                                    <span>Promosi</span>
                                    <i class="fas fa-angle-down"></i>
                                </a>    
                                <div class="megamenu-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-3 side-promo">
                                                <div class="side-promo-inner">
                                                    <img src="<?php echo url('/') ?>/file/images/static/mega-promo26042022.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 side-menu">
                                                <div class="group-menu">
                                                    <h4>Promosi dan Kontes</h4>
                                                    <ul>
                                                        <!-- <li><a href="<?php echo url('/loyalty') ?>">Loyalty Program</a></li> -->
                                                        <!-- <li><a target="_blank" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Bonus Deposit 200% $10.000</a></li> -->
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/demo-contest/<?php echo "$ref" ?>">Kontes Demo WELTRADE Terminal</a></li>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/partners/cpa/<?php echo "$ref" ?>">Program Reward IB</a></li>
                                                    	<!-- <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/demo-contest/<?php echo "$ref" ?>">Kontes Stock Battle $100.000</a></li> -->
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/trade2win/<?php echo "$ref" ?>">Undian Trade2 Win $10.000</a></li>
                                                    	<!-- <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/demo-contest/<?php echo "$ref" ?>">Kontes Trading Akun Demo</a></li> -->
                                                        <!-- <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/ib_contest/<?php echo "$ref" ?>">Kontes Partner Dunia</a></li> -->
                                                        <li><a href="<?php echo url('/webinar') ?>">Konsultasi Super VIP</a></li>
                                                        <li><a href="#">MetaTrader MultiTerminal</a></li>
                                                        <!-- <li><a href="#">Trading Cashback 50%</a></li> -->
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 side-promo">
                                                <div class="side-promo-inner">
                                                    <img src="<?php echo url('/') ?>/template/home/images/mega-promo2.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 side-menu">
                                                <div class="group-menu">
                                                    <h4>Promosi dan Bonus</h4>
                                                    <ul>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/trading/paysystems/<?php echo "$ref" ?>">Fix Rate $1 = Rp 9.000</a></li>
                                                    	<li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/iphone/<?php echo "$ref" ?>">Gratis iPhone 13 Pro Max</a></li>
                                                        <!-- <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/bonus-100/<?php echo "$ref" ?>">Bonus Deposit 100%</a></li> -->
                                                        <li><a target="_blank" href="https://weltrade-idn.net/free_gift">Gratis Kaos dan Topi</a></li>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/spinner/<?php echo "$ref" ?>">Spinner</a></li>
                                                        <!-- <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/cashpuzzle/<?php echo "$ref" ?>">Cash Puzzle</a></li> -->
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/promotions/astrocards/<?php echo "$ref" ?>">Astrocards</a></li>
                                                        <li><a href="<?php echo url('/webinar') ?>">Gratis Konsultasi VIP</a></li>
                                                        <!-- <li><a href="#">Deposit Gift</a></li> -->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="megamenu">
                                <a href="#">
                                    <span>Perusahaan</span>
                                    <i class="fas fa-angle-down"></i>
                                </a>
                                <div class="megamenu-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-3 side-menu">
                                                <div class="group-menu">
                                                    <h4>WELTRADE</h4>
                                                    <ul>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/company/<?php echo "$ref" ?>">Tentang WELTRADE</a></li>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/company/news/<?php echo "$ref" ?>">Berita</a></li>
                                                        <li><a href="#">Keunggulan</a></li>
                                                        <li><a href="#">Sejarah</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 side-menu">
                                                <div class="group-menu">
                                                    <h4>Dukungan</h4>
                                                    <ul>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/legal/contact/<?php echo "$ref" ?>">Layanan Dukungan</a></li>
                                                        <li><a target="_blank" href="https://<?php echo $weltrade ?>/legal/<?php echo "$ref" ?>">Informasi Legal</a></li>
                                                        <li><a href="#">Pertanyaan Umum</a></li>
                                                        <li><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=#">WhatsApp Kami</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 side-menu">
                                                <div class="group-menu">
                                                    <h4>Sosial Media</h4>
                                                    <ul>
                                                        <li><a target="_blank" href="https://facebook.com/brokerweltradeID/">Facebook</a></li>
                                                        <li><a target="_blank" href="https://www.instagram.com/weltrade.id">Instagram</a></li>
                                                        <li><a target="_blank" href="https://www.linkedin.com/in/weltrade-idn/">LinkedIn</a></li>
                                                        <!-- <li><a target="_blank" href="#">Twitter</a></li> -->
                                                        <li><a target="_blank" href="https://www.tiktok.com/@weltrade.idn">TikTok</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 side-menu">
                                                <div class="group-menu">
                                                    <h4>Channel</h4>
                                                    <ul>
                                                        <li><a target="_blank" href="https://www.youtube.com/channel/UC0j8b6Qoa9rxoDVfqBPbsiQ">Youtube</a></li>
                                                        <li><a target="_blank" href="https://web.facebook.com/groups/pasukan.trader.indonesia">Facebook Group</a></li>
                                                        <li><a target="_blank" href="https://t.me/weltradeedukasi">Telegram Edukasi</a></li>
                                                        <li><a target="_blank" href="https://t.me/weltradeidn">Telegram Umum</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="menu-mobile">
                                <a href="https://<?php echo $weltrade ?>/webterminal/<?php echo "$ref" ?>" target="_blank">Web Platform</a>
                            </li>
                            <li class="menu-mobile">
                                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=#">Hubungi Kami</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <!--/header-->

        @yield('main')

        <!--achievement-->
        <section class="achievement">
            <div class="container">
                <h1 class="main-heading lg">Penghargaan Kami</h1>
                <div class="row">
                    <div class="col-6 col-md-4 col-xl-2 mb-5 mb-md-5 thumb-achieve">
                        <div class="thumb-ribbon">
                            <img src="<?php echo url('/') ?>/template/home/images/ribbon.png" alt="">
                            <span>2021</span>
                        </div>
                        <span>Most Trusted In Asia</span>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2 mb-5 mb-md-5 thumb-achieve">
                        <div class="thumb-ribbon">
                            <img src="<?php echo url('/') ?>/template/home/images/ribbon.png" alt="">
                            <span>2019</span>
                        </div>
                        <span>Best Foreign Broker 2019</span>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2 mb-5 mb-md-5 thumb-achieve">
                        <div class="thumb-ribbon">
                            <img src="<?php echo url('/') ?>/template/home/images/ribbon.png" alt="">
                            <span>2017</span>
                        </div>
                        <span>Best Customer Service Award</span>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2 mb-5 thumb-achieve">
                        <div class="thumb-ribbon">
                            <img src="<?php echo url('/') ?>/template/home/images/ribbon.png" alt="">
                            <span>2015</span>
                        </div>
                        <span>Best Affiliate Program</span>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2 thumb-achieve">
                        <div class="thumb-ribbon">
                            <img src="<?php echo url('/') ?>/template/home/images/ribbon.png" alt="">
                            <span>2015</span>
                        </div>
                        <span>Best Trade Execution</span>
                    </div>
                    <div class="col-6 col-md-4 col-xl-2 thumb-achieve">
                        <div class="thumb-ribbon">
                            <img src="<?php echo url('/') ?>/template/home/images/ribbon.png" alt="">
                            <span>2014</span>
                        </div>
                        <span>Business Olimp Awards</span>
                    </div>
                </div>
            </div>
        </section>
        <!--/achievement-->

        <!--footer-->
        <footer class="footer">
            <div class="group-menu-footer">
                <div class="container">
                    <div class="list-menu-footer">
                        <h6>Perusahaan</h6>
                        <ul>
                            <li><a href="#">Tentang Kami</a></li>
                            <li><a href="#">Informasi Legal</a></li>
                            <li><a href="#">Berita</a></li>
                            <li><a href="#">Layanan Dukungan</a></li>
                            <li><a href="#">Mitra</a></li>
                        </ul>
                    </div>
                    <div class="list-menu-footer">
                        <h6>Trading</h6>
                        <ul>
                            <li><a href="#">Tipe Akun</a></li>
                            <li><a href="#">Instrument Trading</a></li>
                            <li><a href="#">Deposit dan Penarikan</a></li>
                            <li><a href="#">Platform</a></li>
                        </ul>
                    </div>
                    <div class="list-menu-footer">
                        <h6>Robot Trading</h6>
                        <ul>
                            <li><a href="#">Sinyal MQL5</a></li>
                            <li><a href="#">Sinyal Zulutrade</a></li>
                            <li><a href="#">Robot dan Indikator</a></li>
                        </ul>
                    </div>
                    <div class="list-menu-footer">
                        <h6>Alat</h6>
                        <ul>
                            <li><a href="#">Saham CFDs</a></li>
                            <li><a href="#">Kalkulator Trading</a></li>
                            <li><a href="#">Kalender Ekonomi</a></li>
                            <li><a href="#">Akun Demo</a></li>
                            <li><a href="#">Pelatihan dan Seminar</a></li>
                            <li><a href="#">Panduan MetaTrader</a></li>
                        </ul>
                    </div>
                    <div class="list-menu-footer">
                        <h6>Promosi</h6>
                        <ul>
                            <li><a href="#">GRATIS iPhone 13 Pro Max</a></li>
                            <li><a href="#">Kontes Demo STOCK BATTLE</a></li>
                            <li><a href="#">WORLD PARTNER CONTEST 2021</a></li>
                            <li><a href="#">Bonus Deposit 100%</a></li>
                            <li><a href="#">TRADE2WIN</a></li>
                            <li><a href="#">ASTROCARDS</a></li>
                            <li><a href="#">SPINNER</a></li>
                            <li><a href="#">CASH PUZZLE</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="main-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 side-left">
                            <h6>Kontak</h6>
                            <ul class="contact-footer">
                                <li>
                                    <i class="fas fa-envelope"></i>
                                	<a style="color: #90A2CD;text-decoration: none;" href="mailto:supportid@weltrade.com">
                                    <span>supportid@weltrade.com</span>
                                    </a>
                                </li>
                                <li>
                                    <i class="fas fa-phone-alt"></i>
                                    <a style="color: #90A2CD;text-decoration: none;" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=#">
                                    <span>#</span>
                                    </a>
                                </li>
                                <li>
                                    <i class="fab fa-facebook-f"></i>
                                    <a style="color: #90A2CD;text-decoration: none;" href="https://facebook.com/brokerweltradeID/">
                                    <span>Weltrade indonesia</span>
                                    </a>
                                </li>
                                <li>
                                    <i class="fab fa-instagram"></i>
                                    <a style="color: #90A2CD;text-decoration: none;" href="https://www.instagram.com/weltrade.id">
                                    <span>Weltrade indonesia</span>
                                    </a>
                                </li>
                                <li>
                                    <i class="fab fa-youtube"></i>
                                    <a style="color: #90A2CD;text-decoration: none;" href="https://www.youtube.com/channel/UC0j8b6Qoa9rxoDVfqBPbsiQ">
                                    <span>Weltrade indonesia</span>
                                    </a>
                                </li>
                                <li>
                                    <i class="fab fa-telegram"></i>
                                    <a style="color: #90A2CD;text-decoration: none;" href="https://t.me/weltradeedukasi">
                                    <span>Telegram Edukasi</span>
                                    </a>
                                </li>
                                <li>
                                    <i class="fab fa-telegram"></i>
                                    <a style="color: #90A2CD;text-decoration: none;" href="https://t.me/weltradeidn">
                                    <span>Telegram Umum</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="list-img">
                                <img src="<?php echo url('/') ?>/template/home/images/visa.png" alt="">
                                <img src="<?php echo url('/') ?>/template/home/images/mastercard.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-9 side-right">
                            <h6>Kantor Pusat</h6>
                            <p>Layanan di Situs Web disediakan dan diatur oleh Systemgates Ltd, sebuah perusahaan yang didirikan di Saint Vincent dan Grenadines dengan nomor perusahaan 24513 IBC 2018 dengan alamat terdaftar di unit Suite 305, Griffith Corporate Center, PO BOX 1510, Beachmont, Kingstown, Saint Vincent dan Grenadines. Perusahaan diatur oleh hukum Saint Vincent dan Grenadines. Perusahaan Rekanan Sofiante LP dengan nomor reg. perusahaan LP1881 menyediakan konten dan menjalankan bisnis, dengan alamat pendaftarannya di Office 29, Clifton House, Fitzwilliam Street Lower, Dublin 2, Dublin, Republik Irlandia.</p>
                            <p>Not for residents of the USA, Canada and Russia.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <div class="container">
                    <span>&copy; 2006-<?php echo date('Y') ?> Systemgates Ltd. Hak cipta dilindungi.</span>
                    <ul>
                        <li><a href="#">Ketentuan Pengguna</a></li>
                        <li><a href="#">Kebijakan Pribadi</a></li>
                        <li><a href="#">Pengungkapan resiko </a></li>
                        <li><a href="#">Kebijakan AML</a></li>
                    </ul>
                </div>
            </div>
        </footer>
        <!--/footer-->

        <!-- Start of LiveChat (www.livechatinc.com) code -->
		<script>
		    window.__lc = window.__lc || {};
		    window.__lc.license = 13446936;
		    ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
		</script>
		<noscript><a href="https://www.livechatinc.com/chat-with/13446936/" rel="nofollow">Chat with us</a>, powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
		<!-- End of LiveChat code -->

    </body>

    <!--vendor js-->
    <script src="<?php echo url('/') ?>/template/home/vendors/jquery/jquery-3.6.0.min.js"></script>
    <script src="<?php echo url('/') ?>/template/home/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo url('/') ?>/template/home/vendors/fancybox/jquery.fancybox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
    <script src="<?php echo url('/') ?>/template/home/vendors/count-up/jquery.countup.min.js"></script>

    <!--main js-->
    <script src="<?php echo url('/') ?>/template/home/js/main.js"></script>

</html>
