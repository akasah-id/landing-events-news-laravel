
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Akasah.id" />
	<link rel="shortcut icon" href="<?php echo url('/') ?>/template/cooming_soon/favicon.png">

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo url('/') ?>/template/cooming_soon/master/css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>On AKASAH.ID | Great Link Shorter</title>
	<script src='https://www.google.com/recaptcha/api.js?render=6LcpeIQUAAAAABRhBgr-UuaVdZql8D2A94JzJKS4'></script>
	<script>
	function reloadGCaptcha() {
		grecaptcha.execute('6LcpeIQUAAAAABRhBgr-UuaVdZql8D2A94JzJKS4', {action: 'link'})
			.then(function(token) {
				var recaptchaResponse = document.getElementById('recaptchaResponse');
			    recaptchaResponse.value = token;
		});
	}
	grecaptcha.ready(function() {
		reloadGCaptcha();
	});
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="transparent-header page-section dark">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="#" class="standard-logo" data-dark-logo="<?php echo url('/') ?>/template/cooming_soon/logo.png"><img src="<?php echo url('/') ?>/template/cooming_soon/logo.png" alt="Akasah Logo"></a>
						<a href="#" class="retina-logo" data-dark-logo="<?php echo url('/') ?>/template/cooming_soon/logo.png"><img src="<?php echo url('/') ?>/template/cooming_soon/logo.png" alt="Akasah Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">

						<ul class="one-page-menu">
							<li class="current"><a href="#" data-href="#header"><div style="FONT-SIZE: 30px;">ON.AKASAH.ID</div></a></li>
						</ul>

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->

		<section id="slider" class="slider-element slider-parallax dark full-screen" style="background: url(<?php echo url('/') ?>/template/cooming_soon/images/landing/landing1.jpg) center;">

			<div class="slider-parallax-inner">

				<div class="container clearfix">

					<div class="vertical-middle">

						<div class="heading-block center nobottomborder">
							<h1 data-animate="fadeInUp">It's your time to <strong>create</strong> your great links for <strong>FREE</strong></h1>
							<span data-animate="fadeInUp" data-delay="300">Building a Great Short Links was never so Easy &amp; Interactive.</span>
						</div>

						<form method="post" role="form" id="form_link">
							@csrf
							<div class="landing-wide-form clearfix" style="padding-bottom: 0px !important;">
								<div>
									<div>
										<input type="text" name="link_url" class="form-control form-control-lg not-dark" value="" placeholder="Your Old Weird Long URL" required>
									</div>
								</div>
							</div>
							<div class="landing-wide-form clearfix">
								<div>
									<div>
										<label>https://on.akasah.id/</label>
										<input type="text" name="link_shortened" class="form-control form-control-lg not-dark" value="" placeholder="Your New Great Short URL" required>
									</div>
								</div>
							</div>
							<div class="landing-wide-form clearfix">
								<div class="col_full col_last nobottommargin">
									<button id="link_button" class="btn btn-lg btn-danger btn-block nomargin" value="submit" type="submit" style="">Give me My Link</button>
								</div>
							</div>

							<input type="hidden" name="recaptcha_response" id="recaptchaResponse">
						</form>

					</div>

					<a href="#" data-scrollto="#section-features" class="one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

				</div>

			</div>

		</section>

		<!-- Modal -->
		<a href="#myModal1" id="link_modal" data-lightbox="inline" class="hidden"></a>
		<div class="modal1 mfp-hide subscribe-widget" id="myModal1">
			<div class="block dark divcenter" style="background: url('<?php echo url('/') ?>/template/cooming_soon/images/modals/modal1.jpg') no-repeat; background-size: cover; max-width: 700px;" data-height-xl="400">
				<div style="padding: 50px;">
					<div class="heading-block nobottomborder bottommargin-sm" style="max-width:500px;">
						<h3>Here's your Link!</h3>
						<span>Don't forget to be happy my friend!</span>
					</div>
					<div class="widget-subscribe-form-result"></div>
					<div class="widget-subscribe-form2" style="max-width: 350px;">
						<input type="text" id="widget-subscribe-form2-email" class="form-control form-control-lg not-dark required email">
						<button id="link_cb" data-clipboard-target="#widget-subscribe-form2-email" class="button button-rounded button-border button-light noleftmargin" type="submit" style="margin-top:15px;">Copy to my clipboard</button>
					</div>
					<br><br><br><span><a href="https://paypal.me/triakasah" target="_blank">Feel generous today? Donate here!</a></span>
				</div>
			</div>
		</div>

		<a href="#myModal2" id="link_modal_failed" data-lightbox="inline" class="hidden"></a>
		<div class="modal1 mfp-hide" id="myModal2">
			<div class="block divcenter" style="background-color: #FFF; max-width: 500px;">
				<div class="center" style="padding: 50px;">
					<h3>So sorry my friend!</h3>
					<p class="nobottommargin">Somebody else already have this link!</p>
				</div>
				<div class="section center nomargin" style="padding: 30px;">
					<a href="#" class="button" onClick="$.magnificPopup.close();return false;">Try a new one!</a>
				</div>
			</div>
		</div>


		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						Copyrights &copy; 2018 All Rights Reserved by Akasah.id<br>
					</div>

					<div class="col_half col_last tright">

						<div class="clear"></div>

						<i class="icon-envelope2"></i> tri@akasah.id <span class="middot">&middot;</span> <i class="icon-headphones"></i> +628-222-0000-796
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="<?php echo url('/') ?>/template/cooming_soon/master/js/jquery.js"></script>
	<script src="<?php echo url('/') ?>/template/cooming_soon/master/js/plugins.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="<?php echo url('/') ?>/template/cooming_soon/master/js/functions.js"></script>

	<script>
		$(function() {
			$( "#side-navigation" ).tabs({ show: { effect: "fade", duration: 400 } });
		});
		$('#form_link').submit(function(e) {
			$('#link_button').attr('disabled','true');
			$.ajax({
				url: '<?php echo url('create') ?>',
				type: 'POST',
				data: $('#form_link').serialize(),
			})
			.done(function(data) {
				data = JSON.parse(data);
				if (data.status) {
					$('#link_modal').click();
					$('#widget-subscribe-form2-email').val('https://on.akasah.id/'+data.link);
				} else {
					$('#link_modal_failed').click();
				}
				reloadGCaptcha();
				$('#link_button').removeAttr('disabled');
			})
			.fail(function() {
				console.log("error");
			});
			
			e.preventDefault();
		});

		var btn = document.getElementById('link_cb');
	    var clipboard = new ClipboardJS(btn);
	    clipboard.on('success', function(e) {
	        // console.log(e);
	    });
	    clipboard.on('error', function(e) {
	        // console.log(e);
	    });
	</script>

</body>
</html>