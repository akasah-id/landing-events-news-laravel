@extends('layout_backend')

@section('title', $title)

@section('custom_css')
	<!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/pages/floating-label.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/switchery/dist/switchery.min.css" rel="stylesheet">
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/switchery/dist/switchery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[class*="nav_ss"]').addClass('active');
            $('[class*="groups"]').addClass('active');

            $(".select2").select2();
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

        });

    </script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
			<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{$title}}</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <li class="breadcrumb-item">{{__('menu.System Support')}}</li>
                                <li class="breadcrumb-item">{{__('menu.Group Management')}}</li>
                                <li class="breadcrumb-item">{{__('access.Access Management')}}</li>
                                <li class="breadcrumb-item active">{{ $title }}</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            	<div class="card-header bg-info">
                    <h4 class="m-b-0 text-white">{{$title}}</h4>
                    <h6 class="m-b-0 text-white">{{__('access.caption')}}</h6>
                </div>
                <div class="card-body">

                    <form class="floating-labels m-t-40" method="post" action="<?php echo url($urlModule."/save/$groupID/$id") ?>">
                    	@csrf
                        <div class="form-group m-b-40">
                            <label for="input1">{{__('access.modulemenu')}}</label>
                            <select  name="menu_id" class="select2 form-control btn-secondary" style="width: 100%; height:36px;">
                                <optgroup label="{{__('access.modulemenu')}}">
                                    <?php 
                                     ?>
                                     <?php foreach ($menus as $key): ?>
                                        <option value="<?php echo $key->menu_id ?>" <?php echo @$information->menu_id==$key->menu_id ? 'selected' : '' ?>><?php echo $key->menu_name." ($key->menu_id)" ?></option>
                                     <?php endforeach ?>
                                </optgroup>
                            </select>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group m-b-5">
                                    <label for="input7" style="    display: contents;">{{__('access.Create')}}</label>
                                    <input type="checkbox" name="is_create" value="1" <?php echo @$information->is_create==1 ? 'checked' : ''; ?> class="js-switch" data-color="#009efb" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group m-b-5">
                                    <label for="input7" style="    display: contents;">{{__('access.Read')}}</label>
                                    <input type="checkbox" name="is_read" value="1" <?php echo @$information->is_read==1 ? 'checked' : ''; ?> class="js-switch" data-color="#009efb" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group m-b-5">
                                    <label for="input7" style="    display: contents;">{{__('access.Update')}}</label>
                                    <input type="checkbox" name="is_update" value="1" <?php echo @$information->is_update==1 ? 'checked' : ''; ?> class="js-switch" data-color="#009efb" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group m-b-5">
                                    <label for="input7" style="    display: contents;">{{__('access.Delete')}}</label>
                                    <input type="checkbox" name="is_delete" value="1" <?php echo @$information->is_delete==1 ? 'checked' : ''; ?> class="js-switch" data-color="#009efb" />
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
				        <center>
				        	<div class="text-xs-right">
					            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> {{__('dashboard.Save')}}</button>
					            <button type="reset" class="btn btn-inverse">{{__('dashboard.Cancel')}}</button>
					        </div>
				        </center>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection