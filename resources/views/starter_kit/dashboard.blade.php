@extends('layout_backend')

@section('title', $title)

@section('custom_css')
	<!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/pages/dashboard1.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/morrisjs/morris.css" rel="stylesheet">
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--morris JavaScript -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/raphael/raphael-min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/morrisjs/morris.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

        });
        <?php if (session()->get('alias')=='superadmin'): ?>
        $(function () {
		    "use strict";
	        // Dashboard 1 Morris-chart
		    Morris.Area({
		        element: 'morris-area-chart'
		        , data: [{
		                period: '2010'
		                , iphone: 50
		                , ipad: 80
		                , itouch: 20
		        }, {
		                period: '2011'
		                , iphone: 130
		                , ipad: 100
		                , itouch: 80
		        }, {
		                period: '2012'
		                , iphone: 80
		                , ipad: 60
		                , itouch: 70
		        }, {
		                period: '2013'
		                , iphone: 70
		                , ipad: 200
		                , itouch: 140
		        }, {
		                period: '2014'
		                , iphone: 180
		                , ipad: 150
		                , itouch: 140
		        }, {
		                period: '2015'
		                , iphone: 105
		                , ipad: 100
		                , itouch: 80
		        }
		            , {
		                period: '2016'
		                , iphone: 250
		                , ipad: 150
		                , itouch: 200
		        }]
		        , xkey: 'period'
		        , ykeys: ['iphone', 'ipad', 'itouch']
		        , labels: ['iPhone', 'iPad', 'iPod Touch']
		        , pointSize: 3
		        , fillOpacity: 0
		        , pointStrokeColors: ['#00bfc7', '#fb9678', '#9675ce']
		        , behaveLikeLine: true
		        , gridLineColor: '#e0e0e0'
		        , lineWidth: 3
		        , hideHover: 'auto'
		        , lineColors: ['#00bfc7', '#fb9678', '#9675ce']
		        , resize: true
		    });
		    Morris.Area({
		        element: 'morris-area-chart2'
		        , data: [{
		                period: '2010'
		                , SiteA: 0
		                , SiteB: 0
		        , }, {
		                period: '2011'
		                , SiteA: 130
		                , SiteB: 100
		        , }, {
		                period: '2012'
		                , SiteA: 80
		                , SiteB: 60
		        , }, {
		                period: '2013'
		                , SiteA: 70
		                , SiteB: 200
		        , }, {
		                period: '2014'
		                , SiteA: 180
		                , SiteB: 150
		        , }, {
		                period: '2015'
		                , SiteA: 105
		                , SiteB: 90
		        , }
		            , {
		                period: '2016'
		                , SiteA: 250
		                , SiteB: 150
		        , }]
		        , xkey: 'period'
		        , ykeys: ['SiteA', 'SiteB']
		        , labels: ['Site A', 'Site B']
		        , pointSize: 0
		        , fillOpacity: 0.4
		        , pointStrokeColors: ['#b4becb', '#01c0c8']
		        , behaveLikeLine: true
		        , gridLineColor: '#e0e0e0'
		        , lineWidth: 0
		        , smooth: false
		        , hideHover: 'auto'
		        , lineColors: ['#b4becb', '#01c0c8']
		        , resize: true
		    });
		});    
		    // sparkline
		    var sparklineLogin = function() { 
		        $('#sales1').sparkline([20, 40, 30], {
		            type: 'pie',
		            height: '90',
		            resize: true,
		            sliceColors: ['#01c0c8', '#7d5ab6', '#ffffff']
		        });
		        $('#sparkline2dash').sparkline([6, 10, 9, 11, 9, 10, 12], {
		            type: 'bar',
		            height: '154',
		            barWidth: '4',
		            resize: true,
		            barSpacing: '10',
		            barColor: '#25a6f7'
		        });
		        
		    };    
		    var sparkResize;
		 
		        $(window).resize(function(e) {
		            clearTimeout(sparkResize);
		            sparkResize = setTimeout(sparklineLogin, 500);
		        });
		        sparklineLogin();
	    <?php endif ?>
    </script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
			<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{__('dashboard.Dashboard')}} <?php echo session()->get('lasturl') ?></h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <li class="breadcrumb-item active">{{__('dashboard.Dashboard')}}</li>
                            </ol>
                            <!-- <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<?php if (session()->get('alias')=='superadmin'): ?>
				<!-- ============================================================== -->
                <!-- Info box -->
                <!-- ============================================================== -->
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="icon-screen-desktop"></i></h3>
                                            <p class="text-muted">MY NEW CLIENTS</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-primary">23</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-primary" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="icon-note"></i></h3>
                                            <p class="text-muted">NEW PROJECTS</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-cyan">169</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-cyan" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="icon-doc"></i></h3>
                                            <p class="text-muted">NEW INVOICES</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-purple">157</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-purple" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="icon-bag"></i></h3>
                                            <p class="text-muted">All PROJECTS</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-success">431</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Over Visitor, Our income , slaes different and  sales prediction -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-8 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex m-b-40 align-items-center no-block">
                                    <h5 class="card-title ">YEARLY SALES</h5>
                                    <div class="ml-auto">
                                        <ul class="list-inline font-12">
                                            <li><i class="fa fa-circle text-cyan"></i> Iphone</li>
                                            <li><i class="fa fa-circle text-primary"></i> Ipad</li>
                                            <li><i class="fa fa-circle text-purple"></i> Ipod</li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="morris-area-chart" style="height: 340px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-lg-4 col-md-12">
                        <div class="row">
                            <!-- Column -->
                            <div class="col-md-12">
                                <div class="card bg-cyan text-white">
                                    <div class="card-body ">
                                        <div class="row weather">
                                            <div class="col-6 m-t-40">
                                                <h3>&nbsp;</h3>
                                                <div class="display-4">73<sup>°F</sup></div>
                                                <p class="text-white">AHMEDABAD, INDIA</p>
                                            </div>
                                            <div class="col-6 text-right">
                                                <h1 class="m-b-"><i class="wi wi-day-cloudy-high"></i></h1>
                                                <b class="text-white">SUNNEY DAY</b>
                                                <p class="op-5">April 14</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-12">
                                <div class="card bg-primary text-white">
                                    <div class="card-body">
                                        <div id="myCarouse2" class="carousel slide" data-ride="carousel">
                                            <!-- Carousel items -->
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <h4 class="cmin-height">My Acting blown <span class="font-medium">Your Mind</span> and you also <br/>laugh at the moment</h4>
                                                    <div class="d-flex no-block">
                                                        <span><img src="<?php echo url('/') ?>/template/eliteadmin/assets/images/users/1.jpg" alt="user" width="50" class="img-circle"></span>
                                                        <span class="m-l-10">
                                                    <h4 class="text-white m-b-0">Govinda</h4>
                                                    <p class="text-white">Actor</p>
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <h4 class="cmin-height">My Acting blown <span class="font-medium">Your Mind</span> and you also <br/>laugh at the moment</h4>
                                                    <div class="d-flex no-block">
                                                        <span><img src="<?php echo url('/') ?>/template/eliteadmin/assets/images/users/2.jpg" alt="user" width="50" class="img-circle"></span>
                                                        <span class="m-l-10">
                                                    <h4 class="text-white m-b-0">Govinda</h4>
                                                    <p class="text-white">Actor</p>
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <h4 class="cmin-height">My Acting blown <span class="font-medium">Your Mind</span> and you also <br/>laugh at the moment</h4>
                                                    <div class="d-flex no-block">
                                                        <span><img src="<?php echo url('/') ?>/template/eliteadmin/assets/images/users/3.jpg" alt="user" width="50" class="img-circle"></span>
                                                        <span class="m-l-10">
                                                    <h4 class="text-white m-b-0">Govinda</h4>
                                                    <p class="text-white">Actor</p>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Comment - table -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- Comment widgets -->
                    <!-- ============================================================== -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Recent Comments</h5>
                            </div>
                            <!-- ============================================================== -->
                            <!-- Comment widgets -->
                            <!-- ============================================================== -->
                            <div class="comment-widgets">
                                <!-- Comment Row -->
                                <div class="d-flex no-block comment-row">
                                    <div class="p-2"><span class="round"><img src="<?php echo url('/') ?>/template/eliteadmin/assets/images/users/1.jpg" alt="user" width="50"></span></div>
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium">James Anderson</h5>
                                        <p class="m-b-10 text-muted">Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has beenorem Ipsum is simply dummy text of the printing and type setting industry.</p>
                                        <div class="comment-footer">
                                            <span class="text-muted pull-right">April 14, 2016</span> <span class="badge badge-pill badge-info">Pending</span> <span class="action-icons">
                                                    <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)"><i class="ti-check"></i></a>
                                                    <a href="javascript:void(0)"><i class="ti-heart"></i></a>    
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Comment Row -->
                                <div class="d-flex no-block comment-row border-top">
                                    <div class="p-2"><span class="round"><img src="<?php echo url('/') ?>/template/eliteadmin/assets/images/users/2.jpg" alt="user" width="50"></span></div>
                                    <div class="comment-text active w-100">
                                        <h5 class="font-medium">Michael Jorden</h5>
                                        <p class="m-b-10 text-muted">Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has beenorem Ipsum is simply dummy text of the printing and type setting industry..</p>
                                        <div class="comment-footer">
                                            <span class="text-muted pull-right">April 14, 2016</span>
                                            <span class="badge badge-pill badge-success">Approved</span>
                                            <span class="action-icons active">
                                                    <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)"><i class="icon-close"></i></a>
                                                    <a href="javascript:void(0)"><i class="ti-heart text-danger"></i></a>    
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Comment Row -->
                                <div class="d-flex no-block comment-row border-top">
                                    <div class="p-2"><span class="round"><img src="<?php echo url('/') ?>/template/eliteadmin/assets/images/users/3.jpg" alt="user" width="50"></span></div>
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium">Johnathan Doeting</h5>
                                        <p class="m-b-10 text-muted">Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has beenorem Ipsum is simply dummy text of the printing and type setting industry.</p>
                                        <div class="comment-footer">
                                            <span class="text-muted pull-right">April 14, 2016</span>
                                            <span class="badge badge-pill badge-danger">Rejected</span>
                                            <span class="action-icons">
                                                    <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)"><i class="ti-check"></i></a>
                                                    <a href="javascript:void(0)"><i class="ti-heart"></i></a>    
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Comment Row -->
                                <div class="d-flex no-block comment-row border-top">
                                    <div class="p-2"><span class="round"><img src="<?php echo url('/') ?>/template/eliteadmin/assets/images/users/4.jpg" alt="user" width="50"></span></div>
                                    <div class="comment-text active w-100">
                                        <h5 class="font-medium">Genelia doe</h5>
                                        <p class="m-b-10 text-muted">Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has beenorem Ipsum is simply dummy text of the printing and type setting industry..</p>
                                        <div class="comment-footer">
                                            <span class="text-muted pull-right">April 14, 2016</span>
                                            <span class="badge badge-pill badge-success">Approved</span>
                                            <span class="action-icons active">
                                                    <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)"><i class="icon-close"></i></a>
                                                    <a href="javascript:void(0)"><i class="ti-heart text-danger"></i></a>    
                                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- Table -->
                    <!-- ============================================================== -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div>
                                        <h5 class="card-title">Sales Overview</h5>
                                        <h6 class="card-subtitle">Check the monthly sales </h6>
                                    </div>
                                    <div class="ml-auto">
                                        <select class="custom-select b-0">
                                            <option>January</option>
                                            <option value="1">February</option>
                                            <option value="2" selected="">March</option>
                                            <option value="3">April</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body bg-light">
                                <div class="row">
                                    <div class="col-6">
                                        <h3>March 2017</h3>
                                        <h5 class="font-light m-t-0">Report for this month</h5></div>
                                    <div class="col-6 align-self-center display-6 text-right">
                                        <h2 class="text-success">$3,690</h2></div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>NAME</th>
                                            <th>STATUS</th>
                                            <th>DATE</th>
                                            <th>PRICE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td class="txt-oflo">Elite admin</td>
                                            <td><span class="badge badge-success badge-pill">sale</span> </td>
                                            <td class="txt-oflo">April 18, 2017</td>
                                            <td><span class="text-success">$24</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">2</td>
                                            <td class="txt-oflo">Real Homes WP Theme</td>
                                            <td><span class="badge badge-info badge-pill">extended</span></td>
                                            <td class="txt-oflo">April 19, 2017</td>
                                            <td><span class="text-info">$1250</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">3</td>
                                            <td class="txt-oflo">Ample Admin</td>
                                            <td><span class="badge badge-info badge-pill">extended</span></td>
                                            <td class="txt-oflo">April 19, 2017</td>
                                            <td><span class="text-info">$1250</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">4</td>
                                            <td class="txt-oflo">Medical Pro WP Theme</td>
                                            <td><span class="badge badge-danger badge-pill">tax</span></td>
                                            <td class="txt-oflo">April 20, 2017</td>
                                            <td><span class="text-danger">-$24</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">5</td>
                                            <td class="txt-oflo">Hosting press html</td>
                                            <td><span class="badge badge-success badge-pill">sale</span></td>
                                            <td class="txt-oflo">April 21, 2017</td>
                                            <td><span class="text-success">$24</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">6</td>
                                            <td class="txt-oflo">Digital Agency PSD</td>
                                            <td><span class="badge badge-success badge-pill">sale</span> </td>
                                            <td class="txt-oflo">April 23, 2017</td>
                                            <td><span class="text-danger">-$14</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">7</td>
                                            <td class="txt-oflo">Helping Hands WP Theme</td>
                                            <td><span class="badge badge-warning badge-pill">member</span></td>
                                            <td class="txt-oflo">April 22, 2017</td>
                                            <td><span class="text-success">$64</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">8</td>
                                            <td class="txt-oflo">Ample Admin</td>
                                            <td><span class="badge badge-info badge-pill">extended</span></td>
                                            <td class="txt-oflo">April 19, 2017</td>
                                            <td><span class="text-info">$1250</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Comment - chats -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Over Visitor, Our income , slaes different and  sales prediction -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-8 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex m-b-40 align-items-center no-block">
                                    <h5 class="card-title ">SALES DIFFERENCE</h5>
                                    <div class="ml-auto">
                                        <ul class="list-inline font-12">
                                            <li><i class="fa fa-circle text-cyan"></i> SITE A</li>
                                            <li><i class="fa fa-circle text-primary"></i> SITE B</li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="morris-area-chart2" style="height: 340px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-lg-4 col-md-12">
                        <div class="row">
                            <!-- Column -->
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">SALES DIFFERENCE</h5>
                                        <div class="row">
                                            <div class="col-6  m-t-30">
                                                <h1 class="text-info">$647</h1>
                                                <p class="text-muted">APRIL 2017</p>
                                                <b>(150 Sales)</b> </div>
                                            <div class="col-6">
                                                <div id="sparkline2dash" class="text-right"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-12">
                                <div class="card bg-purple text-white">
                                    <div class="card-body">
                                        <h5 class="card-title">VISIT STATASTICS</h5>
                                        <div class="row">
                                            <div class="col-6  m-t-30">
                                                <h1 class="text-white">$347</h1>
                                                <p class="light_op_text">APRIL 2017</p>
                                                <b class="text-white">(150 Sales)</b> </div>
                                            <div class="col-6">
                                                <div id="sales1" class="text-right"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->          

<?php elseif (session()->get('alias')=='administrator'): ?>
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">{{__('dashboard.welcome')}}<?php echo session()->get('first_name') ?> <?php echo session()->get('last_name') ?>!</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4" style="border-right: 1px solid #f99f1b;">
                                <div class="card text-black">
                                    <div class="card-body text-center">
                                        <?php if (session()->get('avatar')==''): ?>
                                            <img style="max-height: 200px" src="<?php echo url('template/eliteadmin/assets/images/users/1.jpg') ?>"> 
                                        <?php else: ?>
                                            <img style="max-height: 200px" src="{{ url('file/images/users/'.session()->get('avatar')) }}">
                                        <?php endif ?><hr>
                                        <h3 class="card-title"><?php echo session()->get('first_name').' '.session()->get('last_name') ?></h3>
                                        <p class="card-text"><?php echo session()->get('intro') ?></p>
                                        <a href="<?php echo url('/users/view/'.session()->get('user_id')) ?>" class="btn waves-effect waves-light btn-outline-info">{{__('dashboard.editprofile')}}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4" style="border-right: 1px solid #f99f1b;">
                                <div class="card text-black">
                                    <div class="card-body text-center">
                                        <br><br>
                                        <h1><i class="fa fa-user-shield fa-lg" style="color: #F89F1C;font-size: 3em;"></i></h1>
                                        <br>
                                        <h3 class="card-title">{{__('user.status_admin')}}</h3>
                                        <p class="card-text">{{__('user.status_admin_text')}}</p>
                                        <a href="https://akasah.id" target="_blank" class="btn waves-effect waves-light btn-outline-info">{{__('user.sharethis')}}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <div class="card text-black">
                                    <div class="card-body">
                                        <h3 class="card-title">{{__('dashboard.recentact')}}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php elseif (session()->get('alias')=='the-contributor'): ?>
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">{{__('dashboard.welcome')}}<?php echo session()->get('first_name') ?> <?php echo session()->get('last_name') ?>!</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4" style="border-right: 1px solid #f99f1b;">
                                <div class="card text-black">
                                    <div class="card-body text-center">
                                        <?php if (session()->get('avatar')==''): ?>
                                            <img style="max-height: 200px" src="<?php echo url('template/eliteadmin/assets/images/users/1.jpg') ?>"> 
                                        <?php else: ?>
                                            <img style="max-height: 200px" src="{{ url('file/images/users/'.session()->get('avatar')) }}">
                                        <?php endif ?><hr>
                                        <h3 class="card-title"><?php echo session()->get('first_name').' '.session()->get('last_name') ?></h3>
                                        <p class="card-text"><?php echo session()->get('intro') ?></p>
                                        <a href="<?php echo url('/users/view/'.session()->get('user_id')) ?>" class="btn waves-effect waves-light btn-outline-info">{{__('dashboard.editprofile')}}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4" style="border-right: 1px solid #f99f1b;">
                                <div class="card text-black">
                                    <div class="card-body text-center">
                                        <br><br>
                                        <h1><i class="fa fa-chess-queen fa-lg" style="color: #F89F1C;font-size: 3em;"></i></h1>
                                        <br>
                                        <h3 class="card-title">Head Analyst Weltrade Indonesia</h3>
                                        <p class="card-text">{{__('user.status_thecontributor_text')}}</p>
                                        <a href="https://weltrade-idn.com" target="_blank" class="btn waves-effect waves-light btn-outline-info">Website Utama</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <div class="card text-black">
                                    <div class="card-body">
                                        <h3 class="card-title">{{__('dashboard.recentact')}}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php elseif (session()->get('alias')=='contributor-education'): ?>
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">{{__('dashboard.welcome')}}<?php echo session()->get('first_name') ?> <?php echo session()->get('last_name') ?>!</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4" style="border-right: 1px solid #f99f1b;">
                                <div class="card text-black">
                                    <div class="card-body text-center">
                                        <?php if (session()->get('avatar')==''): ?>
                                            <img style="max-height: 200px" src="<?php echo url('template/eliteadmin/assets/images/users/1.jpg') ?>"> 
                                        <?php else: ?>
                                            <img style="max-height: 200px" src="{{ url('file/images/users/'.session()->get('avatar')) }}">
                                        <?php endif ?><hr>
                                        <h3 class="card-title"><?php echo session()->get('first_name').' '.session()->get('last_name') ?></h3>
                                        <p class="card-text"><?php echo session()->get('intro') ?></p>
                                        <a href="<?php echo url('/users/view/'.session()->get('user_id')) ?>" class="btn waves-effect waves-light btn-outline-info">{{__('dashboard.editprofile')}}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4" style="border-right: 1px solid #f99f1b;">
                                <div class="card text-black">
                                    <div class="card-body text-center">
                                        <br><br>
                                        <h1><i class="fa fa-chess-rook fa-lg" style="color: #F89F1C;font-size: 3em;"></i></h1>
                                        <br>
                                        <h3 class="card-title">{{__('user.status_education')}}</h3>
                                        <p class="card-text">{{__('user.status_education_text')}}
                                        </p>
                                        <a href="<?php echo url('/profession') ?>" target="_blank" class="btn waves-effect waves-light btn-outline-info">{{__('user.status_education_create')}}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <div class="card text-black">
                                    <div class="card-body">
                                        <h3 class="card-title">{{__('dashboard.recentact')}}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php elseif (session()->get('alias')=='contributor-wazzap'): ?>
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">{{__('dashboard.welcome')}}<?php echo session()->get('first_name') ?> <?php echo session()->get('last_name') ?>!</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4" style="border-right: 1px solid #f99f1b;">
                                <div class="card text-black">
                                    <div class="card-body text-center">
                                        <?php if (session()->get('avatar')==''): ?>
                                            <img style="max-height: 200px" src="<?php echo url('template/eliteadmin/assets/images/users/1.jpg') ?>"> 
                                        <?php else: ?>
                                            <img style="max-height: 200px" src="{{ url('file/images/users/'.session()->get('avatar')) }}">
                                        <?php endif ?><hr>
                                        <h3 class="card-title"><?php echo session()->get('first_name').' '.session()->get('last_name') ?></h3>
                                        <p class="card-text"><?php echo session()->get('intro') ?></p>
                                        <a href="<?php echo url('/users/view/'.session()->get('user_id')) ?>" class="btn waves-effect waves-light btn-outline-info">{{__('dashboard.editprofile')}}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4" style="border-right: 1px solid #f99f1b;">
                                <div class="card text-black">
                                    <div class="card-body text-center">
                                        <br><br>
                                        <h1><i class="fa fa-chess-queen fa-lg" style="color: #F89F1C;font-size: 3em;"></i></h1>
                                        <br>
                                        <h3 class="card-title">Head Analyst <br>Weltrade Indonesia</h3>
                                        <p class="card-text">Hebat Sekali! ini tidak hanya sekedar status, ini mewakilkan kontribusi Anda untuk dunia melalui Weltrade! Atas nama Weltrade, terima kasih banyak, Anda memang orang yang baik dan hebat!</p>
                                        <a href="https://weltrade-idn.com" target="_blank" class="btn waves-effect waves-light btn-outline-info">Website Utama</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <div class="card text-black">
                                    <div class="card-body">
                                        <h3 class="card-title">{{__('dashboard.recentact')}}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php elseif (session()->get('alias')=='member'): ?>
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">{{__('dashboard.welcome')}}<?php echo session()->get('first_name') ?> <?php echo session()->get('last_name') ?>!</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4" style="border-right: 1px solid #f99f1b;">
                                <div class="card text-black">
                                    <div class="card-body text-center">
                                        <?php if (session()->get('avatar')==''): ?>
                                            <img style="max-height: 200px" src="<?php echo url('template/eliteadmin/assets/images/users/1.jpg') ?>"> 
                                        <?php else: ?>
                                            <img style="max-height: 200px" src="{{ url('file/images/users/'.session()->get('avatar')) }}">
                                        <?php endif ?><hr>
                                        <h3 class="card-title"><?php echo session()->get('first_name').' '.session()->get('last_name') ?></h3>
                                        <p class="card-text"><?php echo session()->get('intro') ?></p>
                                        <a href="<?php echo url('/users/view/'.session()->get('user_id')) ?>" class="btn waves-effect waves-light btn-outline-info">{{__('dashboard.editprofile')}}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-4" style="border-right: 1px solid #f99f1b;">
                                <div class="card text-black">
                                    <div class="card-body text-center">
                                        <br><br>
                                        <h1><i class="fa fa-chess-rook fa-lg" style="color: #F89F1C;font-size: 3em;"></i></h1>
                                        <br>
                                        <h3 class="card-title">{{__('user.status_member')}}</h3>
                                        <p class="card-text">{{__('user.status_member_text')}}</p>
                                        </p>
                                        <a href="https://akasah.id" target="_blank" class="btn waves-effect waves-light btn-outline-info">{{__('user.exploremore')}}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <div class="card text-black">
                                    <div class="card-body">
                                        <h3 class="card-title">{{__('dashboard.recentact')}}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php endif ?>
@endsection