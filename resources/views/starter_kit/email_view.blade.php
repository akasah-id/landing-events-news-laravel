@extends('layout_backend')

@section('title', $title)

@section('custom_css')
    <!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/pages/floating-label.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/multiselect/css/multi-select.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <!-- Page plugins css -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">


    <style>
        .bootstrap-tagsinput {
            display: block;
        }
    </style>
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/multiselect/js/jquery.multi-select.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/moment/moment.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('[class*="nav_ss"]').addClass('active');
            $('[class*="email"]').addClass('active');
            $(".select2").select2();    
            $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });
        });

    </script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
            <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{$title}}</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <li class="breadcrumb-item">{{__('menu.System Support')}}</li>
                                <li class="breadcrumb-item">{{__('menu.Email Log')}}</li>
                                <li class="breadcrumb-item active">{{$title}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
                <div class="card-header bg-info">
                    <h4 class="m-b-0 text-white">{{$title}}</h4>
                    <h6 class="m-b-0 text-white">{{__('email.formCaption')}}</h6>
                </div>
                <div class="card-body">

                    <form class="floating-labels m-t-40" enctype="multipart/form-data" method="post" action="<?php echo url($urlModule.'/save/'.$id) ?>">
                        @csrf

                        <div class="form-group m-b-40">
                            <label for="input1">{{__('email.formEmail')}}</label>
                            <select  name="user_id" class="select2 form-control btn-secondary" style="width: 100%; height:36px;">
                                <optgroup label="{{__('email.formEmail')}}">
                                     <?php foreach ($user as $key): ?>
                                        <option value="<?php echo $key->user_id ?>" <?php echo @$information->user_id==$key->user_id ? 'selected' : '' ?>><?php echo "$key->first_name $key->last_name ($key->email)" ?></option>
                                     <?php endforeach ?>
                                </optgroup>
                            </select>
                        </div>

                        <div class="form-group m-b-40">
                            <input name="title" type="text" class="form-control" value="<?php echo @$information->title ?>" required>
                            <span class="bar"></span>
                            <label>{{__('email.formtitle')}}</label>
                        </div>

                        <div class="form-group m-b-40">
                            <input name="type" type="text" class="form-control" value="<?php echo @$information->type ?>" required>
                            <span class="bar"></span>
                            <label>{{__('email.formtype')}}</label>
                        </div>

                        <div class="form-group m-b-5">
                            <textarea name="msg" class="form-control" rows="4"><?php echo @$information->msg ?></textarea>
                            <span class="bar"></span>
                            <label for="input7">{{__('email.formMsg')}}</label>
                        </div>

                        <br>
                        <center>
                            <?php if ($id==0): ?>
                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> {{__('dashboard.Save')}}</button>
                                <button type="reset" class="btn btn-inverse">{{__('dashboard.Cancel')}}</button>
                            </div>
                            <?php endif ?>
                        </center>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection