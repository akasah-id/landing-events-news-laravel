@extends('layout_backend')

@section('title', __('menu.File Manager'))

@section('custom_css')
    <!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/sweetalert/sweetalert.css" rel="stylesheet">
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/sweetalert/jquery.sweet-alert.custom.js"></script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
            <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{__('menu.File Manager')}}</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <li class="breadcrumb-item active">{{__('menu.Tools')}}</li>
                                <li class="breadcrumb-item active">{{__('menu.File Manager')}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <iframe src="<?php echo url('filemanager') ?>" style="width: 100%; height: 700px; overflow: hidden; border: none;"></iframe>
    </div>
</div>

@endsection