@extends('layout_backend')

@section('title', $title)

@section('custom_css')
	<!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/sweetalert/sweetalert.css" rel="stylesheet">
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/sweetalert/jquery.sweet-alert.custom.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            init_datatable(0, "<?php echo url($urlModule.'/ajax_data') ?>",<?php echo $column ?>);
            delbtn();
        });

        function delbtn() {
            <?php if ($access->grant_check('is_delete', $menuID, true)): ?>
                $('#datatable-complete_length').append('<?php echo $delBtn ?>');
            <?php endif ?>
        }
    </script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
			<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{$title}}</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <li class="breadcrumb-item">{{__('menu.System Support')}}</li>
                                <li class="breadcrumb-item active">{{$title}}</li>
                            </ol>
                            <?php if ($access->grant_check('is_create', $menuID, true)): ?>
                            <a href="<?php echo url($urlModule.'/new') ?>"><button type="button" class="btn btn-info addBtnMobile d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> {{__('dashboard.Create New')}}</button></a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">{{$title}}</h4>
                <h6 class="m-b-0 text-white">{{__('group.caption')}}</h6>
            </div>
            <div class="card-body no-padding-top">
                <table class="table table-striped table-bordered table-checkable order-column" id="datatable-complete">
                <thead>
                <tr class="form-material" role="row" id="filterrow">
                    <td> <i class="fa fa-search fa-lg"></i></td>
                    <td>
                        <input onclick="stopPropagation(event);" type="text" class="form-control form-filter input-sm" style="width:100%"> 
                    </td>
                    <td>
                        <div class="margin-bottom-5">
                            <button onclick='reset("<?php echo url($urlModule.'/ajax_data') ?>",<?php echo $column ?>);delbtn()' class="btn waves-effect waves-light btn-outline-info">
                                <i class="fa fa-refresh"></i> {{__('dashboard.Reset / Refresh')}}
                            </button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th><input name="select_all" value="1" type="checkbox"></th>
                    <th>{{__('dashboard.Name')}}</th>
                    <th>{{__('dashboard.Action')}}</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

@endsection