@extends('layout_backend')

@section('title', $title)

@section('custom_css')
	<!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/pages/floating-label.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet">
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[class*="nav_ss"]').addClass('active');
            $('[class*="groups"]').addClass('active');
        });

    </script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
			<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{$title}}</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <li class="breadcrumb-item">{{__('menu.System Support')}}</li>
                                <li class="breadcrumb-item">{{__('menu.Group Management')}}</li>
                                <li class="breadcrumb-item active">{{ $title }}</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            	<div class="card-header bg-info">
                    <h4 class="m-b-0 text-white">{{$title}}</h4>
                    <h6 class="m-b-0 text-white">{{__('group.formGroupCaption')}}</h6>
                </div>
                <div class="card-body">

                    <form class="floating-labels m-t-40" method="post" action="<?php echo url($urlModule.'/save/'.$id) ?>">
                    	@csrf
                        <div class="form-group m-b-40">
                            <input name="name" type="text" class="form-control" value="<?php echo @$information->name ?>" required>
                            <span class="bar"></span>
                            <label>{{__('dashboard.formName')}}</label>
                        </div>

                        <div class="form-group m-b-5">
                            <textarea name="description" class="form-control" rows="4"><?php echo @$information->description ?></textarea>
                            <span class="bar"></span>
                            <label for="input7">{{__('dashboard.formDesc')}}</label>
                        </div>

                        <br>
				        <center>
				        	<div class="text-xs-right">
					            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> {{__('dashboard.Save')}}</button>
					            <button type="reset" class="btn btn-inverse">{{__('dashboard.Cancel')}}</button>
					        </div>
				        </center>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection