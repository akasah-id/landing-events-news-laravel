
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo url('/') ?>/template/eliteadmin/assets/images/favicon.png">
    <title>{{$title}} - Akasah </title>
    
    <!-- page css -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/pages/login-register-lock.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/style.css" rel="stylesheet">
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">eXchanger Market Admin Panel</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url(<?php echo url('/') ?>/template/eliteadmin/assets/images/background/login-register.jpg);">
            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-material" method="post" action="<?php echo url('unlock') ?>">
                    	@csrf
                        <div class="form-group">
                            <div class="col-xs-12 text-center">
                                <div class="user-thumb text-center"> 
                                	<?php if (session()->get('avatar')==''): ?>
	                                    <img class="img-circle" width="100" src="<?php echo url('template/eliteadmin/assets/images/users/1.jpg') ?>"> 
	                                <?php else: ?>
	                                    <img class="img-circle" width="100" src="{{ url('file/images/users/'.session()->get('avatar')) }}">
	                                <?php endif ?>
                                    <h3><?php echo session()->get('first_name').' '.session()->get('last_name') ?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" name="password" type="password" required="" placeholder="{{__('login.Password')}}">
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{__('login.Log In')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/popper/popper.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/toast-master/js/jquery.toast.js"></script>
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });

        $(document).ready(function() {
            <?php if (Session::has('message')): ?>
            $.toast({
                heading: '<?php echo session('title') ?>'
                , text: '<?php echo session('message') ?>'
                , position: 'top-right'
                , loaderBg: '#fff'
                , icon: '<?php echo session('type') ?>'
                , hideAfter: 5000
                , stack: 6
            })
            <?php endif ?>
        });
        
    </script>
</body>

</html>