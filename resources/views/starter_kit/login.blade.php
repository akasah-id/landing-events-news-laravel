<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo url('/') ?>/template/eliteadmin/assets/images/favicon.png">
    <title>{{ __('login.Login - Akasah') }}</title>
    
    <!-- page css -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/pages/login-register-lock.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="horizontal-nav skin-exchanger-market card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Akasah.id</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div class="floating-lang">
        <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-globe" style="font-size: 30px; vertical-align: middle "></i></a>
        <div class="dropdown-menu dropdown-menu-right animated flipInY">
            <!-- text-->
            <a href="javascript:void(0)" onclick="changeLang('en')" class="dropdown-item">{{__('user.en')}}</a>
            <a href="javascript:void(0)" onclick="changeLang('id')" class="dropdown-item">{{__('user.id')}}</a>
            <!-- text-->
        </div>
    </div>

    <section id="wrapper" class="login-register login-sidebar" style="background-image:url(<?php echo url('/') ?>/template/eliteadmin/assets/images/background/login-register.jpg);">
        <div class="login-box card">
            <?php if (Session::has('message')): ?>
            <div class="alert alert-<?php echo session('type') ?>">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <h3 class="text-<?php echo session('type') ?>"><i class="fa fa-info-circle"></i> <?php echo session('title') ?></h3> 
                <?php echo session('message') ?>
            </div>
            <?php endif ?>

            <div class="card-body">
                <form method="post" action="{{ url('/login/process') }}" class="form-horizontal form-material" id="loginform" action="dashboard">
                 @csrf
                    <a href="javascript:void(0)" class="text-center db"><center><img src="<?php echo url('/') ?>/template/eliteadmin/assets/images/logo.png" alt="Home" width="300"/></center></a>
                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" name="email" required="" placeholder="{{ __('login.Username') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="password" required="" placeholder="{{__('login.Password')}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <!-- <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">{{__('login.Remember me')}}</label>
                            </div>    -->  
                        <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> {{__('login.Forgot pwd?')}}</a> 
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">{{__('login.Log In')}}</button>
                        </div>
                    </div>
                   <!--  <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
                        </div>
                    </div> -->
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            {{__('login.no_account')}} <a href="javascript:void(0)" id="to-register" class="text-primary m-l-5"><b>{{__('login.Sign Up')}}</b></a>
                        </div>
                    </div>
                </form>
                <form class="form-horizontal" id="recoverform" method="post" action="<?php echo url('login/reset') ?>">
                    @csrf
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>{{__('login.Recover Password')}}</h3>
                            <p class="text-muted">{{__('login.Enter your Email and instructions will be sent to you!')}} </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" name="email" type="text" required="" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{__('login.Reset')}}</button>
                            <button id="to-login" class="btn btn-ouline-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="button">{{__('login.Back')}}</button>
                        </div>
                    </div>
                </form>
                <form class="form-horizontal hide" id="registerform" method="post" action="<?php echo url('login/register') ?>">
                    @csrf
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>{{__('login.Sign Up')}}</h3>
                            <p class="text-muted">{{__('login.signupmsg')}} </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" name="first_name" type="text" required="" placeholder="{{__('login.Your')}}">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" name="last_name" type="text" required="" placeholder="{{__('login.Name')}}">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" name="username" type="text" required="" placeholder="{{__('login.@yourname')}}">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" name="email" type="email" required="" placeholder="{{__('login.youremail@mail.com')}}">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" name="password" type="password" required="" placeholder="{{__('login.pass****')}}">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{__('login.Register')}}</button>
                            <button id="to-login2" class="btn btn-ouline-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="button">{{__('login.Back')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/popper/popper.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.js"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ============================================================== 
        // Login and Recover Password 
        // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });

        $('#to-register').on("click", function() {
            $("#loginform").slideUp();
            $("#registerform").fadeIn();
        });

        $('#to-login').on('click', function() {
            $("#loginform").slideDown();
            $("#recoverform").fadeOut();
        });

        $('#to-login2').on('click', function() {
            $("#loginform").slideDown();
            $("#registerform").fadeOut();
        });

        function changeLang(lang='') {
            if (lang=='') {
                lang = $('#floating-lang').val();
            } else {
                lang = lang;
            }
            $.ajax({
                url: '<?php echo url("login/changelang/") ?>/'+lang,
                type: 'GET',
            })
            .done(function() {
                window.location.reload();
            })
            .fail(function(e) {
                console.log(e);
            });
        }
    </script>
    
</body>

</html>