@extends('layout_backend')

@section('title', $title)

@section('custom_css')
	<!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/nestable/nestable.css"" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/sweetalert/sweetalert.css" rel="stylesheet">
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/nestable/jquery.nestable.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/sweetalert/jquery.sweet-alert.custom.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#nestable").nestable({group: 1});
            $('#simpan_menu').click(function () {
                var output = window.JSON.stringify($('#nestable').nestable('serialize'));
                $.ajax({
                    url: '<?php echo url("menu/reposition") ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {'menu': output,"_token":'{{ csrf_token() }}'},
                })
                    .done(function (json) {
                        if (json.stat) {
                            // console.log(json);
                            window.location.reload();
                        }
                        ;
                    })
                    .fail(function (e) {
                        console.log(e);
                    })
                    .always(function () {
                        // console.log("complete");
                        // window.location.reload();
                    });
            });
        });
    </script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
			<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{$title}}</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <li class="breadcrumb-item">{{__('menu.System Support')}}</li>
                                <li class="breadcrumb-item active">{{$title}}</li>
                            </ol>
                            <?php if ($access->grant_check('is_create', $menu_id, true)): ?>
                            <a href="<?php echo url($urlModule.'/new') ?>"><button type="button" class="btn btn-info addBtnMobile d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> {{__('dashboard.Create New')}}</button></a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">{{$title}}</h4>
                <h6 class="m-b-0 text-white">{{__('menu.addMenuCaption')}}</h6>
            </div>
            <div class="card-body">
                <form class="form-horizontal">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="myadmin-dd dd" id="nestable" style="max-width: none;">
                                <?php echo $menus ?>
                            </div>
                        </div>
                    </div>
                    <br>
                    <legend></legend>
                    <button type="button" id="simpan_menu" class="btn btn-sm btn-info btn-icon-fixed pull-right"><span
                                class="fa fa-check"></span> {{__('menu.Update Menu Order')}}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection