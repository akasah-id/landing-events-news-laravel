@extends('layout_backend')

@section('title', $title)

@section('custom_css')
	<!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/pages/floating-label.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet">
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[class*="nav_ss"]').addClass('active');
            $('[class*="menu"]').addClass('active');

            $(".select2").select2();
	        $('.selectpicker').selectpicker();
	        check_menu_type();
        });

        function check_menu_type() {
            var mtype=$('#menu_type').val();
            if (mtype=='Internal') {
                $('#module').css('display', '');
                $('#url').css('display', 'none');
            } else {
                $('#url').css('display', '');
                $('#module').css('display', 'none');
            }
        }
    </script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
			<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{$title}}</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <li class="breadcrumb-item">{{__('menu.System Support')}}</li>
                                <li class="breadcrumb-item">{{__('menu.Menu')}}</li>
                                <li class="breadcrumb-item active">{{ $title }}</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            	<div class="card-header bg-info">
                    <h4 class="m-b-0 text-white">{{$title}}</h4>
                    <h6 class="m-b-0 text-white">{{__('menu.addMenuCaption')}}</h6>
                </div>
                <div class="card-body">

                    <form class="floating-labels m-t-40" method="post" action="<?php echo url($urlModule.'/save/'.$id) ?>">
                    	@csrf
                        <div class="form-group m-b-40">
                            <input name="menu_name" type="text" class="form-control" value="<?php echo @$information->menu_name ?>" required>
                            <span class="bar"></span>
                            <label for="input1">{{__('menu.menuName')}}</label>
                        </div>

                        <div class="form-group m-b-40">
                            <label for="input1">{{__('menu.menuType')}}</label>
                            <select  name="menu_type" onchange="check_menu_type()" id="menu_type" class="select2 form-control btn-secondary" style="width: 100%; height:36px;">
                                <optgroup label="{{__('menu.menuType')}}">
                                    <?php 
                                    $menu = array('Internal','External');
                                     ?>
                                     <?php foreach ($menu as $key): ?>
                                        <option value="<?php echo $key ?>" <?php echo @$information->menu_type==$key ? 'selected' : '' ?>><?php echo $key ?></option>
                                     <?php endforeach ?>
                                </optgroup>
					        </select>
                        </div>

                        <div class="form-group m-b-40" id="module">
                            <input name="module" type="text" class="form-control" value="<?php echo @$information->module ?>">
                            <span class="bar"></span>
                            <label for="input1">{{__('menu.menuModule')}}</label>
                        </div>
                        <div class="form-group m-b-40" id="url">
                            <input name="url" type="text" class="form-control" value="<?php echo @$information->url ?>">
                            <span class="bar"></span>
                            <label for="input1">{{__('menu.menuURL')}}</label>
                        </div>
                        <div class="form-group m-b-40">
                            <input name="menu_icons" type="text" class="form-control" value="<?php echo @$information->menu_icons ?>">
                            <span class="bar"></span>
                            <label for="input1">{{__('menu.menuIcon')}}</label>
                        </div>
                        <br>
				        <center>
				        	<div class="text-xs-right">
					            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> {{__('dashboard.Save')}}</button>
					            <button type="reset" class="btn btn-inverse">{{__('dashboard.Cancel')}}</button>
					        </div>
				        </center>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection