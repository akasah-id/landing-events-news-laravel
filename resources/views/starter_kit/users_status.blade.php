@extends('layout_backend')

@section('title', $title)

@section('custom_css')
	<!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/pages/floating-label.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet">
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
        });

    </script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
			<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{$title}}</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <li class="breadcrumb-item active">{{ $title }}</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            	<div class="card-header bg-info">
                    <h4 class="m-b-0 text-white">{{$title}}</h4>
                    <h6 class="m-b-0 text-white">{{__('user.formStatusCaption')}}</h6>
                </div>
                <?php 
                $superadmin = session()->get('alias')=='superadmin' ? true : false;
				$admin = session()->get('alias')=='administrator' ? true : false;
				$thecontributor = session()->get('alias')=='the-contributor' ? true : false;
                 ?>
                <?php if ($superadmin): ?>
                	<div class="card-body">
	                	<div class="row">
	                		<div class="col-md-12">
		                        <div class="card text-black">
		                            <div class="card-body text-center">
		                            	<h1><i class="fa fa-user-injured fa-lg" style="color: #F89F1C;font-size: 3em;"></i></h1>
		                            	<br>
		                                <h3 class="card-title">{{__('user.status_superadmin')}}</h3>
		                                <p class="card-text">{{__('user.status_superadmin_text')}}</p>
		                                <a href="https://akasah.id" target="_blank" class="btn waves-effect waves-light btn-outline-info">{{__('user.sharethis')}}</a>
		                            </div>
		                        </div>
		                    </div>
	                	</div>
	                </div>
                <?php elseif ($admin) : ?>
                	<div class="card-body">
	                	<div class="row">
	                		<div class="col-md-12">
		                        <div class="card text-black">
		                            <div class="card-body text-center">
		                            	<h1><i class="fa fa-user-shield fa-lg" style="color: #F89F1C;font-size: 3em;"></i></h1>
		                            	<br>
		                                <h3 class="card-title">{{__('user.status_admin')}}</h3>
		                                <p class="card-text">{{__('user.status_admin_text')}}</p>
		                                <a href="https://akasah.id" target="_blank" class="btn waves-effect waves-light btn-outline-info">{{__('user.sharethis')}}</a>
		                            </div>
		                        </div>
		                    </div>
	                	</div>
	                </div>
                <?php elseif ($thecontributor) : ?>
                	<div class="card-body">
	                	<div class="row">
	                		<div class="col-md-12">
		                        <div class="card text-black">
		                            <div class="card-body text-center">
		                            	<h1><i class="fa fa-chess-queen fa-lg" style="color: #F89F1C;font-size: 3em;"></i></h1>
		                            	<br>
		                                <h3 class="card-title">{{__('user.status_thecontributor')}}</h3>
		                                <p class="card-text">{{__('user.status_thecontributor_text')}}</p>
		                                <a href="https://akasah.id" target="_blank" class="btn waves-effect waves-light btn-outline-info">{{__('user.sharethis')}}</a>
		                            </div>
		                        </div>
		                    </div>
	                	</div>
	                </div>
                <?php else: ?>
                	<?php 
                	$wazzap = session()->get('alias')=='contributor-wazzap' ? true : false;
                	$education = session()->get('alias')=='contributor-education' ? true : false;
                	 ?>
                	<div class="card-body">
	                	<div class="row">
	                		<div class="col-md-12">
		                        <div class="card text-black bg-light">
		                        	<div class="card-header">
		                                <h4 class="m-b-0 text-white"></h4></div>
		                            <div class="card-body text-center">
		                            	<h1><i class="fa fa-chess-pawn fa-lg" style="color: #F89F1C;font-size: 3em;"></i></h1>
		                            	<br>
		                                <h3 class="card-title">{{__('user.status_member')}}</h3>
		                                <p class="card-text">{{__('user.status_member_text')}}</p>
		                                <a href="https://akasah.id" target="_blank" class="btn waves-effect waves-light btn-outline-info">{{__('user.exploremore')}}</a>
		                            </div>
		                        </div>
		                    </div>
	                	</div>
	                	<div class="row">
		                	<div class="col-md-6">
		                        <div class="card text-white <?php echo $wazzap ? 'bg-danger' : 'bg-dark' ?>  text-center">
		                            <div class="card-header">
		                                <h4 class="m-b-0 text-white"><?php echo $wazzap ? '' : __('user.status_locked') ?></h4></div>
		                            <div class="card-body">
		                            	<h1><i class="fa fa-chess-rook fa-lg" style="color: #F89F1C;font-size: 3em;"></i></h1>
		                            	<br>
		                                <h3 class="card-title">{{__('user.status_wazzap')}}</h3>
		                                <p class="card-text">{{__('user.status_wazzap_text')}}
		                                	<?php echo $wazzap ? '' : '<hr><strong>'.__('user.status_not_eligible').'</strong>' ?>	
	                                	</p>
	                                	<?php if ($wazzap): ?>
		                                <a href="<?php echo url('/news') ?>" target="_blank" class="btn waves-effect waves-light btn-outline-info">{{__('user.status_wazzap_create')}}</a>
	                                	<?php else: ?>
		                                <a href="https://api.whatsapp.com/send?phone=6282220000796&text=Halo" target="_blank" class="btn waves-effect waves-light btn-outline-info">{{__('user.status_contact_me')}}</a>
	                                	<?php endif ?>
		                            </div>
		                        </div>
		                    </div>
		                	<div class="col-md-6">
		                        <div class="card text-white <?php echo $education ? 'bg-purple' : 'bg-dark' ?>   text-center">
		                            <div class="card-header">
		                                <h4 class="m-b-0 text-white"><?php echo $education ? '' : __('user.status_locked') ?></h4></div>
		                            <div class="card-body">
		                            	<h1><i class="fa fa-chess-rook fa-lg" style="color: #F89F1C;font-size: 3em;"></i></h1>
		                            	<br>
		                                <h3 class="card-title">{{__('user.status_education')}}</h3>
		                                <p class="card-text">{{__('user.status_education_text')}}
		                                	<?php echo $education ? '' : '<hr><strong>'.__('user.status_not_eligible').'</strong>' ?>	
	                                	</p>
	                                	<?php if ($education): ?>
		                                <a href="<?php echo url('/profession') ?>" target="_blank" class="btn waves-effect waves-light btn-outline-info">{{__('user.status_education_create')}}</a>
	                                	<?php else: ?>
		                                <a href="https://api.whatsapp.com/send?phone=6282220000796&text=Halo" target="_blank" class="btn waves-effect waves-light btn-outline-info">{{__('user.status_contact_me')}}</a>
	                                	<?php endif ?>
		                            </div>
		                        </div>
		                    </div>
	                	</div>
	                </div>
                <?php endif ?>
        </div>
    </div>
</div>
@endsection