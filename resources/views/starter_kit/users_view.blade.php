@extends('layout_backend')

@section('title', $title)

@section('custom_css')
	<!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/pages/floating-label.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/dropify/dist/css/dropify.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <style type="text/css">
        .bootstrap-tagsinput {
            width: 100%;
        }
    </style>
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/dropify/dist/js/dropify.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/moment/moment.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[class*="nav_ss"]').addClass('active');
            $('[class*="users"]').addClass('active');
            $('.dropify').dropify();
            $(".select2").select2();
            $('.date-format').bootstrapMaterialDatePicker({ format: 'dddd, DD MMMM YYYY', time: false });
            $("#countries").select2({
                templateResult: function(item) {
                  return format(item, false);
                }
              });
        });

        function format(item, state) {
          if (!item.id) {
            return item.text;
          }
          var countryUrl = "https://lipis.github.io/flag-icon-css/flags/4x3/";
          var stateUrl = "https://oxguy3.github.io/flags/svg/us/";
          var url = state ? stateUrl : countryUrl;
          var img = $("<img>", {
            class: "img-flag",
            width: 26,
            src: url + item.element.value.toLowerCase() + ".svg"
          });
          var span = $("<span>", {
            text: " " + item.text
          });
          span.prepend(img);
          return span;
        }

        <?php 
        $school=1;
        @$school = count(json_decode($information->school));
        ?>
        var school = <?php echo $school ?>;
        var school_name = "{{__('user.formschool_name')}}";
        var school_loc = "{{__('user.formschool_loc')}}";
        var remove = "{{__('user.form_remove')}}";
        function school_field() {

            school++;
            var objTo = document.getElementById('school_field')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "row removeschool" + school);
            var rdiv = 'removeschool' + school;
            divtest.innerHTML = '<div class="col-sm-4 nopadding"><div class="form-group m-b-40"><input name="school_name[]" type="text" class="form-control" value="" required><span class="bar"></span><label>'+school_name+'</label></div></div><div class="col-sm-7 nopadding"><div class="form-group m-b-40"><input name="school_loc[]" type="url" class="form-control" value="" required><span class="bar"></span><label>'+school_loc+'</label></div></div><div class="col-sm-1 nopadding"><div class="text-right"><button class="btn btn-danger" type="button" onclick="remove_school_field(' + school + ');"> <i class="fa fa-minus"></i> '+remove+'</button></div></div>';

            objTo.appendChild(divtest)

            $('.floating-labels .form-control').on('focus blur', function (e) {
                $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
            }).trigger('blur');
        }

        function remove_school_field(rid) {
            $('.removeschool' + rid).remove();
        }

        <?php 
        $website=1;
        @$website = count(json_decode($information->website));
        ?>
        var website = <?php echo $website ?>;
        var website_name = "{{__('user.formwebsite_name')}}";
        var website_url = "{{__('user.formwebsite_url')}}";
        var remove = "{{__('user.form_remove')}}";
        function website_field() {

            website++;
            var objTo = document.getElementById('website_field')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "row removewebsite" + website);
            var rdiv = 'removewebsite' + website;
            divtest.innerHTML = '<div class="col-sm-4 nopadding"><div class="form-group m-b-40"><input name="website_name[]" type="text" class="form-control" value="" required><span class="bar"></span><label>'+website_name+'</label></div></div><div class="col-sm-7 nopadding"><div class="form-group m-b-40"><input name="website_url[]" type="url" class="form-control" value="" required><span class="bar"></span><label>'+website_url+'</label></div></div><div class="col-sm-1 nopadding"><div class="text-right"><button class="btn btn-danger" type="button" onclick="remove_website_field(' + website + ');"> <i class="fa fa-minus"></i> '+remove+'</button></div></div>';

            objTo.appendChild(divtest)

            $('.floating-labels .form-control').on('focus blur', function (e) {
                $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
            }).trigger('blur');
        }

        function remove_website_field(rid) {
            $('.removewebsite' + rid).remove();
        }

        <?php 
        $work=1;
        @$work = count(json_decode($information->work));
        ?>
        var work = <?php echo $work ?>;
        var work_company = "{{__('user.formwork_company')}}";
        var work_position = "{{__('user.formwork_position')}}";
        var work_start = "{{__('user.formwork_start')}}";
        var work_end = "{{__('user.formwork_end')}}";
        var remove = "{{__('user.form_remove')}}";
        function work_field() {

            work++;
            var objTo = document.getElementById('work_field')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "row removework" + work);
            var rdiv = 'removework' + work;
            divtest.innerHTML = '<div class="col-sm-2 nopadding"><div class="form-group m-b-40"><input name="work_company[]" type="text" class="form-control" value=""><span class="bar"></span><label>'+work_company+'</label></div></div><div class="col-sm-3 nopadding"><div class="form-group m-b-40"><input name="work_position[]" type="text" class="form-control" value=""><span class="bar"></span><label>'+work_position+'</label></div></div><div class="col-sm-3 nopadding"><div class="form-group m-b-40"><input name="work_start[]"  type="text" class="form-control date-format"><span class="bar"></span><label>'+work_start+'</label></div></div><div class="col-sm-3 nopadding"><div class="form-group m-b-40"><input name="work_end[]"  type="text" class="form-control date-format"><span class="bar"></span><label>'+work_end+'</label></div></div>'+
            '<div class="col-sm-1 nopadding"><div class="text-right"><button class="btn btn-danger" type="button" onclick="remove_work_field(' + work + ');"> <i class="fa fa-minus"></i> '+remove+'</button></div></div>';

            objTo.appendChild(divtest)

            $('.date-format').bootstrapMaterialDatePicker({ format: 'dddd, DD MMMM YYYY', time: false });
            $('.floating-labels .form-control').on('focus blur', function (e) {
                $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
            }).trigger('blur');
        }

        function remove_work_field(rid) {
            $('.removework' + rid).remove();
        }

    </script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
			<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{$title}}</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <?php if ($myprofile || $admin): ?>
                                    <li class="breadcrumb-item active">{{$title}}</li>
                                <?php else: ?>
                                    <li class="breadcrumb-item">{{__('menu.System Support')}}</li>
                                    <li class="breadcrumb-item">{{__('menu.User Management')}}</li>
                                    <li class="breadcrumb-item active">{{$title}}</li>
                                <?php endif ?>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            	<div class="card-header bg-info">
                    <h4 class="m-b-0 text-white">{{$title}}</h4>
                    <h6 class="m-b-0 text-white">{{ session()->get('group_id')==1 ? __('user.formCaption') : __('user.formCaptionProfile') }}</h6>
                </div>
                <div class="card-body">

                    <form class="floating-labels m-t-40" enctype="multipart/form-data" method="post" action="<?php echo url($urlModule.'/save/'.$id) ?>">
                    	@csrf

                        <div class="form-group m-b-5" style=" text-align: center; width: 200px; margin-bottom: 40px; margin-left: 40%; margin-right: 40%;">
                            <label for="input-file-now card-title" style="display: contents">{{__('user.formPic')}}</label>
                            <?php 
                            $avatar='';
                            $imgdefault=url('/template/eliteadmin/assets/images/users/1.jpg');
                            $img=url("/file/images/users/".@$information->avatar);
                            $avatar = @$information->avatar=='' ? "data-default-file='$imgdefault'" : "data-default-file='$img'";
                             ?>
                            <input data-allowed-file-extensions='["jpg", "png", "jpeg"]' type="file" name="avatar" id="input-file-now" class="dropify" data-height="200" <?php echo $avatar ?> />
                        </div>

                        <div class="form-group m-b-40">
                            <input name="first_name" type="text" class="form-control" value="<?php echo @$information->first_name ?>" required>
                            <span class="bar"></span>
                            <label>{{__('user.formFirstname')}}</label>
                        </div>
                        <div class="form-group m-b-40">
                            <input name="last_name" type="text" class="form-control" value="<?php echo @$information->last_name ?>" required>
                            <span class="bar"></span>
                            <label>{{__('user.formLastname')}}</label>
                        </div>
                        <div class="form-group m-b-40">
                            <input name="username" type="text" class="form-control" value="<?php echo @$information->username ?>" required>
                            <span class="bar"></span>
                            <label>{{__('user.formUsername')}}</label>
                        </div>
                        <div class="form-group m-b-40">
                            <input name="phone" type="text" class="form-control" value="<?php echo @$information->phone ?>" required>
                            <span class="bar"></span>
                            <label>{{__('user.formNoHP')}}</label>
                        </div>
                        <div class="form-group m-b-40">
                            <input name="email" type="email" class="form-control" value="<?php echo @$information->email ?>" required>
                            <span class="bar"></span>
                            <label>{{__('user.formEmail')}}</label>
                        </div>
                        <div class="form-group m-b-40">
                            <input name="password" type="password" class="form-control" <?php echo isset($information->password) ? '' : 'required' ?>>
                            <span class="bar"></span>
                            <label>{{__('user.formPassword')}}</label>
                            <?php if (isset($information->password)): ?>
                                <span class="help-block"><small>{{__('user.Fill if want to change')}}</small></span>
                            <?php endif ?>
                        </div>

                        <div class="form-group m-b-40">
                            <label for="input1">{{__('user.formLang')}}</label>
                            <select  name="lang" class="select2 form-control btn-secondary" style="width: 100%; height:36px;">
                                <optgroup label="{{__('user.formLang')}}">
                                    <?php 
                                    $item = array('en','id');
                                     ?>
                                     <?php foreach ($item as $key): ?>
                                        <option value="<?php echo $key ?>" <?php echo @$information->lang==$key ? 'selected' : '' ?>><?php echo __('user.'.$key) ?></option>
                                     <?php endforeach ?>
                                </optgroup>
                            </select>
                        </div>

                        <?php if (!$myprofile || @$information->group_id==1): ?>
                        <?php if (@$information->user_id==$id && @$information->group_id!=1): ?>
                            <div class="form-group m-b-40">
                                <label for="input1">{{__('user.formGroup')}}</label>
                                <select  name="group_id" class="select2 form-control btn-secondary" style="width: 100%; height:36px;">
                                    <optgroup label="{{__('user.formGroup')}}">
                                         <?php foreach ($groups as $key): ?>
                                            <option value="<?php echo $key->group_id ?>" <?php echo @$information->group_id==$key->group_id ? 'selected' : '' ?>><?php echo $key->name ?></option>
                                         <?php endforeach ?>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="form-group m-b-40">
                                <label for="input1">{{__('user.formStatus')}}</label>
                                <select  name="active" class="select2 form-control btn-secondary" style="width: 100%; height:36px;">
                                    <optgroup label="{{__('menu.menuType')}}">
                                        <?php 
                                        $item = array(0,1);
                                         ?>
                                         <?php foreach ($item as $key): ?>
                                            <option value="<?php echo $key ?>" <?php echo @$information->active==$key ? 'selected' : '' ?>><?php echo __('user.status_'.$key) ?></option>
                                         <?php endforeach ?>
                                    </optgroup>
                                </select>
                            </div>
                        <?php endif ?>
                        <?php endif ?>

                        <div class="card m-b-40">
                            <div>
                                <br>
                                <h4 class="card-title">{{__('user.additional')}}</h4>
                                <hr><br>
                                <div class="form-group m-b-40">
                                    <input name="intro" type="text" class="form-control" value="<?php echo @$information->intro ?>" >
                                    <span class="bar"></span>
                                    <label>{{__('user.formintro')}}</label>
                                </div>
                                <div class="form-group m-b-40">
                                    <input name="address" type="text" class="form-control" value="<?php echo @$information->address ?>" >
                                    <span class="bar"></span>
                                    <label>{{__('user.formaddress')}}</label>
                                </div>
                                <div class="form-group m-b-40">
                                    <label>{{__('user.formbirthdate')}}</label>
                                    <input name="birthdate" type="text" class="form-control date-format" value="<?php echo @$information->birthdate ?>">
                                </div>
                                <div class="form-group m-b-5">
                                    <textarea name="about" class="form-control" rows="4"><?php echo @$information->about ?></textarea>
                                    <span class="bar"></span>
                                    <label>{{__('user.formabout')}}</label>
                                </div><br>
                                <div class="form-group m-b-40">
                                    <h6 style="padding-top: 10px; font-weight: 400;" for="input1">{{__('user.formprofessional_skills')}}</h6>
                                    <input name="professional_skills" type="text" data-role="tagsinput" class="form-control" value="<?php echo @$information->professional_skills ?>"  style="width: 100%; height:36px;"  >
                                </div>
                                <div class="form-group m-b-40">
                                    <label>{{__('user.formcountry')}}</label>
                                    <select id="countries" name="country" class="form-control btn-secondary" style="width: 100%; height:36px;">
                                        <optgroup label="{{__('user.formcountry')}}">
                                             <?php foreach ($country as $key): ?>
                                                <option value="<?php echo $key['country_code'] ?>" <?php echo @$information->country==$key['country_code'] ? 'selected' : '' ?>><?php echo $key['country_name'] ?></option>
                                             <?php endforeach ?>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="form-group m-b-40">
                                    <label>{{__('user.formgender')}}</label>
                                    <select  name="gender" class="select2 form-control btn-secondary" style="width: 100%; height:36px;">
                                        <optgroup label="{{__('user.formgender')}}">
                                             <?php 
                                             $item = array('m','f','no');
                                             ?>
                                             <?php foreach ($item as $key): ?>
                                                <option value="<?php echo $key ?>" <?php echo @$information->gender==$key ? 'selected' : '' ?>><?php echo __('user.gender_'.$key) ?></option>
                                             <?php endforeach ?>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="form-group m-b-40">
                                    <label>{{__('user.formreligion')}}</label>
                                    <select  name="religion" class="select2 form-control btn-secondary" style="width: 100%; height:36px;">
                                        <optgroup label="{{__('user.formreligion')}}">
                                             <?php 
                                             $item = array('isl','chr','hin','sec','bud','chi','eth','afr','sik','spi','jud','bah','jai','shi','cao','zor','ten','ani','neo','uni','ras');
                                             ?>
                                             <?php foreach ($item as $key): ?>
                                                <option value="<?php echo $key ?>" <?php echo @$information->religion==$key ? 'selected' : '' ?>><?php echo __('user.religion_'.$key) ?></option>
                                             <?php endforeach ?>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="card">
                                    <h4 class="card-title">{{__('user.formschool')}}</h4>
                                    <br>
                                    <?php @$school = json_decode($information->school); if ($school!=null): ?>
                                    <?php foreach ($school as $key => $value): ?> 
                                    <div class="row <?php echo $key>0 ? "removeschool".($key+1) : "" ?>">
                                        <div class="col-sm-4 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="school_name[]" type="text" class="form-control" value="<?php echo $value->name ?>">
                                                <span class="bar"></span>
                                                <label>{{__('user.formschool_name')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-7 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="school_loc[]" type="url" class="form-control" value="<?php echo $value->loc ?>">
                                                <span class="bar"></span>
                                                <label>{{__('user.formschool_loc')}}</label>
                                            </div>
                                        </div>
                                        <?php if ($key == 0): ?>
                                        <div class="col-sm-1 nopadding">
                                            <div class="text-right">
                                                <button class="btn btn-success" type="button" onclick="school_field();"><i class="fa fa-plus"></i> {{__('user.form_addmore')}}</button>
                                            </div>
                                        </div>
                                        <?php else: ?>  
                                        <div class="col-sm-1 nopadding">
                                            <div class="text-right">
                                                <button class="btn btn-danger" type="button" onclick="remove_school_field(<?php echo $key+1 ?>);"> <i class="fa fa-minus" aria-hidden="true"></i> {{__('user.form_remove')}}</button>
                                            </div>
                                        </div>
                                        <?php endif ?>
                                    </div>
                                    <?php endforeach ?>
                                    <?php else: ?>
                                    <div class="row">
                                        <div class="col-sm-4 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="school_name[]" type="text" class="form-control" value="">
                                                <span class="bar"></span>
                                                <label>{{__('user.formschool_name')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-7 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="school_loc[]" type="url" class="form-control" value="">
                                                <span class="bar"></span>
                                                <label>{{__('user.formschool_loc')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1 nopadding">
                                            <div class="text-right">
                                                <button class="btn btn-success" type="button" onclick="school_field();"><i class="fa fa-plus"></i> {{__('user.form_addmore')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif ?>
                                    <div id="school_field"></div>
                                </div>
                                <div class="card">
                                    <h4 class="card-title">{{__('user.formwebsite')}}</h4>
                                    <br>
                                    <?php @$website = json_decode($information->website); if ($website!=null): ?>
                                    <?php foreach ($website as $key => $value): ?> 
                                    <div class="row <?php echo $key>0 ? "removewebsite".($key+1) : "" ?>">
                                        <div class="col-sm-4 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="website_name[]" type="text" class="form-control" value="<?php echo $value->name ?>">
                                                <span class="bar"></span>
                                                <label>{{__('user.formwebsite_name')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-7 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="website_url[]" type="url" class="form-control" value="<?php echo $value->url ?>">
                                                <span class="bar"></span>
                                                <label>{{__('user.formwebsite_url')}}</label>
                                            </div>
                                        </div>
                                        <?php if ($key == 0): ?>
                                        <div class="col-sm-1 nopadding">
                                            <div class="text-right">
                                                <button class="btn btn-success" type="button" onclick="website_field();"><i class="fa fa-plus"></i> {{__('user.form_addmore')}}</button>
                                            </div>
                                        </div>
                                        <?php else: ?>  
                                        <div class="col-sm-1 nopadding">
                                            <div class="text-right">
                                                <button class="btn btn-danger" type="button" onclick="remove_website_field(<?php echo $key+1 ?>);"> <i class="fa fa-minus" aria-hidden="true"></i> {{__('user.form_remove')}}</button>
                                            </div>
                                        </div>
                                        <?php endif ?>
                                    </div>
                                    <?php endforeach ?>
                                    <?php else: ?>
                                    <div class="row">
                                        <div class="col-sm-4 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="website_name[]" type="text" class="form-control" value="">
                                                <span class="bar"></span>
                                                <label>{{__('user.formwebsite_name')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-7 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="website_url[]" type="url" class="form-control" value="">
                                                <span class="bar"></span>
                                                <label>{{__('user.formwebsite_url')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1 nopadding">
                                            <div class="text-right">
                                                <button class="btn btn-success" type="button" onclick="website_field();"><i class="fa fa-plus"></i> {{__('user.form_addmore')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif ?>
                                    <div id="website_field"></div>
                                </div>
                                <div class="card">
                                    <h4 class="card-title">{{__('user.formwork')}}</h4>
                                    <br>
                                    <?php @$work = json_decode($information->work); if ($work!=null): ?>
                                    <?php foreach ($work as $key => $value): ?> 
                                    <div class="row <?php echo $key>0 ? "removework".($key+1) : "" ?>">
                                        <div class="col-sm-2 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="work_company[]" type="text" class="form-control" value="<?php echo $value->company ?>">
                                                <span class="bar"></span>
                                                <label>{{__('user.formwork_company')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="work_position[]" type="text" class="form-control" value="<?php echo $value->position ?>">
                                                <span class="bar"></span>
                                                <label>{{__('user.formwork_position')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="work_start[]" type="text" class="form-control date-format" value="<?php echo $value->start ?>">
                                                <span class="bar"></span>
                                                <label>{{__('user.formwork_start')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="work_end[]" type="text" class="form-control date-format" value="<?php echo $value->end ?>">
                                                <span class="bar"></span>
                                                <label>{{__('user.formwork_end')}}</label>
                                            </div>
                                        </div>
                                        <?php if ($key == 0): ?>
                                        <div class="col-sm-1 nopadding">
                                            <div class="text-right">
                                                <button class="btn btn-success" type="button" onclick="work_field();"><i class="fa fa-plus"></i> {{__('user.form_addmore')}}</button>
                                            </div>
                                        </div>
                                        <?php else: ?>  
                                        <div class="col-sm-1 nopadding">
                                            <div class="text-right">
                                                <button class="btn btn-danger" type="button" onclick="remove_work_field(<?php echo $key+1 ?>);"> <i class="fa fa-minus" aria-hidden="true"></i> {{__('user.form_remove')}}</button>
                                            </div>
                                        </div>
                                        <?php endif ?>
                                    </div>
                                    <?php endforeach ?>
                                    <?php else: ?>
                                    <div class="row">
                                        <div class="col-sm-2 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="work_company[]" type="text" class="form-control" value="">
                                                <span class="bar"></span>
                                                <label>{{__('user.formwork_company')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="work_position[]" type="text" class="form-control" value="">
                                                <span class="bar"></span>
                                                <label>{{__('user.formwork_position')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="work_start[]"  type="text" class="form-control date-format">
                                                <span class="bar"></span>
                                                <label>{{__('user.formwork_start')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 nopadding">
                                            <div class="form-group m-b-40">
                                                <input name="work_end[]"  type="text" class="form-control date-format">
                                                <span class="bar"></span>
                                                <label>{{__('user.formwork_end')}}</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1 nopadding">
                                            <div class="text-right">
                                                <button class="btn btn-success" type="button" onclick="work_field();"><i class="fa fa-plus"></i> {{__('user.form_addmore')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif ?>
                                    <div id="work_field"></div>
                                </div>

                            </div>
                        </div>


                        <br>
				        <center>
				        	<div class="text-xs-right">
					            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> {{__('dashboard.Save')}}</button>
					            <button type="reset" class="btn btn-inverse">{{__('dashboard.Cancel')}}</button>
					        </div>
				        </center>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection