@extends('layout_backend')

@section('title', $title)

@section('custom_css')
	<!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/pages/floating-label.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/dropify/dist/css/dropify.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/dropify/dist/js/dropify.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/moment/moment.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bfu5a6m8isttm4xidrncl37jdkvwzcia49d8nphrqy3t3j5f"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.<?php echo $urlModule ?>').addClass('active');
            $('.dropify').dropify();
            $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });
        });

        var editor_config = {
		    path_absolute : "/",
		    selector: '#editor',
		    theme: 'modern',
		    height: 300,
		    content_css: 'https://on.akasah.id/template/cooming_soon/master/style.css',
		    plugins: [
		      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
		      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
		      'save table contextmenu directionality emoticons template paste textcolor'
		    ],
		    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
		    relative_urls: false,
		    file_browser_callback : function(field_name, url, type, win) {
		      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
		      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

		      var cmsURL = editor_config.path_absolute + 'filemanager?field_name=' + field_name;
		      if (type == 'image') {
		        cmsURL = cmsURL + "&type=Images";
		      } else {
		        cmsURL = cmsURL + "&type=Files";
		      }

		      tinyMCE.activeEditor.windowManager.open({
		        file : cmsURL,
		        title : 'Filemanager',
		        width : x * 0.8,
		        height : y * 0.8,
		        resizable : "yes",
		        close_previous : "no"
		      });
		    }
		};

		tinymce.init(editor_config);

    </script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
			<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{$title}}</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <li class="breadcrumb-item">{{__("menu.$title_module")}}</li>
                                <li class="breadcrumb-item active">{{ $title }}</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            	<div class="card-header bg-info">
                    <h4 class="m-b-0 text-white">{{$title}}</h4>
                    <h6 class="m-b-0 text-white">{{__("$urlModule.formCaption")}}</h6>
                </div>
                <div class="card-body">

                    <form class="floating-labels m-t-40"  enctype="multipart/form-data" method="post" action="<?php echo url($urlModule.'/save/'.$id) ?>">
                    	@csrf
                        <div class="form-group m-b-40">
                            <input name="event_title" type="text" class="form-control" value="<?php echo @$information->event_title ?>" required>
                            <span class="bar"></span>
                            <label>{{__('event.formevent_title')}}</label>
                        </div>
                        
                        <div class="form-group m-b-40">
                            <input name="event_caption" type="text" class="form-control" value="<?php echo @$information->event_caption ?>" required>
                            <span class="bar"></span>
                            <label>{{__('event.formevent_caption')}}</label>
                        </div>
                        
                        <div class="row">
                        	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        		<div class="form-group m-b-5" style=" text-align: center; width: 200px; margin-bottom: 40px; margin-left: 40%; margin-right: 40%;">
		                            <label for="input-file-now card-title" style="display: contents">{{__('event.formevent_image')}}</label>
		                            <?php 
		                            $event_image='';
		                            $imgdefault=url('/template/eliteadmin/assets/images/users/1.jpg');
		                            $img=url("/file/images/event/".@$information->event_image);
		                            $event_image = @$information->event_image=='' ? "data-default-file='$imgdefault' required" : "data-default-file='$img'";
		                             ?>
		                            <input data-allowed-file-extensions='["jpg", "png", "jpeg"]' type="file" name="event_image" id="input-file-now" class="dropify" data-height="200" <?php echo $event_image ?>/>
		                        </div>
                        	</div>
                        	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        		<div class="form-group m-b-5" style=" text-align: center; width: 200px; margin-bottom: 40px; margin-left: 40%; margin-right: 40%;">
		                            <label for="input-file-now card-title" style="display: contents">{{__('event.formevent_poster')}}</label>
		                            <?php 
		                            $event_poster='';
		                            $imgdefault=url('/template/eliteadmin/assets/images/users/1.jpg');
		                            $img=url("/file/images/event/".@$information->event_poster);
		                            $event_poster = @$information->event_poster=='' ? "data-default-file='$imgdefault' required" : "data-default-file='$img'";
		                             ?>
		                            <input data-allowed-file-extensions='["jpg", "png", "jpeg"]' type="file" name="event_poster" id="input-file-now" class="dropify" data-height="200" <?php echo $event_poster ?>/>
		                        </div>
                        	</div>
                        </div>

                        <div class="form-group m-b-40">
                        	<br><br>
                            <label>{{__('event.formevent_desc')}}</label>
                            <span class="bar"></span>
                            <textarea name="event_desc" id="editor" required><?php echo @$information->event_desc ?>
                            </textarea>
                        </div>
                        
                        <div class="form-group m-b-40">
                            <input name="event_datetime" type="text" id="date-format" class="form-control" value="<?php echo @$event_datetime ?>" required>
                            <span class="bar"></span>
                            <label>{{__('event.formevent_datetime')}}</label>
                        </div>
                        
                        <div class="form-group m-b-40">
                            <input name="event_participant_target" type="number" class="form-control" value="<?php echo @$information->event_participant_target ?>" required>
                            <span class="bar"></span>
                            <label>{{__('event.formevent_participant_target')}}</label>
                        </div>
                        
                        <div class="form-group m-b-40">
                            <input name="event_gmap_url" type="text" class="form-control" value="<?php echo @$information->event_gmap_url ?>" required>
                            <span class="bar"></span>
                            <label>{{__('event.formevent_gmap_url')}}</label>
                        </div>
                        
                        <div class="form-group m-b-40">
                            <input name="event_gmap_lat" type="text" class="form-control" value="<?php echo @$information->event_gmap_lat ?>" required>
                            <span class="bar"></span>
                            <label>{{__('event.formevent_gmap_lat')}}</label>
                        </div>
                        
                        <div class="form-group m-b-40">
                            <input name="event_gmap_lng" type="text" class="form-control" value="<?php echo @$information->event_gmap_lng ?>" required>
                            <span class="bar"></span>
                            <label>{{__('event.formevent_gmap_lng')}}</label>
                        </div>
                        
                        <div class="form-group m-b-40">
                            <input name="event_city" type="text" class="form-control" value="<?php echo @$information->event_city ?>" required>
                            <span class="bar"></span>
                            <label>{{__('event.formevent_city')}}</label>
                        </div>
                        
                        <div class="form-group m-b-40">
                            <input name="event_state" type="text" class="form-control" value="<?php echo @$information->event_state ?>" required>
                            <span class="bar"></span>
                            <label>{{__('event.formevent_state')}}</label>
                        </div>
                        
                        <div class="form-group m-b-40">
                            <input name="event_country" type="text" class="form-control" value="<?php echo @$information->event_country ?>" required>
                            <span class="bar"></span>
                            <label>{{__('event.formevent_country')}}</label>
                        </div>
                        
                        <div class="form-group m-b-40">
                            <textarea name="event_location" class="form-control" rows="3" required><?php echo @$information->event_location ?></textarea>
                            <span class="bar"></span>
                            <label>{{__('event.formevent_location')}}</label>
                        </div>

                        <br>
				        <center>
				        	<div class="text-xs-right">
					            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> {{__('dashboard.Save')}}</button>
					            <button type="reset" class="btn btn-inverse">{{__('dashboard.Cancel')}}</button>
					        </div>
				        </center>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection