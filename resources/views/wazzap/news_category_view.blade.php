@extends('layout_backend')

@section('title', $title)

@section('custom_css')
    <!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/pages/floating-label.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/dropify/dist/css/dropify.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/multiselect/css/multi-select.css" rel="stylesheet">
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/dropify/dist/js/dropify.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.news').addClass('active');
            $('.<?php echo $urlModule ?>').addClass('active');
            $('.dropify').dropify();
            $('.select2').select2();
        });

    </script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
			<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{$title}}</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <li class="breadcrumb-item">{{__('menu.News Management')}}</li>
                                <li class="breadcrumb-item">{{__("menu.$title_module")}}</li>
                                <li class="breadcrumb-item active">{{ $title }}</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            	<div class="card-header bg-info">
                    <h4 class="m-b-0 text-white">{{$title}}</h4>
                    <h6 class="m-b-0 text-white">{{__("$urlModule.formCaption")}}</h6>
                </div>
                <div class="card-body">

                    <form class="floating-labels m-t-40" enctype="multipart/form-data" method="post" action="<?php echo url($urlModule.'/save/'.$id) ?>">
                    	@csrf

                        <div class="form-group m-b-5" style=" text-align: center; width: 200px; margin-bottom: 40px; margin-left: 40%; margin-right: 40%;">
                            <label for="input-file-now card-title" style="display: contents">{{__('news_category.formcategory_image')}}</label>
                            <?php 
                            $category_image='';
                            $imgdefault=url('/template/eliteadmin/assets/images/users/1.jpg');
                            $img=url("/file/images/news_category/".@$information->category_image);
                            $category_image = @$information->category_image=='' ? "data-default-file='$imgdefault'" : "data-default-file='$img'";
                             ?>
                            <input type="file" data-allowed-file-extensions='["jpg", "png", "jpeg"]' name="category_image" id="input-file-now" class="dropify" data-height="200" <?php echo $category_image ?> />
                        </div>

                        <div class="form-group m-b-40">
                            <input name="category_name" type="text" class="form-control" value="<?php echo @$information->category_name ?>" required>
                            <span class="bar"></span>
                            <label>{{__('news_category.formcategory_name')}}</label>
                        </div>
                        
                        <div class="form-group m-b-40">
                            <input name="category_hashtag" type="text" class="form-control" value="<?php echo @$information->category_hashtag ?>" required>
                            <span class="bar"></span>
                            <label>{{__('news_category.formcategory_hashtag')}}</label>
                        </div>
                        
                        <div class="form-group m-b-5">
                            <textarea name="category_desc" class="form-control" rows="4"><?php echo @$information->category_desc ?></textarea>
                            <span class="bar"></span>
                            <label for="input7">{{__('news_category.formcategory_desc')}}</label>
                        </div>
                        
                        
                        <div class="form-group m-b-40">
                            <h6 style="padding-top: 10px; font-weight: 400;" for="input1">{{__('news_category.formcategory_related')}}</h6>
                            <select  name="category_related[]" class="select2 form-control btn-secondary" style="width: 100%; height:36px;" multiple="multiple">
                                <?php foreach ($category_related as $key => $value): ?>
                                    <option 
                                    <?php 
                                    if (@$information->category_related!=null) {
                                        if (in_array($value->news_category_id, json_decode($information->category_related))) {
                                            echo "selected";
                                        }
                                    }
                                     ?>
                                    value="<?php echo $value->news_category_id ?>"><?php echo $value->category_name ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>

                        <br>
				        <center>
				        	<div class="text-xs-right">
					            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> {{__('dashboard.Save')}}</button>
					            <button type="reset" class="btn btn-inverse">{{__('dashboard.Cancel')}}</button>
					        </div>
				        </center>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection