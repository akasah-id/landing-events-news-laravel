@extends('layout_backend')

@section('title', $title)

@section('custom_css')
    <!-- Page CSS -->
    <link href="<?php echo url('/') ?>/template/eliteadmin/template/dist/css/pages/floating-label.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/dropify/dist/css/dropify.min.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/multiselect/css/multi-select.css" rel="stylesheet">
    <link href="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/wizard/steps.css" rel="stylesheet">
    <style type="text/css">
        .error-right {
            right: 0;
        }

        .wizard-content .wizard>.actions {
            position: relative;
            display: block;
            text-align: right;
            padding: 0 20px 20px;
            padding-top: 30px;
        }
    </style>
@endsection
@section('custom_js')
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/dropify/dist/js/dropify.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/multiselect/js/jquery.multi-select.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/wizard/jquery.steps.min.js"></script>
    <script src="<?php echo url('/') ?>/template/eliteadmin/assets/node_modules/wizard/jquery.validate.min.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bfu5a6m8isttm4xidrncl37jdkvwzcia49d8nphrqy3t3j5f"></script>

    <script type="text/javascript">
        var editor_config = {
            path_absolute : "/",
            selector: '#editor',
            theme: 'modern',
            height: 300,
            content_css: 'https://on.akasah.id/template/cooming_soon/master/style.css',
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
              var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
              var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

              var cmsURL = editor_config.path_absolute + 'filemanager?field_name=' + field_name;
              if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
              } else {
                cmsURL = cmsURL + "&type=Files";
              }

              tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
              });
            }
        };

        tinymce.init(editor_config);

        var form = $(".validation-wizard").show();

        $(".validation-wizard").steps({
            headerTag: "h6"
            , bodyTag: "section"
            , transitionEffect: "fade"
            , titleTemplate: '<span class="step">#index#</span> #title#'
            , labels: {
                finish: "{{__('dashboard.Save')}}",
                previous: "{{__('news.Previous')}}",
                next: "{{__('news.Next')}}",
            }
            , onStepChanging: function (event, currentIndex, newIndex) {
                return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
            }
            , onFinishing: function (event, currentIndex) {
                return form.validate().settings.ignore = ":disabled", form.valid()
            }
            , onFinished: function (event, currentIndex) {
                 // swal("{{__('news.News Saved!')}}", "{{__('news.ThanksNote')}}");
                 $('#news_form').submit();

            }
        }), $(".validation-wizard").validate({
            ignore: "input[type=hidden]"
            , errorClass: "text-danger error-right"
            , successClass: "text-success error-right"
            , highlight: function (element, errorClass) {
                $(element).removeClass(errorClass)
            }
            , unhighlight: function (element, errorClass) {
                $(element).removeClass(errorClass)
            }
            , errorPlacement: function (error, element) {
                error.insertAfter(element)
            }
            , rules: {
                email: {
                    email: !0
                }
            }
        });

        $(document).ready(function () {
            $('.news').addClass('active');
            $('.<?php echo $urlModule ?>').addClass('active');
            $('.dropify').dropify();
            $('.select2').select2();
        });
    </script>
    <!-- END THIS PAGE SCRIPTS -->
@endsection

@section('breadcrumbs')
			<!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">{{$title}}</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo url('/') ?>">{{__('dashboard.Home')}}</a></li>
                                <li class="breadcrumb-item">{{__("news.$title_module")}}</li>
                                <li class="breadcrumb-item active">{{ $title }}</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">

        <div class="card wizard-content">
        	<div class="card-header bg-info">
                <h4 class="m-b-0 text-white">{{$title}}</h4>
                <h6 class="m-b-0 text-white">{{__("$urlModule.formCaption")}}</h6>
            </div>
            <div class="card-body">
                <form id="news_form" class="floating-labels m-t-40 validation-wizard wizard-circle" enctype="multipart/form-data" method="post" action="<?php echo url($urlModule.'/save/'.$id) ?>">
                    <!-- <form action="#" class="validation-wizard wizard-circle"> -->
                    @csrf
                    <!-- Step 1 -->
                    <h6>{{__('news.Category & Meta')}}</h6>
                    <section>
                        <div class="form-group m-b-40">
                            <h6 style="padding-top: 10px; font-weight: 400;" for="input1">{{__('news.formnews_category')}}</h6>
                            <select  name="news_category_id[]" class="select2 form-control btn-secondary" style="width: 100%; height:36px;" multiple="multiple" required>
                                <?php foreach ($news_category as $key => $value): ?>
                                    <option 
                                    <?php 
                                    if (isset($information->news_category_id)) {
                                        if (in_array($value->news_category_id, json_decode($information->news_category_id))) {
                                            echo "selected";
                                        }
                                    }
                                     ?>
                                    value="<?php echo $value->news_category_id ?>"><?php echo $value->category_name ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>

                        <div class="form-group m-b-5" style=" text-align: center; width: 200px; margin-bottom: 40px; margin-left: 40%; margin-right: 40%;">
                            <label for="input-file-now card-title" style="display: contents">{{__('news.formnews_metaimage')}} (960x540)</label>
                            <?php 
                            $news_metaimage='';
                            $imgdefault=url('/template/eliteadmin/assets/images/users/1.jpg');
                            $img=url("/file/images/news/".@$information->news_metaimage);
                            $news_metaimage = @$information->news_metaimage=='' ? "data-default-file='$imgdefault'" : "data-default-file='$img'";
                             ?>
                            <input data-allowed-file-extensions='["jpg", "png", "jpeg"]' type="file" name="news_metaimage" id="input-file-now" class="dropify" data-height="200" <?php echo $news_metaimage ?> />
                        </div>

                        <div class="form-group m-b-40">
                            <input name="news_metakey" type="text" class="form-control" value="<?php echo @$information->news_metakey ?>" required>
                            <span class="bar"></span>
                            <label>{{__('news.formnews_metakey')}}</label>
                        </div>

                        <div class="form-group m-b-5">
                            <textarea name="news_metadesc" class="form-control" rows="4" required><?php echo @$information->news_metadesc ?></textarea>
                            <span class="bar"></span>
                            <label for="input7">{{__('news.formnews_metadesc')}}</label>
                        </div>
                    </section>
                    <!-- Step 2 -->
                    <h6>{{__('news.Headlines')}}</h6>
                    <section>
                        <br>
                        <div class="form-group m-b-40">
                            <input name="news_title" type="text" class="form-control" value="<?php echo @$information->news_title ?>" required>
                            <span class="bar"></span>
                            <label>{{__('news.formnews_title')}}</label>
                        </div>
                        <div class="form-group m-b-40">
                            <input name="news_subtitle" type="text" class="form-control" value="<?php echo @$information->news_subtitle ?>" required>
                            <span class="bar"></span>
                            <label>{{__('news.formnews_subtitle')}}</label>
                        </div>
                        <div class="form-group m-b-40">
                            <input name="news_voicetext" type="text" class="form-control" value="<?php echo @$information->news_voicetext ?>">
                            <span class="bar"></span>
                            <label>Tanggal Acara (ex: Jum'at, 4 Februari 2022)</label>
                        </div>

                        <div class="form-group m-b-5" style=" text-align: left; width: 100%; margin-bottom: 40px;">
                            <label for="input-file-now card-title" style="display: contents">{{__('news.formnews_voice')}} (.wav .mp3 .aac .m4a)</label>
                            <?php 
                            $img=url("/file/sounds/news/".@$information->news_voice);
                            $news_voice = @$information->news_voice=='' ? "" : "data-default-file='$img'";
                             ?>
                            <input type="file" name="news_voice" id="input-file-now" class="dropify" data-allowed-file-extensions='["wav", "mp3", "aac", "m4a"]' data-height="70" <?php echo $news_voice ?> />
                        </div>

                        <div class="form-group m-b-40">
                            <label for="input1">{{__('news.formnews_status')}}</label>
                            <select  name="news_status" class="select2 form-control btn-secondary" style="width: 100%; height:36px;">
                                <optgroup label="{{__('news.formnews_status')}}">
                                    <?php 
                                    $item = array('Active','Inactive');
                                     ?>
                                     <?php foreach ($item as $key): ?>
                                        <option value="<?php echo $key ?>" <?php echo @$information->news_status==$key ? 'selected' : '' ?>><?php echo $key ?></option>
                                     <?php endforeach ?>
                                </optgroup>
                            </select>
                        </div>
                    </section>
                    <!-- Step 3 -->
                    <h6>{{__('news.Contents')}}</h6>
                    <section>
                        <br>
                        <div class="form-group m-b-40">
                            <label for="input1">{{__('news.formnews_layout')}}</label>
                            <br><br>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" value="landscapefull" id="customRadio1" name="news_layout" class="custom-control-input" <?php echo @$information->news_layout=='' ? "checked" : "" ?> <?php echo @$information->news_layout=='landscapefull' ? "checked" : "" ?>>
                                        <label class="custom-control-label" style="padding-left: 30px;" for="customRadio1">{{__('news.formnews_landscapefull')}}</label>
                                    </div>
                                    <br>
                                    <img style="padding-left: 30px;" src="<?php echo url('/') ?>/template/eliteadmin/assets/images/news_layout/mockup-landscapefull.jpg" height="300">
                                </div>
                                <!-- <div class="col-sm-4">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" value="landscapehalf" id="customRadio2" name="news_layout" class="custom-control-input" <?php echo @$information->news_layout=='landscapehalf' ? "checked" : "" ?>>
                                        <label class="custom-control-label" style="padding-left: 30px;" for="customRadio2">{{__('news.formnews_landscapehalf')}}</label>
                                    </div>
                                    <br>
                                    <img style="padding-left: 30px;" src="<?php echo url('/') ?>/template/eliteadmin/assets/images/news_layout/mockup-landscapehalf.jpg" height="300">
                                </div>
                                <div class="col-sm-4">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" value="potrait" id="customRadio3" name="news_layout" class="custom-control-input" <?php echo @$information->news_layout=='potrait' ? "checked" : "" ?>>
                                        <label class="custom-control-label" style="padding-left: 30px;" for="customRadio3">{{__('news.formnews_potrait')}}</label>
                                    </div>
                                    <br>
                                    <img style="padding-left: 30px;" src="<?php echo url('/') ?>/template/eliteadmin/assets/images/news_layout/mockup-potrait.jpg" height="300">
                                </div> -->
                            </div>
                        </div>
                        <hr>
                        <div class="form-group m-b-5 news_layout_div" style=" text-align: center; width: 200px; margin-bottom: 40px; margin-left: 40%; margin-right: 40%;">
                            <label for="input-file-now card-title" style="display: contents">{{__('news.formnews_image')}} (1920x250)</label>
                            <?php 
                            $news_image='';
                            $imgdefault=url('/template/eliteadmin/assets/images/users/1.jpg');
                            $img=url("/file/images/news/".@$information->news_image);
                            $news_image = @$information->news_image=='' ? "data-default-file='$imgdefault'" : "data-default-file='$img'";
                             ?>
                            <input type="file" data-allowed-file-extensions='["jpg", "png", "jpeg"]' name="news_image" id="input-file-now" class="dropify news_layout_file" data-height="200" <?php echo $news_image ?> />
                        </div>
                        <div class="form-group m-b-40">
                            <label>{{__('news.formnews_content')}}</label>
                            <span class="bar"></span>
                            <textarea name="news_content" id="editor" required><?php echo @$information->news_content ?>
                            </textarea>
                        </div>

                        <div class="form-group m-b-40">
                            <input name="news_image_source" type="text" class="form-control" value="<?php echo @$information->news_image_source ?>">
                            <span class="bar"></span>
                            <label>{{__('news.formnews_image_source')}}</label>
                        </div>
                        <div class="form-group m-b-40">
                            <input name="news_content_source" type="text" class="form-control" value="<?php echo @$information->news_content_source ?>">
                            <span class="bar"></span>
                            <label>{{__('news.formnews_content_source')}}</label>
                        </div>
                        <div class="form-group m-b-40">
                            <label for="input1">{{__('news.formnews_lang')}}</label>
                            <select  name="news_lang" class="select2 form-control btn-secondary" style="width: 100%; height:36px;">
                                <optgroup label="{{__('news.formnews_lang')}}">
                                    <?php 
                                    $item = array('id','en');
                                     ?>
                                     <?php foreach ($item as $key): ?>
                                        <option value="<?php echo $key ?>" <?php echo @$information->news_lang==$key ? 'selected' : '' ?>><?php echo strtoupper($key) ?></option>
                                     <?php endforeach ?>
                                </optgroup>
                            </select>
                        </div>
                    </section>
                </form>
            </div>
        </div>

    </div>
</div>
@endsection