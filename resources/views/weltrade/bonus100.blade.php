@extends('layout_weltrade')

@section('title', 'Weltrade - Keuntungan 2 Kali Lipat')

@section('main_section')
<div class="uk-section uk-padding-remove-vertical">
    <div class="in-slideshow" data-uk-slideshow>
        <ul class="uk-slideshow-items uk-light">
            <li>
                <div class="uk-position-cover">
                    <img src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-slideshow-image-bonus100.jpg" alt="slideshow-image" data-uk-cover width="1920" height="700" data-uk-img>
                </div>
                <span></span>
                <div class="uk-container">
                    <div class="uk-grid" data-uk-grid>
                        <div class="uk-width-3-5@m">
                            <div class="uk-overlay">
                                <h1>Weltrade, "Keuntungan 2 Kali Lipat"</h1>
                                <p class="uk-text-lead uk-visible@m">Hasilkan keuntungan maksimal hingga dua kali lipat! Dengan ketahanan 2 kali lipat, kami gandakan modal Anda 100%!</p>
                                <a href="https://account.<?php echo $weltrade ?>/auth/registration/?r1=ids&r2=landing_<?php echo $referral ?>" class="uk-button uk-button-primary uk-border-rounded"><i class="fas fa-comment-dollar uk-margin-small-right"></i>Buka Akun sekarang</a>
                                <a href="https://<?php echo $weltradeidn ?>" class="uk-button uk-button-primary uk-border-rounded"><i class="fas fa-home uk-margin-small-right"></i>Kembali ke Web Utama</a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <div class="uk-container uk-light">
            <ul class="uk-slideshow-nav uk-dotnav uk-position-bottom-center"></ul>
        </div>
    </div>
</div> 
@endsection