@extends('layout_weltrade')

@section('title', 'Weltrade - Daftar dan Belajar Trading Forex Gratis')

@section('main_section')
<div class="uk-section uk-padding-remove-vertical">
    <div class="in-slideshow" data-uk-slideshow>
        <ul class="uk-slideshow-items uk-light">
            <li>
                <div class="uk-position-cover">
                    <img src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-slideshow-image-education.jpg" alt="slideshow-image" data-uk-cover width="1920" height="700" data-uk-img>
                </div>
                <span></span>
                <div class="uk-container">
                    <div class="uk-grid" data-uk-grid>
                        <div class="uk-width-3-5@m">
                            <div class="uk-overlay">
                                <h1>Weltrade, Daftar dan Belajar Trading Forex Gratis</h1>
                                <p class="uk-text-lead uk-visible@m">Mulai belajar trading dengan akun micro, resiko kecil tapi keuntungan maksimal. Kesempatan untuk menguji strategi trading Anda</p>
                                <a href="https://wltrd.link/Webinar" class="uk-button uk-button-primary uk-border-rounded"><i class="fa fa-book uk-margin-small-right"></i>Webinar Minggu Ini</a>
                                <a href="https://<?php echo $weltradeidn ?>/webinar" class="uk-button uk-button-primary uk-border-rounded"><i class="fa fa-books uk-margin-small-right"></i>Jadwal Webinar Edukasi</a>
                                <a href="https://account.<?php echo $weltrade ?>/auth/registration/?r1=ids&r2=landing_<?php echo $referral ?>" class="uk-button uk-button-primary uk-border-rounded"><i class="fas fa-comment-dollar uk-margin-small-right"></i>Buka Akun sekarang</a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <div class="uk-container uk-light">
            <ul class="uk-slideshow-nav uk-dotnav uk-position-bottom-center"></ul>
        </div>
    </div>
</div> 
@endsection