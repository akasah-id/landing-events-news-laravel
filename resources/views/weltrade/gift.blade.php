@extends('layout_weltrade_gift')

@section('title', 'Weltrade - Hadiah Untuk Trader Indonesia')

@section('main_section')
	<!--== Start Projects Area Wrapper ==-->
    <section class="project-area">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-lg-10 offset-lg-2 m-auto">
            <div class="portfolio-filter-menu">
              <div class="btn-portfolio">
                <button class="active" data-filter=".all">Semua</button>
              </div>
              <span>-</span>
              <div class="btn-portfolio">
                <button data-filter=".shirts">Kaos Keren</button>
              </div>
              <span>-</span>
              <div class="btn-portfolio">
                <button data-filter=".cap">Topi Elegan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row masonry-style">
          <div class="col-sm-6 col-lg-6 portfolio-item all shirts">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia,%20Mau%20Kaos%20YFP-H&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/shirt-yfp-b.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia,%20Mau%20Kaos%20YFP-H&phone=6281223192802">Your Forex Partner - Hitam</a></h3>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item all cap">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia,%20Mau%20Topi%20Elegan&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/cap.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia,%20Mau%20Topi%20Elegan&phone=6281223192802">Topi Elegan</a></h3>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
        	<p>Aturan: <br>
        		- Deposit $200 atau Saldo $200, Dapatkan Kaos atau Topi <br>
        		- Deposit $500 atau Saldo $500, Dapatkan Kaos dan Topi
        	</p>
        </div>
        <div class="row">
          <div class="col-12 text-center">
            <div class="portfolio-footer">
              <a href="https://weltrade-idn.net/" target="_blank" class="btn-theme btn-projects">Website Utama <i class="icofont-double-right"></i></a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Projects Area Wrapper ==-->
@endsection