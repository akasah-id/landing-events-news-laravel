@extends('layout_weltrade_home')

@section('metadesc', 'WELTRADE - Trading Forex, Emas, Saham, Index dan Kripto Online Paling Aman 15 tahun berpengalaman dan konsisten melayani trader berkelas didunia! Komitmen kami tetap selalu berada disisi sahabat trader Indonesia tak pernah pudar dan akan selalu terjaga! Rayakan 15 Tahun WELTRADE, Komisi IB Tertinggi, Kontes Demo $100.000, Kontes IB $50.000, Trade2Win $10.000.')

@section('title', 'WELTRADE - Trading Forex, Emas, Saham, Index dan Kripto Online Paling Aman')
@section('ogimage', "https://$weltrade/images/wt_preview.png")
@section('ogurl', "https://$weltrade")

@section('main')
        <!--hero banner-->
        <section class="hero-banner owl-carousel" style="background-image: url('template/home/images/main-curve-min.png'); background-repeat: no-repeat;">
            <div class="item-slider">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 order-lg-2 side-text">
                            <div class="side-text-inner">
                                <span class="label-title-banner">Kode Promo: WTERMINAL150</span>
                                <h1 class="title-banner">Bonus Deposit 150%</h1>
                                <p class="desc-banner">Kami memiliki sesuatu yang sangat istimewa untuk Anda! Kami menawarkan promosi bonus 150% deposit (Free margin/support floating/bisa ditradingkan) untuk semua trader Indonesia. <br>Deposit mulai dari $100. Promo berlaku hingga 30 Juni</p>
                                <a class="btn btn-success btn-lg" href="https://terminal.<?php echo $weltrade ?>/login/registration<?php echo "/$ref" ?>">Daftar Sekarang dan Gandakan Keuntungan!</a>
                            </div>
                        </div>
                        <div class="col-lg-7 order-lg-1 side-img">
                            <img src="<?php echo url('/') ?>/file/images/static/promojune.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="item-slider">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 order-lg-2 side-text">
                            <div class="side-text-inner">
                                <span class="label-title-banner">Kode Promo: PROMOGAJIAN</span>
                                <h1 class="title-banner">Bonus Deposit 50%</h1>
                                <p class="desc-banner">Promo Gajian Datang! Kami menawarkan promosi bonus 50% deposit (Free margin/support floating/bisa ditradingkan) untuk semua trader Indonesia. Deposit $200 sampai $2000 dan dapatkan bonus deposit hingga $1000!<br><strong>Promo berlaku 27 Mei - 5 Juni</strong></p>
                                <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Daftar Sekarang dan Gandakan Keuntungan!</a>
                            </div>
                        </div>
                        <div class="col-lg-7 order-lg-1 side-img">
                            <img src="<?php echo url('/') ?>/file/images/static/promogajian27may.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- <div class="item-slider">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 order-lg-2 side-text">
                            <div class="side-text-inner">
                                <span class="label-title-banner">Kode Promo: <strong>THRWELTRADE</strong></span>
                                <h1 class="title-banner">Promo Idul Fitri 1443H</h1>
                                <p class="desc-banner">Yes! Ada THR dari WELTRADE! Kami menawarkan promosi bonus deposit 100% untuk semua trader Indonesia. Cukup deposit mulai dari $200 hingga $2000 (Free margin/support floating/bisa ditradingkan)
                                <br><strong>Promo berlaku 2 Mei - 8 Mei 2022</strong></p>
                                <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Daftar Sekarang dan Gandakan Keuntungan!</a>
                            </div>
                        </div>
                        <div class="col-lg-7 order-lg-1 side-img">
                            <img src="<?php echo url('/') ?>/file/images/static/thrweltrade.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- <div class="item-slider">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 order-lg-2 side-text">
                            <div class="side-text-inner">
                                <span class="label-title-banner">Demo Contest</span>
                                <h1 class="title-banner">WELTRADE Terminal</h1>
                                <p class="desc-banner">KONTES DEMO Baru dari WELTRADE dengan total hadiah $25.000! untuk 250 pemenang. Bebas Trading dengan berbagai instrumen yang tersedia dan hasilkan profit sebanyak banyaknya!
                                    <br><strong>Periode Kontes 2 Mei - 19 Juni 2022</strong></p>
                                <a class="btn btn-success btn-lg" href="https://terminal.<?php echo $weltrade ?>/login/registration<?php echo "/$ref" ?>">Daftar Sekarang. Tahun baru cuan baru!</a>
                            </div>
                        </div>
                        <div class="col-lg-7 order-lg-1 side-img">
                            <img src="<?php echo url('/') ?>/file/images/static/demo25k.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="item-slider">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 order-lg-2 side-text">
                            <div class="side-text-inner">
                                <span class="label-title-banner">Promo Fix Rate</span>
                                <h1 class="title-banner">$1 = Rp 9.000</h1>
                                <p class="desc-banner">Tingkatkan keuntungan dengan promo Fix Rate! <br>Deposit dan Withdraw kini menjadi lebih menguntungkan! Lebih hemat uang dan tingkatkan penghasilan Anda. Fix rate memungkinkan Anda untuk deposit dan menarik dana dengan rate yang sama kapanpun itu. Baik ketika Rupiah menguat atau melemah, nilai tukar akan selalu bernilai sama.</p>
                                <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Deposit dan Trading Sekarang!</a>
                            </div>
                        </div>
                        <div class="col-lg-7 order-lg-1 side-img">
                            <img src="<?php echo url('/') ?>/file/images/static/fixrate.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-slider">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 order-lg-2 side-text">
                            <div class="side-text-inner">
                                <span class="label-title-banner">Program baru dari Weltrade</span>
                                <h1 class="title-banner">IB Reward Program $50</h1>
                                <p class="desc-banner">Dapatkan tambahan $50 untuk setiap trader yang diundang sebagai tambahan dari pembayaran komisi dari volume trading klien seperti biasanya. Klien cukup trading dengan setidaknya 5 lot setelah registrasi dan verifikasi.</p>
                                <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Undang Klien dan Dapatkan Bonus!</a>
                            </div>
                        </div>
                        <div class="col-lg-7 order-lg-1 side-img">
                            <img src="<?php echo url('/') ?>/file/images/static/cpa50.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-slider">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 order-lg-2 side-text">
                            <div class="side-text-inner">
                                <span class="label-title-banner">Trading 300 Lot</span>
                                <h1 class="title-banner">Gratis iPhone 13 Pro Max</h1>
                                <p class="desc-banner">Trading di WELTRADE dan dapatkan IPhone 13 Pro Max baru, Caranya gampang banget! Cukup daftar dan Trading 300 lot tanpa minimum deposit! Promo ini berlangsung sepanjang tahun 2022 lohh.</p>
                                <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Daftar Sekarang. Tahun baru iPhone baru!</a>
                            </div>
                        </div>
                        <div class="col-lg-7 order-lg-1 side-img">
                            <img src="<?php echo url('/') ?>/file/images/static/iphone13pro.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="item-slider">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 order-lg-2 side-text">
                            <div class="side-text-inner">
                                <span class="label-title-banner">Demo Contest</span>
                                <h1 class="title-banner">Stock Battle</h1>
                                <p class="desc-banner">Ayo ikuti kontes demo Stock Battle, sepenuhnya gratis! Menangkan hadiah uang tunai hingga $100.000 untuk 1000 pemenang. Trading saham perusahaan terkenal Google, Meta, Tesla, Amazon, dan 200 saham lainnya.
                                    <br>Ayo Dapatkan lebih banyak pengalaman dalam perdagangan.</p>
                                <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Daftar Sekarang. Tahun baru cuan baru!</a>
                            </div>
                        </div>
                        <div class="col-lg-7 order-lg-1 side-img">
                            <img src="<?php echo url('/') ?>/template/home/images/stock-battle-banner-img.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="item-slider">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 order-lg-2 side-text">
                            <div class="side-text-inner">
                                <h1 class="title-banner">Trading Mata Uang Kripto</h1>
                                <p class="desc-banner">Tingkatkan level trading Anda dengan komisi 0% alias bebas komisi di setiap trading. 
                                <br>Bitcoin, Etherium, Litecoin, dan masih banyak lagi tersedia di Weltrade! Trading crypto sekarang lebih mudah dan tanpa perlu modal besar. Sudah pasti trading di Weltrade itu bebas komisi, termasuk trading crypto.
                                </p>
                                <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Daftar Sekarang dan Trading Nonstop</a>
                            </div>
                        </div>
                        <div class="col-lg-7 order-lg-1 side-img">
                            <img src="<?php echo url('/') ?>/template/home/images/trading-mata-uang-white.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <!--/hero banner-->
        </section>

        <!--instrument trading-->
        <section class="instrument-trading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-5 side-text">
                        <h1 class="main-heading lg">Instrument Trading Terlengkap</h1>
                        <p>Trading Forex, Logam, Komoditas, Saham, Index dan Kripto di Broker Paling Aman dan Terpercaya</p>
                        <div class="group-btn">
                            <a class="btn btn-primary btn btn-lg" href="https://<?php echo $weltrade ?>/trading/tools/<?php echo "$ref" ?>" target="_blank">Selengkapnya</a>
                            <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Buka Akun </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-6 offset-xl-1 side-img">
                        <img src="<?php echo url('/') ?>/template/home/images/instrument-trading.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!--/instrument trading-->

        <!--account type-->
        <section class="account-type">
            <h1 class="main-heading lg">Tipe Akun</h1>
            <p class="sub-heading">Pilih akun yang sesuai dengan strategi terbaik trading anda</p>
            <div class="container">
                <!--account micro-->
                <div class="wp-card-account mb-1">
                    <div class="card-account micro">
                        <div class="label-card">Trader Pemula</div>
                        <div class="title-card">
                            <div class="frame-icon">
                                <img src="<?php echo url('/') ?>/template/home/images/icon-micro.png" alt="">
                            </div>
                            <h2>Micro</h2>
                        </div>
                        <div class="desc-card">
                            <ul>
                                <li>Deposit 1 USD</li>
                                <li>Eksekusi 0.9 detik</li>
                                <li>Sen USD dan Sen EUR</li>
                                <li>Spread Floating 1.5</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/account micro-->

                <!--account premium-->
                <div class="wp-card-account mb-1">
                    <div class="card-account premium">
                        <div class="label-card">Trader Umum</div>
                        <div class="title-card">
                            <div class="frame-icon">
                                <img src="<?php echo url('/') ?>/template/home/images/icon-premium.png" alt="">
                            </div>
                            <h2>Premium</h2>
                        </div>
                        <div class="desc-card">
                            <ul>
                                <li>Deposit 25 USD</li>
                                <li>Eksekusi 0.7 detik</li>
                                <li>USD dan EUR</li>
                                <li>Spread Floating 1.5</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/account premium-->

                <!--account Pro-->
                <div class="wp-card-account mb-1">
                    <div class="card-account pro">
                        <div class="label-card">Pilihan Terbaik</div>
                        <div class="title-card">
                            <div class="frame-icon">
                                <img src="<?php echo url('/') ?>/template/home/images/icon-pro.png" alt="">
                            </div>
                            <h2>Pro</h2>
                        </div>
                        <div class="desc-card">
                            <ul>
                                <li>Deposit 100 USD</li>
                                <li>Eksekusi 0.5 detik</li>
                                <li>Mata Uang USD</li>
                                <li>Spread Floating 0.3</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/account Pro-->

                <!--account zulu trade-->
                <!-- <div class="wp-card-account mb-1">
                    <div class="card-account zulu-trade">
                        <div class="label-card">Robot Trading</div>
                        <div class="title-card">
                            <div class="frame-icon">
                                <img src="<?php echo url('/') ?>/template/home/images/icon-zulu-trade.png" alt="">
                            </div>
                            <h2>Zulu Trade</h2>
                        </div>
                        <div class="desc-card">
                            <ul>
                                <li>Deposit Minimum 200 USD</li>
                                <li>Kecepatan Eksekusi Mulai 0.7 detik</li>
                                <li>Mata Uang USD</li>
                                <li>Spread Floating Dari 2.9</li>
                            </ul>
                        </div>
                    </div>
                </div> -->
                <!--/account zulu trade-->

                <!--account crypto-->
                <div class="wp-card-account mb-1">
                    <div class="card-account crypto">
                        <div class="label-card">Tanpa Biaya</div>
                        <div class="title-card">
                            <div class="frame-icon">
                                <img src="<?php echo url('/') ?>/template/home/images/icon-crypto.png" alt="">
                            </div>
                            <h2>Kripto</h2>
                        </div>
                        <div class="desc-card">
                            <ul>
                                <li>Deposit 50 USD</li>
                                <li>Eksekusi 0.7 detik</li>
                                <li>Mata Uang USD</li>
                                <li>Spread Floating 0</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/account crypto-->
            </div>
            <div class="group-btn">
                <a class="btn btn-primary btn btn-lg" href="https://<?php echo $weltrade ?>/trading/type_account/<?php echo "$ref" ?>" target="_blank">Pelajari Selengkapnya</a>
                <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration/<?php echo "$ref" ?>">Buka Akun Sekarang</a>
            </div>
        </section>
        <!--/account type-->

        <!--instrument trading-->
        <section class="instrument-trading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-6 side-text" style="text-align: center;">
                        <h1 class="main-heading lg">Tingkatkan Kehidupan Finansial Anda</h1>
                        <iframe width="100%" height="350px" src="https://www.youtube.com/embed/RgXiu3s0pOA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="col-lg-6 col-xl-6 side-text" style="text-align: center;">
                        <h1 class="main-heading lg">Jadi Partner dan Dapatkan Komisi Besar</h1>
                        <iframe width="100%" height="350px" src="https://www.youtube.com/embed/jyZCzng-Tak" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>
        <!--/instrument trading-->

        <!--deposit and withdraw-->
        <section class="transaction">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 mb-5 mb-lg-0 side-text">
                        <h1 class="main-heading sm">Deposit dan Penarikan Tercepat di Market</h1>
                        <p>Deposit ke akun trading Anda melalui transfer bank, kartu kredit, dan segera dapatkan uang Anda menggunakan metode penarikan tercepat kami!</p>
                        <a class="btn btn-success btn-lg" href="https://<?php echo $weltrade ?>/trading/paysystems/<?php echo "$ref" ?>" target="_blank">Pelajari Lebih Lanjut</a>
                    </div>
                    <div class="col-lg-6 offset-lg-1 list-logo">
                        <div class="row">
                            <div class="col-6 col-sm-4 mb-4">
                                <div class="thumb-logo bca">
                                    <img src="<?php echo url('/') ?>/template/home/images/logo-bca.png" alt="">
                                </div>
                            </div>
                            <div class="col-6 col-sm-4 mb-4">
                                <div class="thumb-logo bri">
                                    <img src="<?php echo url('/') ?>/template/home/images/logo-bri.png" alt="">
                                </div>
                            </div>
                            <div class="col-6 col-sm-4 mb-4">
                                <div class="thumb-logo bni">
                                    <img src="<?php echo url('/') ?>/template/home/images/logo-bni.png" alt="">
                                </div>
                            </div>
                            <div class="col-6 col-sm-4 mb-4">
                                <div class="thumb-logo local-bank">
                                    <img src="<?php echo url('/') ?>/template/home/images/logo-local-bank.jpg" alt="" style="width: 195px;">
                                </div>
                            </div>
                            <div class="col-6 col-sm-4 mb-4">
                                <div class="thumb-logo fasapay">
                                    <img src="<?php echo url('/') ?>/template/home/images/logo-fasapay.png" alt="">
                                </div>
                            </div>
                            <div class="col-6 col-sm-4 mb-4">
                                <div class="thumb-logo skrill">
                                    <img src="<?php echo url('/') ?>/template/home/images/logo-skrill.png" alt="">
                                </div>
                            </div>
                            <div class="col-6 col-sm-4 mb-4 mb-sm-0">
                                <div class="thumb-logo neteller">
                                    <img src="<?php echo url('/') ?>/template/home/images/logo-neteller.png" alt="">
                                </div>
                            </div>
                            <div class="col-6 col-sm-4">
                                <div class="thumb-logo perfect-money">
                                    <img src="<?php echo url('/') ?>/template/home/images/logo-perfect-money.png" alt="">
                                </div>
                            </div>
                            <div class="col-6 col-sm-4">
                                <div class="thumb-logo crypto-currency">
                                    <img src="<?php echo url('/') ?>/template/home/images/logo-crypto-currency.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/deposit and withdraw-->

        <!--security fund-->
        <section class="security-fund">       
            <div class="container">
                <h1 class="main-heading lg">Keamanan Dana dan Data</h1>
                <p class="sub-heading">Keamanan dana Anda adalah prioritas utama kami dan Keamanan data Anda adalah tanggung jawab kami</p>
                <div class="row">
                    <div class="col-lg-4 mb-4 mb-lg-0">
                        <div class="card-thumb-1">
                            <div class="frame-icon">
                                <img style="width:80px" src="<?php echo url('/') ?>/template/home/images/icon-shield.png" alt="">
                            </div>
                            <h4>Proteksi Saldo Negatif</h4>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-4 mb-lg-0">
                        <div class="card-thumb-1">
                            <div class="frame-icon">
                                <img style="width:65px" src="<?php echo url('/') ?>/template/home/images/icon-thumbs-up.png" alt="">
                            </div>
                            <h4>Broker Andal dan Tepercaya</h4>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card-thumb-1">
                            <div class="frame-icon">
                                <img style="width:65px" src="<?php echo url('/') ?>/template/home/images/icon-id.png" alt="">
                            </div>
                            <h4>Verifikasi Akun Cepat</h4>
                        </div>
                    </div>
                </div>
                <div class="block-btn text-center">
                    <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration/<?php echo "$ref" ?>">Mulai Trading</a>
                </div>
            </div>
        </section>
        <!--/security fund-->


        <!--count-->
        <section class="count">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-lg-3 mobile-mb-5 count-list">
                        <h1>
                            <span class="count-num">15</span>
                            <span>+</span>
                        </h1>
                        <span>tahun pengalaman di pasar forex</span>
                    </div>
                    <div class="col-sm-6 col-lg-3 mobile-mb-5 count-list">
                        <h1>
                            <span class="count-num">600</span>
                            <span>K+</span>
                        </h1>
                        <span>trader yang puas dari 180 negara</span>
                    </div>
                    <div class="col-sm-6 col-lg-3 mobile-mb-5 mb-sm-0 count-list">
                        <h1>
                            <span class="count-num">24</span>
                            <span>/</span>
                            <span class="count-num">7</span>
                        </h1>
                        <span>layanan full support untuk klien</span>
                    </div>
                    <div class="col-sm-6 col-lg-3 count-list">
                        <h1>
                            <span class="count-num">30</span>
                        </h1>
                        <span>menit untuk penarikan dana</span>
                    </div>
                </div>
            </div>
        </section>
        <!--/count-->

        <!--platform trading-->
        <section class="platform-trading">
            <div class="container">
                <h1 class="main-heading lg">Platform Trading</h1>
                <div class="row">
                    <div class="col-lg-6 col-xl-5 offset-xl-1">
                        <div class="card-offer">
                            <div class="side-top">
                                <h2>Meta Trader 4</h2>
                                <p>MetaTrader 4 menawarkan teknologi trading dan analisa terbaik, juga berbagai layanan lainnya. Semua yang Anda butuhkan untuk Trading Forex tersedia disini.</p>
                                <ul>
                                    <li>
                                        <i class="fas fa-check-circle"></i>
                                        <span>3 Mode Eksekusi</span>
                                    </li>
                                    <li>
                                        <i class="fas fa-check-circle"></i>
                                        <span>9 Time Frame untuk Trading</span>
                                    </li>
                                    <li>
                                        <i class="fas fa-check-circle"></i>
                                        <span>1700+ robot trading</span>
                                    </li>
                                    <li>
                                        <i class="fas fa-check-circle"></i>
                                        <span>Notifikasi tepat waktu</span>
                                    </li>
                                    <li>
                                        <i class="fas fa-check-circle"></i>
                                        <span>Tersedia untuk iOS dan Android OS</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="side-bottom">
                                <a class="btn btn-success btn-sm" href="#" style="margin-right: 20px; margin-bottom: 20px">
                                    <i class="fab fa-apple"></i>
                                    <span>IOS</span>
                                </a>
                                <a class="btn btn-success btn-sm" href="#" style="margin-right: 20px; margin-bottom: 20px">
                                    <i class="fab fa-android"></i>
                                    <span>Android</span>
                                </a>
                                <a class="btn btn-success btn-sm" href="#" style="margin-right: 20px; margin-bottom: 20px">
                                    <i class="fab fa-windows"></i>
                                    <span>Windows</span>
                                </a>
                                <a class="btn btn-success btn-sm" href="#" style="margin-bottom: 20px">
                                    <i class="fas fa-window-restore"></i>
                                    <span>Web Terminal</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-5">
                        <div class="card-offer">
                            <label class="label-card">Versi Terbaru</label>
                            <div class="side-top">
                                <h2>Meta Trader 5</h2>
                                <p>Platform multi-aset MetaTrader 5 yang mendukung metode hedging, yang memungkinkan pembukaan beberapa posisi dari instrumen keuangan yang sama.</p>
                                <ul>
                                    <li>
                                        <i class="fas fa-check-circle"></i>
                                        <span>5 mode eksekusi</span>
                                    </li>
                                    <li>
                                        <i class="fas fa-check-circle"></i>
                                        <span>Multi-currency tester</span>
                                    </li>
                                    <li>
                                        <i class="fas fa-check-circle"></i>
                                        <span>Lebih dari 2500 aplikasi algoritmik siap pakai</span>
                                    </li>
                                    <li>
                                        <i class="fas fa-check-circle"></i>
                                        <span>6 jenis pending order</span>
                                    </li>
                                    <li>
                                        <i class="fas fa-check-circle"></i>
                                        <span>Tersedia untuk iOS dan Android OS</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="side-bottom">
                                <a class="btn btn-success btn-sm" href="#" style="margin-right: 20px; margin-bottom: 20px">
                                    <i class="fab fa-apple"></i>
                                    <span>IOS</span>
                                </a>
                                <a class="btn btn-success btn-sm" href="#" style="margin-right: 20px; margin-bottom: 20px">
                                    <i class="fab fa-android"></i>
                                    <span>Android</span>
                                </a>
                                <a class="btn btn-success btn-sm" href="#" style="margin-right: 20px; margin-bottom: 20px">
                                    <i class="fab fa-windows"></i>
                                    <span>Windows</span>
                                </a>
                                <a class="btn btn-success btn-sm" href="#" style="margin-right: 20px; margin-bottom: 20px">
                                    <i class="fas fa-window-restore"></i>
                                    <span>Web Terminal</span>
                                </a>
                                <a class="btn btn-success btn-sm" href="#" style="margin-right: 20px; margin-bottom: 20px">
                                    <img src="<?php echo url('/') ?>/template/home/images/icon-huaweii.png" alt="">
                                    <span>Huaweii</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/platform trading-->

        <!--crypto trading-->
        <section class="crypto-trading">
            <img class="ornament" src="<?php echo url('/') ?>/template/home/images/coin-crypto-1.png" alt="">
            <img class="ornament" src="<?php echo url('/') ?>/template/home/images/coin-crypto-2.png" alt="">
            <img class="ornament" src="<?php echo url('/') ?>/template/home/images/coin-crypto-3.png" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 side-img">
                        <img src="<?php echo url('/') ?>/template/home/images/crypto-trading.jpg" alt="">
                    </div>
                    <div class="col-lg-5 offset-lg-1 side-text">
                        <h1>
                            <span class="me-3">Trading</span>
                            <span>Mata Uang Kripto</span>
                        </h1>
                        <p>Tingkatkan level trading Anda dengan komisi 0% alias bebas komisi di setiap trading.</p>
                        <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration/<?php echo "$ref" ?>">Mulai Trading</a>
                    </div>
                </div>
            </div>
        </section>
        <!--/crypto trading-->

        <!--feature 1-->
        <section class="feature-1">
            <div class="container">
                <div class="row">
                    <div class="d-none d-lg-block col-lg-6 col-xl-6 side-img">
                        <img src="<?php echo url('/') ?>/template/home/images/bg-feature-1.jpg" alt="">
                    </div>
                    <div class="col-lg-6 col-xl-5 offset-xl-1 side-text">
                        <h1 class="main-heading lg">Sarana Inovatif Trader</h1>
                        <ul>
                            <li>
                                <div class="icon-frame">
                                    <img src="<?php echo url('/') ?>/template/home/images/icon-feature-list-1.png" alt="">
                                </div>
                                <div class="text-list">
                                    <h4>Bonus 100%</h4>
                                    <p>Deposit $200 ke akun Anda dan dapatkan bonus kredit 100% langsung.</p>
                                </div>
                            </li>
                            <li>
                                <div class="icon-frame">
                                    <img src="<?php echo url('/') ?>/template/home/images/icon-feature-list-2.png" alt="">
                                </div>
                                <div class="text-list">
                                    <h4>Bebas Komisi</h4>
                                    <p>Ada Lebih dari 30 cara untuk deposit ke akun trading Anda, bebas komisi dan selalu pada kurs yang menguntungkan.</p>
                                </div>
                            </li>
                            <li>
                                <div class="icon-frame">
                                    <img src="<?php echo url('/') ?>/template/home/images/icon-feature-list-3.png" alt="">
                                </div>
                                <div class="text-list">
                                    <h4>Program Affiliasi</h4>
                                    <p>Dapatkan komisi untuk semua pelanggan baru yang Anda undang.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!--/feature 1-->

        <!--feature 2-->
        <section class="feature-2">
            <div class="container">
                <h1 class="main-heading lg">Era Baru Trading Forex</h1>
                <div class="row">
                    <div class="col-lg-4 mb-4 mb-lg-0">
                        <div class="card-thumb-2">
                            <img src="<?php echo url('/') ?>/template/home/images/icon-feature-list-4.png" alt="">
                            <h4>CASH PUZZLE</h4>
                            <p>Kumpulkan puzzle untuk memenangkan hadiah uang tunai harian hingga $15.000!</p>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-4 mb-lg-0">
                        <div class="card-thumb-2">
                            <img src="<?php echo url('/') ?>/template/home/images/icon-feature-list-5.png" alt="">
                            <h4>TRADE2WIN</h4>
                            <p>Selesaikan 2 lot turnover trading selama seminggu dan dapatkan kesempatan untuk memenangkan hadiah tunai setiap hari Minggu!</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card-thumb-2">
                            <img src="<?php echo url('/') ?>/template/home/images/icon-feature-list-6.png" alt="">
                            <h4>SPINNER</h4>
                            <p>Anda bisa mendapatkan bonus hingga $1.000 untuk trading setelah setiap deposit.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/feature 2-->

        <!--event gallery-->
        <section class="event-gallery">
            <div class="container">
                <!--group heading-->
                <div class="group-heading-slider">
                    <h1 class="main-heading lg text-center">Galeri Acara</h1>
                    <div class="group-meta">
                        <div class="clue-slide"><img src="<?php echo url('/') ?>/template/home/images/icon-swipe.png" alt=""><span>Geser untuk melihat foto lainnya</span></div>
                        <div class="count-slide-event"></div>
                    </div>
                </div>
                <!--/group heading-->

                <!--slider event gallery-->
                <div class="slider-event owl-carousel">
                    <a class="slider-item" href="<?php echo url('/') ?>/template/home/images/event-gallery-1.jpg" data-fancybox data-caption="">
                        <div class="group-img">
                            <img src="<?php echo url('/') ?>/template/home/images/event-gallery-1-thumb.jpg" alt="">
                            <img src="<?php echo url('/') ?>/template/home/images/icon-zoom.png" alt="">
                        </div>
                        <div class="title-slider"></div>
                    </a>
                    <a class="slider-item" href="<?php echo url('/') ?>/template/home/images/event-gallery-2.jpg" data-fancybox data-caption="">
                        <div class="group-img">
                            <img src="<?php echo url('/') ?>/template/home/images/event-gallery-2-thumb.jpg" alt="">
                            <img src="<?php echo url('/') ?>/template/home/images/icon-zoom.png" alt="">
                        </div>
                        <div class="title-slider"></div>
                    </a>
                    <a class="slider-item" href="<?php echo url('/') ?>/template/home/images/event-gallery-3.jpg" data-fancybox data-caption="">
                        <div class="group-img">
                            <img src="<?php echo url('/') ?>/template/home/images/event-gallery-3-thumb.jpg" alt="">
                            <img src="<?php echo url('/') ?>/template/home/images/icon-zoom.png" alt="">
                        </div>
                        <div class="title-slider"></div>
                    </a>
                </div>
                <!--/slider event gallery-->
            </div>
        </section>
        <!--/event gallery-->
@endsection