@extends('layout_weltrade_home')

@section('metadesc', "Ayo daftar dan dapatkan hadiah keren dari WELTRADE! Tersedia berbagai hadiah menarik mulai dari gadget mewah, sepeda, hingga merchandise.")

@section('title', 'Loyalty Program Untuk Indonesia - Weltrade')
@section('ogimage', "https://weltrade.win/images/wt_preview.png")
@section('ogurl', url()->current())

@section('main')
        <!--platform trading-->
        <section class="platform-trading detail-title education" style="background: white !important;">
    	<img src="<?php echo url('/file/images/static/loyalty.jpg') ?>" class="news-banner-top" alt="">
            <div class="container">
                <h1 class="main-heading lg">Loyalty Program</h1>
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <div class="card-offer">
                            <label class="label-card news-label-card">
                        	Untuk Trader Indonesia - Dari WELTRADE
                            </label>
                            <div class="side-top" style="padding-bottom: 30px;">
                                <!--slider education-->
                            <div class="slider-list slider-education">
                            		<!--== Start Projects Area Wrapper ==-->
    <section class="project-area">

      <div class="container">
        <div class="row masonry-style">
          
          <div class="col-sm-6 col-lg-6 portfolio-item pro laptops">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/pro/mbp14.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Macbook Pro 14 M1 Pro</a></h3>
                <p style="color:white"><strong>Pro Users,</strong> Balance $50.000, 3000 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item pro laptops all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/pro/mbp16.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Macbook Pro 16 M1 Max</a></h3>
                <p style="color:white"><strong>Pro Users,</strong> Balance $50.000, 4500 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item pro laptops all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/pro/surface.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Microsoft Surface Studio</a></h3>
                <p style="color:white"><strong>Pro Users,</strong> Balance $50.000, 3000 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item pro laptops">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/pro/yoga.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Lenovo X1 Titanium Yoga</a></h3>
                <p style="color:white"><strong>Pro Users,</strong> Balance $50.000, 3000 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item pro laptops">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/pro/rog.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">ASUS ROG STRIX SCAR</a></h3>
                <p style="color:white"><strong>Pro Users,</strong> Balance $50.000, 3000 Lot</p>
              </div>
            </div>
          </div>

          <div class="col-sm-6 col-lg-6 portfolio-item pro smartphones all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/pro/iphone13promax.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">iPhone 13 Pro Max</a></h3>
                <p style="color:white"><strong>Pro Users,</strong> Balance $10.000, 2500 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item pro smartphones all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/pro/zfold3.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Samsung Z Fold3</a></h3>
                <p style="color:white"><strong>Pro Users,</strong> Balance $10.000, 2000 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item pro smartphones">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/pro/ipadpro.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">iPad Pro 12.9 Inch</a></h3>
                <p style="color:white"><strong>Pro Users,</strong> Balance $10.000, 3000 Lot</p>
              </div>
            </div>
          </div>
          <!-- Premium Users -->
          <div class="col-sm-6 col-lg-6 portfolio-item premium laptops">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/yogaduet.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Lenovo Yoga Duet 7i</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 1500 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium gadgets all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/ps5.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">PlayStation 5</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 1000 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium smartphones all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/zflip3.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Samsung Z Flip3</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 1000 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium smartphones">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/iphone13.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">iPhone 13</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 1000 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium smartphones">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/s21ultra.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Samsung S21 Ultra</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 1100 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium laptops all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/mbair.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Macbook Air M1</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 1000 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium gadgets">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/ipadair.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">iPad Air Wifi</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 700 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium gadgets">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/applewatch.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Apple Watch 7</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 500 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium gadgets">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/samswatch.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Samsung Watch 4</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 250 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium gadgets">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/airpodspro.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Airpods Pro</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 250 Lot</p>
              </div>
            </div>
          </div>

          <div class="col-sm-6 col-lg-6 portfolio-item premium stuff">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/londoncrb.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">London Taxi CRB M 700 C</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 400 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium stuff all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/londonrb.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">London Taxi RB 700C</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 300 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium stuff">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/united.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">United MTB Shadow Nagato</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 400 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium stuff all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/unitedfold.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">United Folding Bike 20</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 400 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium stuff all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/lgfridge.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">LG Side by Side InstaView</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 1500 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium stuff">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/modena.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Modena Freestanding FC 5942 S</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 1500 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item premium stuff">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/premium/airfryer.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Philips Air Fryer Spectre</a></h3>
                <p style="color:white"><strong>Premium Users,</strong> Balance $1000, 100 Lot</p>
              </div>
            </div>
          </div>
          <!-- Micro Users -->
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/shirt-candle-b.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">T-Shirt "Candle" - Black</a></h3>
                <p style="color:white"><strong>All Users,</strong> 5 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/shirt-candle-w.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">T-Shirt "Candle" - White</a></h3>
                <p style="color:white"><strong>All Users,</strong> 5 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/shirt-flying-b.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">T-Shirt "Flying" - Black</a></h3>
                <p style="color:white"><strong>All Users,</strong> 5 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/shirt-flying-w.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">T-Shirt "Flying" - White</a></h3>
                <p style="color:white"><strong>All Users,</strong> 5 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/shirt-uyfl-b.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">T-Shirt "Upgrade Your Financial Life" - Black</a></h3>
                <p style="color:white"><strong>All Users,</strong> 5 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/shirt-uyfl-w.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">T-Shirt "Upgrade Your Financial Life" - White</a></h3>
                <p style="color:white"><strong>All Users,</strong> 5 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/shirt-yfp-b.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">T-Shirt "Your Forex Partner" - Black</a></h3>
                <p style="color:white"><strong>All Users,</strong> 5 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/shirt-yfp-w.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">T-Shirt "Your Forex Partner" - White</a></h3>
                <p style="color:white"><strong>All Users,</strong> 5 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/cap.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Elegant Cap</a></h3>
                <p style="color:white"><strong>All Users,</strong> 5 Lot</p>
              </div>
            </div>
          </div>


          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/polo.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Classy Polo</a></h3>
                <p style="color:white"><strong>All Users,</strong> 10 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/hoodie.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Amazing Hoodie</a></h3>
                <p style="color:white"><strong>All Users,</strong> 20 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/tumbler.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Travel Tumbler</a></h3>
                <p style="color:white"><strong>All Users,</strong> 20 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/backpack.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Cool Backpack</a></h3>
                <p style="color:white"><strong>All Users,</strong> 40 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro gadgets">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/speaker.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Google Home Mini</a></h3>
                <p style="color:white"><strong>All Users,</strong> 40 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/duffle.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Duffle Bag</a></h3>
                <p style="color:white"><strong>All Users,</strong> 50 Lot</p>
              </div>
            </div>
          </div>

          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/birthday.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Exclusive Birthday Gift</a></h3>
                <p style="color:white"><strong>All Users,</strong> 0 Lot</p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-6 portfolio-item micro merchandise all" data-filter="all">
            <div class="inner-content">
              <div class="thumb">
                <a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802"><img src="<?php echo url('/') ?>/template/loyalty/assets/img/loyalty/micro/mysterious.jpg" alt="image" width="500"/></a>
              </div>
              <div class="portfolio-info">
                <h3 class="title"><a target="_blank" href="https://api.whatsapp.com/send?text=Halo%20Weltrade%20Indonesia&phone=6281223192802">Mysterious Gift</a></h3>
                <p style="color:white"><strong>All Users,</strong> 10 Lot</p>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          	<p>Pro Status
                <br> Min Balance: $10.000
                <br> Advantages:
                <br> - Private Professional Manager 
                <br> - Private Professional Mentor
                <br> - Exclusive Birthday Gift
          	</p>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <p>Premium Status
              <br> Min Balance: $1000
              <br> Advantages:
              <br> - Private Support
              <br> - Weekly Market Outlook
              <br> - Free Trading Consultation
          </p>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <p>Micro Status
              <br> Min Balance: $0
              <br> Advantages:
              <br> - Free Webinar
              <br> - Dedicated Support Time
              <br> - Fast Deposit and Withdraw
          </p>
          </div>
        </div>
      </div>
    </section>
    <!--== End Projects Area Wrapper ==-->
							</div>
                            <!--/slider education-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/platform trading-->

        <!--instrument trading-->
        <section class="instrument-trading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-5 side-text">
                        <h1 class="main-heading lg">Instrument Trading Terlengkap</h1>
                        <p>Trading Forex, Logam, Komoditas, Saham, Index dan Kripto di Broker Paling Aman dan Terpercaya</p>
                        <div class="group-btn">
                            <a class="btn btn-primary btn btn-lg" href="https://<?php echo $weltrade ?>/trading/tools/<?php echo "$ref" ?>" target="_blank">Selengkapnya</a>
                            <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Buka Akun </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-6 offset-xl-1 side-img">
                        <img src="<?php echo url('/') ?>/template/home/images/instrument-trading.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!--/instrument trading-->

        <!--count-->
        <section class="count">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-lg-3 mobile-mb-5 count-list">
                        <h1>
                            <span class="count-num">15</span>
                            <span>+</span>
                        </h1>
                        <span>tahun pengalaman di pasar forex</span>
                    </div>
                    <div class="col-sm-6 col-lg-3 mobile-mb-5 count-list">
                        <h1>
                            <span class="count-num">600</span>
                            <span>K+</span>
                        </h1>
                        <span>trader yang puas dari 180 negara</span>
                    </div>
                    <div class="col-sm-6 col-lg-3 mobile-mb-5 mb-sm-0 count-list">
                        <h1>
                            <span class="count-num">24</span>
                            <span>/</span>
                            <span class="count-num">7</span>
                        </h1>
                        <span>layanan full support untuk klien</span>
                    </div>
                    <div class="col-sm-6 col-lg-3 count-list">
                        <h1>
                            <span class="count-num">30</span>
                        </h1>
                        <span>menit untuk penarikan dana</span>
                    </div>
                </div>
            </div>
        </section>
        <!--/count-->
@endsection