@extends('layout_weltrade_home')

@section('metadesc', $category['category_desc'])

@section('title', $category['category_name'].' - Weltrade')
@section('ogimage', url('/file/images/news_category/'.$category['category_image']))
@section('ogurl', url()->current())

@section('main')
        <!--platform trading-->
        <section class="platform-trading detail-title education" style="background: white !important;">
    	<img src="<?php echo url('/file/images/static/list.jpg') ?>" class="news-banner-top" alt="">
            <div class="container">
                <h1 class="main-heading lg"><?php echo $category['category_name'] ?></h1>
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <div class="card-offer">
                            <label class="label-card news-label-card">
                        	Berdasarkan data terbaru per hari ini, <?php echo $datenow; ?>
                            </label>
                            <div class="side-top" style="padding-bottom: 30px;">
                                <!--slider education-->
                            <div class="slider-list slider-education">
                            	<div class="row">
                            		<?php 
                            		if ($category_hashtag=='webinar') {
                            			// $news = array_reverse($news);
                            		}
                            		 ?>
                            		<?php foreach ($news as $key => $value): ?>
                            		<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
                            			<div class="slider-item" style="margin-bottom: 20px;">
											<img src="<?php echo $value['meta_image'] ?>" style="width: 100%;" alt="">
											<div class="text-slider">
												<a href="<?php echo url('/'.$category_hashtag.'/'.$value['slug']) ?>"><?php echo $value['title'] ?></a>
												<div class="block-meta">
													<p><?php 
													if (strlen($value['subtitle']) > 100) {
														echo substr($value['subtitle'] , 0, 70) . '...';
													} else {
														echo $value['subtitle'];
													}
													?></p>
		                                            <div class="list-meta" style="display: block;">
		                                                <i class="fas fa-user"></i>
		                                                <span><?php echo $value['creator'] ?></span>
		                                            </div>
		                                            <div class="list-meta" style="display: block;">
		                                                <i class="fas fa-calendar"></i>
		                                                <?php if ($category_hashtag=='webinar'): ?>
		                                                <span><?php echo $value['voicetext'] ?></span>
		                                                <?php else: ?>
		                                                <span><?php echo $value['datetime'] ?></span>
		                                                <?php endif ?>
		                                            </div>
		                                        </div>
											</div>
										</div>
                            		</div>
                            		<?php endforeach ?>
                            	</div>
							</div>
                            <!--/slider education-->
                            </div>
                            <div class="side-bottom" style="padding: 50px 60px 50px 60px;">
                            	<div class="row">
		                        	<div>
		                                <a class="btn btn-success btn-sm" href="#" style="padding: 32px 20px;margin-right: 20px;margin-bottom: 0px; text-align: left;">
		                                    <span style="font-size: 1.3rem;"> <?php echo $category['category_name'] ?></span>
		                                    <hr>
		                                    <i class="fa fa-award"></i>
		                                    <span style="font-size: 1rem; font-weight: 500;"><?php echo $category['category_desc'] ?></span>
		                                </a>
		                        	</div>
                            	</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/platform trading-->

        <!--instrument trading-->
        <section class="instrument-trading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-5 side-text">
                        <h1 class="main-heading lg">Instrument Trading Terlengkap</h1>
                        <p>Trading Forex, Logam, Komoditas, Saham, Index dan Kripto di Broker Paling Aman dan Terpercaya</p>
                        <div class="group-btn">
                            <a class="btn btn-primary btn btn-lg" href="https://<?php echo $weltrade ?>/trading/tools/<?php echo "$ref" ?>" target="_blank">Selengkapnya</a>
                            <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Buka Akun </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-6 offset-xl-1 side-img">
                        <img src="<?php echo url('/') ?>/template/home/images/instrument-trading.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!--/instrument trading-->

        <!--count-->
        <section class="count">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-lg-3 mobile-mb-5 count-list">
                        <h1>
                            <span class="count-num">15</span>
                            <span>+</span>
                        </h1>
                        <span>tahun pengalaman di pasar forex</span>
                    </div>
                    <div class="col-sm-6 col-lg-3 mobile-mb-5 count-list">
                        <h1>
                            <span class="count-num">600</span>
                            <span>K+</span>
                        </h1>
                        <span>trader yang puas dari 180 negara</span>
                    </div>
                    <div class="col-sm-6 col-lg-3 mobile-mb-5 mb-sm-0 count-list">
                        <h1>
                            <span class="count-num">24</span>
                            <span>/</span>
                            <span class="count-num">7</span>
                        </h1>
                        <span>layanan full support untuk klien</span>
                    </div>
                    <div class="col-sm-6 col-lg-3 count-list">
                        <h1>
                            <span class="count-num">30</span>
                        </h1>
                        <span>menit untuk penarikan dana</span>
                    </div>
                </div>
            </div>
        </section>
        <!--/count-->
@endsection