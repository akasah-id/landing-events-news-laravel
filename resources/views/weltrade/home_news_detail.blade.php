@extends('layout_weltrade_home')

@section('metadesc', $newsDetail['subtitle'])

@section('title', $newsDetail['title'].' - '.$newsDetail['category'].' Weltrade')
@section('ogimage', url('/file/images/news/'.$newsDetail['meta_image']))
@section('ogurl', url()->current())

@section('main')
        <!--platform trading-->
        <section class="platform-trading detail-title">
    	<img src="<?php echo url('/file/images/news/'.$newsDetail['image']) ?>"  class="news-banner-top" alt="">
            <div class="container">
                <h1 class="main-heading lg"><?php echo $newsDetail['title'] ?></h1>
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <div class="card-offer">
                            <label class="label-card news-label-card">
                                <a style="color: deepskyblue;text-decoration: none;" href="<?php echo url('/'.$newsDetail['category_hashtag']) ?>"><strong><?php echo $newsDetail['category'] ?></strong></a>
                        	<?php 
                        		if ($newsDetail['category_hashtag']=='webinar') {
                            		echo ' - Hari '.$newsDetail['voicetext']; 
                        		} else {
                            		echo ' - Berita '.$newsDetail['datetime']; 
                        		}
                            ?>
                            </label>
                            <div class="side-top">
                                <img src="<?php echo url('/file/images/news/'.$newsDetail['meta_image']) ?>" style="max-width: 100%;margin-bottom: 30px;height: auto;" alt="">
                                <h3><?php echo $newsDetail['subtitle'] ?></h3>
                                <?php echo $newsDetail['content'] ?>
                                <ul style="margin-bottom: 30px;">
                                    <li>
                                        <i class="fas fa-check-circle"></i>
                                        <span><a style="color: inherit;text-decoration: none;" href="<?php echo "https://account.$weltrade/auth/registration" ?>">Mulai belajar dan trading sekarang gratis!</a></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="side-bottom" style="padding: 50px 60px 50px 60px;">
                            	<div class="row">
		                        	<div>
		                        		<img src="<?php echo url('/file/images/users/'.$avatar) ?>" style="height: 150px;width: 150px;border-radius: 10px;" alt="">
		                        	
		                                <a class="btn btn-success btn-sm" href="#" style="padding: 32px 20px;margin-right: 20px;margin-bottom: 0px; text-align: left;">
		                                    <!-- <i class="fa fa-lg fa-user-tie"></i> -->
		                                    <span style="font-size: 1.3rem;"> <?php echo $newsDetail['creator'] ?></span>
		                                    <hr>
		                                    <i class="fa fa-award"></i>
		                                    <span style="font-size: 1rem; font-weight: 500;"> <?php echo $newsDetail['intro'] ?></span>
		                                </a>
		                        	</div>
                            	</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/platform trading-->

        <!--instrument trading-->
        <section class="instrument-trading">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-5 side-text">
                        <h1 class="main-heading lg">Instrument Trading Terlengkap</h1>
                        <p>Trading Forex, Logam, Komoditas, Saham, Index dan Kripto di Broker Paling Aman dan Terpercaya</p>
                        <div class="group-btn">
                            <a class="btn btn-primary btn btn-lg" href="https://<?php echo $weltrade ?>/trading/tools/<?php echo "$ref" ?>" target="_blank">Selengkapnya</a>
                            <a class="btn btn-success btn-lg" href="https://account.<?php echo $weltrade ?>/auth/registration<?php echo "/$ref" ?>">Buka Akun </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-6 offset-xl-1 side-img">
                        <img src="<?php echo url('/') ?>/template/home/images/instrument-trading.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!--/instrument trading-->

        <!--count-->
        <section class="count">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-lg-3 mobile-mb-5 count-list">
                        <h1>
                            <span class="count-num">15</span>
                            <span>+</span>
                        </h1>
                        <span>tahun pengalaman di pasar forex</span>
                    </div>
                    <div class="col-sm-6 col-lg-3 mobile-mb-5 count-list">
                        <h1>
                            <span class="count-num">600</span>
                            <span>K+</span>
                        </h1>
                        <span>trader yang puas dari 180 negara</span>
                    </div>
                    <div class="col-sm-6 col-lg-3 mobile-mb-5 mb-sm-0 count-list">
                        <h1>
                            <span class="count-num">24</span>
                            <span>/</span>
                            <span class="count-num">7</span>
                        </h1>
                        <span>layanan full support untuk klien</span>
                    </div>
                    <div class="col-sm-6 col-lg-3 count-list">
                        <h1>
                            <span class="count-num">30</span>
                        </h1>
                        <span>menit untuk penarikan dana</span>
                    </div>
                </div>
            </div>
        </section>
        <!--/count-->
@endsection