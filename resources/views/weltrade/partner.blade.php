@extends('layout_weltrade')

@section('title', 'Weltrade - Komisi IB Paling Besar')

@section('main_section')
<div class="uk-section uk-padding-remove-vertical">
    <div class="in-slideshow" data-uk-slideshow>
        <ul class="uk-slideshow-items uk-light">
            <li>
                <div class="uk-position-cover">
                    <img src="<?php echo url('/') ?>/template/landing/img/in-lazy.gif" data-src="<?php echo url('/') ?>/template/landing/img/in-slideshow-image-partner.jpg" alt="slideshow-image" data-uk-cover width="1920" height="700" data-uk-img>
                </div>
                <span></span>
                <div class="uk-container">
                    <div class="uk-grid" data-uk-grid>
                        <div class="uk-width-3-5@m">
                            <div class="uk-overlay">
                                <h1>Weltrade, "Komisi IB Paling Besar"</h1>
                                <p class="uk-text-lead uk-visible@m">Kami siap mendukung berbagai ide dan kegiatan Anda. Jadi Partner(IB) dan dapatkan komisi besar hingga 85% dari spread dan tambahan komisi hingga $30.000 dibayar setiap jam!</p>
                                <a href="https://account.<?php echo $weltrade ?>/auth/registration/?r1=ids&r2=landing_<?php echo $referral ?>" class="uk-button uk-button-primary uk-border-rounded"><i class="fas fa-comment-dollar uk-margin-small-right"></i>Buka Akun sekarang</a>
                                <a href="https://<?php echo $weltradeidn ?>" class="uk-button uk-button-primary uk-border-rounded"><i class="fas fa-home uk-margin-small-right"></i>Kembali ke Web Utama</a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <div class="uk-container uk-light">
            <ul class="uk-slideshow-nav uk-dotnav uk-position-bottom-center"></ul>
        </div>
    </div>
</div> 
@endsection