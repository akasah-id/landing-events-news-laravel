<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// weltrade-idn.com
Route::domain('weltrade-idn.com')->group(function () {
	Route::get('/', 'Weltrade\Landing@home');
	Route::get('/{any}', 'Weltrade\Landing@home');
	Route::get('/{any}/{any2}', 'Weltrade\Landing@home');
	Route::get('/{any}/{any2}/{any3}', 'Weltrade\Landing@home');
	// Route::get('/campaign/{ref}', 'Weltrade\Landing@homeCampaign');
	// Route::get('/partner/{ref}', 'Weltrade\Landing@homePartner');
	// Route::get('/analisa-harian', 'Weltrade\Landing@homeAnalisa');
	// Route::get('/analisa-harian/{slug}', 'Weltrade\Landing@homeNewsDetail');
	// Route::get('/sinyal-trading', 'Weltrade\Landing@homeSinyal');
	// Route::get('/sinyal-trading/{slug}', 'Weltrade\Landing@homeNewsDetail');
	// Route::get('/prakiraan-mingguan', 'Weltrade\Landing@homePrakiraan');
	// Route::get('/prakiraan-mingguan/{slug}', 'Weltrade\Landing@homeNewsDetail');
	// Route::get('/webinar', 'Weltrade\Landing@homeWebinar');
	// Route::get('/webinar/{slug}', 'Weltrade\Landing@homeNewsDetail');
	// Route::get('/artikel/{slug}', 'Weltrade\Landing@homeNewsDetail');
	// Route::get('/loyalty', 'Weltrade\Landing@homeLoyalty');
	// Route::get('/sitemap', function()
	// {
	//    return Response::view('sitemap')->header('Content-Type', 'application/xml');
	// });
});

// weltrade-idn.net
Route::domain('weltrade-idn.net')->group(function () {
	Route::get('/', 'Weltrade\Landing@main');
	Route::get('/{referral}', 'Weltrade\Landing@main');
});

Route::domain('belajartrading.cc')->group(function () {
    Route::get('/', 'Weltrade\Landing@main');
    Route::get('/{referral}', 'Weltrade\Landing@main');
});

Route::domain('brokerterbaik.cc')->group(function () {
    Route::get('/', 'Weltrade\Landing@main');
    Route::get('/{referral}', 'Weltrade\Landing@main');
});

Route::domain('kontesdemo.cc')->group(function () {
    Route::get('/', 'Weltrade\Landing@main');
    Route::get('/{referral}', 'Weltrade\Landing@main');
});

Route::domain('tradingsaham.cc')->group(function () {
    Route::get('/', 'Weltrade\Landing@main');
    Route::get('/{referral}', 'Weltrade\Landing@main');
});

Route::domain('brokerterbaik.info')->group(function () {
    Route::get('/', 'Weltrade\Landing@main');
    Route::get('/{referral}', 'Weltrade\Landing@main');
});

Route::domain('edukasitrading.org')->group(function () {
    Route::get('/', function () {
	    return redirect('/gg_education');
	});
    Route::get('/{referral}', 'Weltrade\Landing@main');
});

// All domain
Route::get('/', 'Weltrade\Landing@main');
Route::get('/home', 'Weltrade\Landing@home');
// Route::get('/home/campaign/{ref}', 'Weltrade\Landing@homeCampaign');
// Route::get('/home/partner/{ref}', 'Weltrade\Landing@homePartner');
Route::get('/{referral}', 'Weltrade\Landing@main');


// End

// Route::get('/', 'StarterKit\Login@login');
Route::get('/admin', 'StarterKit\Login@login');
Route::get('/login', 'StarterKit\Login@login');
Route::post('/login/process', 'StarterKit\Login@process');
Route::post('/login/reset', 'StarterKit\Login@reset');
Route::post('/login/register', 'StarterKit\Login@register');
Route::get('/confirm/{id}/{token}', 'StarterKit\Login@registerConfirm');
Route::get('/login/changelang/{lang}', 'StarterKit\Login@changeLang');
Route::get('/logout', 'StarterKit\Login@logout');

Route::middleware(['AuthAdmin'])->group(function () {
		// Tools
		Route::get("/file_manager", function(){
		   return View::make("starter_kit.file_manager");
		});

	    Route::get('/link', 'Tools\Link@data');
	    Route::get('/link/new', 'Tools\Link@view');
	    Route::get('/link/view/{id}', 'Tools\Link@view');
	    Route::get('/link/delete/{id}', 'Tools\Link@delete');
	    Route::post('/link/save', 'Tools\Link@save');
	    Route::post('/link/save/{id}', 'Tools\Link@save');
	    Route::get('/link/ajax_data', 'Tools\Link@ajax_data');
	    Route::post('/link/batch_action', 'Tools\Link@batch_action');
	    // End of Tools

		// Wazzap
		Route::get('/event', 'Wazzap\Event@data');
		Route::get('/event/new', 'Wazzap\Event@view');
		Route::get('/event/view/{id}', 'Wazzap\Event@view');
		Route::get('/event/delete/{id}', 'Wazzap\Event@delete');
		Route::post('/event/save', 'Wazzap\Event@save');
		Route::post('/event/save/{id}', 'Wazzap\Event@save');
		Route::get('/event/ajax_data', 'Wazzap\Event@ajax_data');
		Route::post('/event/batch_action', 'Wazzap\Event@batch_action');
		Route::get('/event/participant/{id}', 'Wazzap\Event@getParticipant');

		Route::get('/podcast', 'Wazzap\Podcast@data');
		Route::get('/podcast/new', 'Wazzap\Podcast@view');
		Route::get('/podcast/view/{id}', 'Wazzap\Podcast@view');
		Route::get('/podcast/delete/{id}', 'Wazzap\Podcast@delete');
		Route::post('/podcast/save', 'Wazzap\Podcast@save');
		Route::post('/podcast/save/{id}', 'Wazzap\Podcast@save');
		Route::get('/podcast/ajax_data', 'Wazzap\Podcast@ajax_data');
		Route::post('/podcast/batch_action', 'Wazzap\Podcast@batch_action');

	    Route::get('/news', 'Wazzap\News@data');
	    Route::get('/news/new', 'Wazzap\News@view');
	    Route::get('/news/view/{id}', 'Wazzap\News@view');
	    Route::get('/news/delete/{id}', 'Wazzap\News@delete');
	    Route::post('/news/save', 'Wazzap\News@save');
	    Route::post('/news/save/{id}', 'Wazzap\News@save');
	    Route::get('/news/ajax_data', 'Wazzap\News@ajax_data');
	    Route::post('/news/batch_action', 'Wazzap\News@batch_action');

	    Route::get('/news_category', 'Wazzap\NewsCategory@data');
	    Route::get('/news_category/new', 'Wazzap\NewsCategory@view');
	    Route::get('/news_category/view/{id}', 'Wazzap\NewsCategory@view');
	    Route::get('/news_category/delete/{id}', 'Wazzap\NewsCategory@delete');
	    Route::post('/news_category/save', 'Wazzap\NewsCategory@save');
	    Route::post('/news_category/save/{id}', 'Wazzap\NewsCategory@save');
	    Route::get('/news_category/ajax_data', 'Wazzap\NewsCategory@ajax_data');
	    Route::post('/news_category/batch_action', 'Wazzap\NewsCategory@batch_action');

	    Route::get('/news_rating', 'Wazzap\NewsRating@data');
	    Route::get('/news_rating/new', 'Wazzap\NewsRating@view');
	    Route::get('/news_rating/view/{id}', 'Wazzap\NewsRating@view');
	    Route::get('/news_rating/delete/{id}', 'Wazzap\NewsRating@delete');
	    Route::post('/news_rating/save', 'Wazzap\NewsRating@save');
	    Route::post('/news_rating/save/{id}', 'Wazzap\NewsRating@save');
	    Route::get('/news_rating/ajax_data', 'Wazzap\NewsRating@ajax_data');
	    Route::post('/news_rating/batch_action', 'Wazzap\NewsRating@batch_action');
	    Route::get('/news_rating/get_review/{id}', 'Wazzap\NewsRating@get_review');
	    // End of Wazzap

	    // Starter Kit
		Route::get('/dashboard', 'StarterKit\Dashboard@data');
		Route::get('/lock', 'StarterKit\Dashboard@lock');
		Route::post('/lasturl', 'StarterKit\Dashboard@lasturl');
		Route::post('/unlock', 'StarterKit\Dashboard@unlock');

		Route::get('/users', 'StarterKit\Users@data');
		Route::get('/users/new', 'StarterKit\Users@view');
		Route::get('/users/status', 'StarterKit\Users@status');
		Route::get('/users/view/{id}', 'StarterKit\Users@view');
		Route::get('/users/delete/{id}', 'StarterKit\Users@delete');
		Route::post('/users/save', 'StarterKit\Users@save');
		Route::post('/users/save/{id}', 'StarterKit\Users@save');
		Route::get('/users/ajax_data', 'StarterKit\Users@ajax_data');
		Route::post('/users/batch_action', 'StarterKit\Users@batch_action');

		Route::get('/groups', 'StarterKit\Groups@data');
		Route::get('/groups/new', 'StarterKit\Groups@view');
		Route::get('/groups/view/{id}', 'StarterKit\Groups@view');
		Route::get('/groups/delete/{id}', 'StarterKit\Groups@delete');
		Route::post('/groups/save', 'StarterKit\Groups@save');
		Route::post('/groups/save/{id}', 'StarterKit\Groups@save');
		Route::get('/groups/ajax_data', 'StarterKit\Groups@ajax_data');
		Route::post('/groups/batch_action', 'StarterKit\Groups@batch_action');

		Route::get('/access/{group_id}', 'StarterKit\Access@data');
		Route::get('/access/new/{group_id}', 'StarterKit\Access@view');
		Route::get('/access/view/{group_id}/{id}', 'StarterKit\Access@view');
		Route::get('/access/delete/{group_id}/{id}', 'StarterKit\Access@delete');
		Route::post('/access/save/{group_id}/{id}', 'StarterKit\Access@save');
		Route::get('/access/ajax_data/{group_id}', 'StarterKit\Access@ajax_data');
		Route::post('/access/batch_action/{group_id}', 'StarterKit\Access@batch_action');

		Route::get('/menu', 'StarterKit\Menus@data');
		Route::get('/menu/new', 'StarterKit\Menus@view');
		Route::get('/menu/view/{id}', 'StarterKit\Menus@view');
		Route::get('/menu/delete/{id}', 'StarterKit\Menus@delete');
		Route::post('/menu/save', 'StarterKit\Menus@save');
		Route::post('/menu/save/{id}', 'StarterKit\Menus@save');
		Route::post('/menu/reposition', 'StarterKit\Menus@reposition');	

	    Route::get('/logactivity', 'StarterKit\Audit@data');
	    Route::get('/logactivity/ajax_data', 'StarterKit\Audit@ajax_data');
	    Route::get('/logactivity/delete/{id}', 'StarterKit\Audit@delete');
	    Route::post('/logactivity/batch_action', 'StarterKit\Audit@batch_action');

	    Route::get('/email', 'StarterKit\Email@data');
	    Route::get('/email/new', 'StarterKit\Email@view');
	    Route::get('/email/view/{id}', 'StarterKit\Email@view');
	    Route::get('/email/delete/{id}', 'StarterKit\Email@delete');
	    Route::post('/email/save', 'StarterKit\Email@save');
	    Route::post('/email/save/{id}', 'StarterKit\Email@save');
	    Route::get('/email/ajax_data', 'StarterKit\Email@ajax_data');
	    Route::post('/email/batch_action', 'StarterKit\Email@batch_action');
	    // End of Starter Kit
});